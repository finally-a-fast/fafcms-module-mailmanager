<?php

namespace fafcms\mailmanager\abstracts\models;

use fafcms\fafcms\{
    inputs\DateTimePicker,
    inputs\DropDownList,
    inputs\ExtendedDropDownList,
    inputs\NumberInput,
    inputs\SwitchCheckbox,
    inputs\Textarea,
    inputs\TextInput,
    items\ActionColumn,
    items\Card,
    items\Column,
    items\DataColumn,
    items\FormField,
    items\Row,
    items\Tab,
    models\User,
};
use fafcms\helpers\{
    ActiveRecord,
    classes\OptionProvider,
    interfaces\EditViewInterface,
    interfaces\FieldConfigInterface,
    interfaces\IndexViewInterface,
    traits\AttributeOptionTrait,
    traits\BeautifulModelTrait,
    traits\OptionProviderTrait,
};
use fafcms\mailmanager\{
    Bootstrap,
    models\Mail,
    models\Mailing,
};
use fafcms\sitemanager\models\Contentmeta;
use Yii;
use yii\db\ActiveQuery;
use yii\validators\DateValidator;

/**
 * This is the abstract model class for table "{{%template}}".
 *
 * @package fafcms\mailmanager\abstracts\models
 *
 * @property-read array $fieldConfig
 *
 * @property int $id
 * @property string $status
 * @property string $name
 * @property string|null $to
 * @property string|null $cc
 * @property string|null $bcc
 * @property string|null $from
 * @property string|null $replyto
 * @property string|null $subject
 * @property string|null $message_html
 * @property string|null $message_text
 * @property string|null $attachments
 * @property int $track_view
 * @property int $track_click
 * @property int|null $unsubscribe_contentmeta_id
 * @property int|null $config_contentmeta_id
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $activated_by
 * @property int|null $deactivated_by
 * @property int|null $deleted_by
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $activated_at
 * @property string|null $deactivated_at
 * @property string|null $deleted_at
 *
 * @property User $activatedBy
 * @property Contentmeta $configContentmeta
 * @property User $createdBy
 * @property User $deactivatedBy
 * @property User $deletedBy
 * @property Mailing[] $templateMailings
 * @property Mail[] $templateMails
 * @property Contentmeta $unsubscribeContentmeta
 * @property User $updatedBy
 */
abstract class BaseTemplate extends ActiveRecord implements FieldConfigInterface, IndexViewInterface, EditViewInterface
{
    use BeautifulModelTrait;
    use OptionProviderTrait;
    use AttributeOptionTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return Bootstrap::$id . '/template';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return  'template';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-mailmanager', 'Templates');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-mailmanager', 'Template');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['name'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionProviderTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptionProvider(array $properties = []): OptionProvider
    {
        return (new OptionProvider(static::class))
            ->setSelect([
                static::tableName() . '.id',
                static::tableName() . '.name'
            ])
            ->setSort([static::tableName() . '.name' => SORT_ASC])
            ->setItemLabel(static function ($item) {
                return static::extendedLabel($item);
            })
            ->setProperties($properties);
    }
    //endregion OptionProviderTrait implementation

    //region AttributeOptionTrait implementation
    /**
     * @inheritDoc
     */
    public function attributeOptions(): array
    {
        return [
            'status' => [
                static::STATUS_ACTIVE => Yii::t('fafcms-core', 'Active'),
                static::STATUS_INACTIVE => Yii::t('fafcms-core', 'Inactive'),
            ],
            'unsubscribe_contentmeta_id' => static function($properties = []) {
                return Contentmeta::getOptionProvider($properties)->getOptions();
            },
            'config_contentmeta_id' => static function($properties = []) {
                return Contentmeta::getOptionProvider($properties)->getOptions();
            },
            'created_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'updated_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'activated_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'deactivated_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'deleted_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
        ];
    }
    //endregion AttributeOptionTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'id' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'status' => [
                'type' => DropDownList::class,
                'items' => $this->getAttributeOptions('status', false),
            ],
            'name' => [
                'type' => TextInput::class,
            ],
            'to' => [
                'type' => Textarea::class,
            ],
            'cc' => [
                'type' => Textarea::class,
            ],
            'bcc' => [
                'type' => Textarea::class,
            ],
            'from' => [
                'type' => Textarea::class,
            ],
            'replyto' => [
                'type' => Textarea::class,
            ],
            'subject' => [
                'type' => Textarea::class,
            ],
            'message_html' => [
                'type' => Textarea::class,
            ],
            'message_text' => [
                'type' => Textarea::class,
            ],
            'attachments' => [
                'type' => Textarea::class,
            ],
            'track_view' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'track_click' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'unsubscribe_contentmeta_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('unsubscribe_contentmeta_id', false),
                'relationClassName' => Contentmeta::class,
            ],
            'config_contentmeta_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('config_contentmeta_id', false),
                'relationClassName' => Contentmeta::class,
            ],
            'created_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('created_by', false),
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('updated_by', false),
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'activated_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('activated_by', false),
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deactivated_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('deactivated_by', false),
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deleted_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('deleted_by', false),
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'created_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'activated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deactivated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deleted_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
        ];
    }
    //endregion FieldConfigInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        return [
            'default' => [
                'id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'id',
                        'sort' => 1,
                    ],
                ],
                'status' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'status',
                        'sort' => 2,
                    ],
                ],
                'name' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'name',
                        'sort' => 3,
                        'link' => true,
                    ],
                ],
                'to' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'to',
                        'sort' => 4,
                    ],
                ],
                'cc' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'cc',
                        'sort' => 5,
                    ],
                ],
                'bcc' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'bcc',
                        'sort' => 6,
                    ],
                ],
                'from' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'from',
                        'sort' => 7,
                    ],
                ],
                'replyto' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'replyto',
                        'sort' => 8,
                    ],
                ],
                'subject' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'subject',
                        'sort' => 9,
                    ],
                ],
                'message_html' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'message_html',
                        'sort' => 10,
                    ],
                ],
                'message_text' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'message_text',
                        'sort' => 11,
                    ],
                ],
                'attachments' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'attachments',
                        'sort' => 12,
                    ],
                ],
                'track_view' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'track_view',
                        'sort' => 13,
                    ],
                ],
                'track_click' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'track_click',
                        'sort' => 14,
                    ],
                ],
                'unsubscribe_contentmeta_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'unsubscribe_contentmeta_id',
                        'sort' => 15,
                        'link' => true,
                    ],
                ],
                'config_contentmeta_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'config_contentmeta_id',
                        'sort' => 16,
                        'link' => true,
                    ],
                ],
                'action-column' => [
                    'class' => ActionColumn::class,
                ],
            ]
        ];
    }
    //endregion IndexViewInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-status' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'status',
                                                    ],
                                                ],
                                                'field-name' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'name',
                                                    ],
                                                ],
                                                'field-to' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'to',
                                                    ],
                                                ],
                                                'field-cc' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'cc',
                                                    ],
                                                ],
                                                'field-bcc' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'bcc',
                                                    ],
                                                ],
                                                'field-from' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'from',
                                                    ],
                                                ],
                                                'field-replyto' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'replyto',
                                                    ],
                                                ],
                                                'field-subject' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'subject',
                                                    ],
                                                ],
                                                'field-message_html' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'message_html',
                                                    ],
                                                ],
                                                'field-message_text' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'message_text',
                                                    ],
                                                ],
                                                'field-attachments' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'attachments',
                                                    ],
                                                ],
                                                'field-track_view' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'track_view',
                                                    ],
                                                ],
                                                'field-track_click' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'track_click',
                                                    ],
                                                ],
                                                'field-unsubscribe_contentmeta_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'unsubscribe_contentmeta_id',
                                                    ],
                                                ],
                                                'field-config_contentmeta_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'config_contentmeta_id',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%template}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'required-name' => ['name', 'required'],
            'string-to' => ['to', 'string'],
            'string-cc' => ['cc', 'string'],
            'string-bcc' => ['bcc', 'string'],
            'string-from' => ['from', 'string'],
            'string-replyto' => ['replyto', 'string'],
            'string-subject' => ['subject', 'string'],
            'string-message_html' => ['message_html', 'string'],
            'string-message_text' => ['message_text', 'string'],
            'string-attachments' => ['attachments', 'string'],
            'boolean-track_view' => ['track_view', 'boolean'],
            'boolean-track_click' => ['track_click', 'boolean'],
            'integer-unsubscribe_contentmeta_id' => ['unsubscribe_contentmeta_id', 'integer'],
            'integer-config_contentmeta_id' => ['config_contentmeta_id', 'integer'],
            'integer-created_by' => ['created_by', 'integer'],
            'integer-updated_by' => ['updated_by', 'integer'],
            'integer-activated_by' => ['activated_by', 'integer'],
            'integer-deactivated_by' => ['deactivated_by', 'integer'],
            'integer-deleted_by' => ['deleted_by', 'integer'],
            'date-created_at' => ['created_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'created_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-updated_at' => ['updated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'updated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-activated_at' => ['activated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'activated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-deactivated_at' => ['deactivated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'deactivated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-deleted_at' => ['deleted_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'deleted_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'string-status' => ['status', 'string', 'max' => 255],
            'string-name' => ['name', 'string', 'max' => 255],
            'exist-activated_by' => [['activated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['activated_by' => 'id']],
            'exist-config_contentmeta_id' => [['config_contentmeta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contentmeta::class, 'targetAttribute' => ['config_contentmeta_id' => 'id']],
            'exist-created_by' => [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            'exist-deactivated_by' => [['deactivated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['deactivated_by' => 'id']],
            'exist-deleted_by' => [['deleted_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['deleted_by' => 'id']],
            'exist-unsubscribe_contentmeta_id' => [['unsubscribe_contentmeta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contentmeta::class, 'targetAttribute' => ['unsubscribe_contentmeta_id' => 'id']],
            'exist-updated_by' => [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'id' => Yii::t('fafcms-mailmanager', 'ID'),
            'status' => Yii::t('fafcms-mailmanager', 'Status'),
            'name' => Yii::t('fafcms-mailmanager', 'Name'),
            'to' => Yii::t('fafcms-mailmanager', 'To'),
            'cc' => Yii::t('fafcms-mailmanager', 'Cc'),
            'bcc' => Yii::t('fafcms-mailmanager', 'Bcc'),
            'from' => Yii::t('fafcms-mailmanager', 'From'),
            'replyto' => Yii::t('fafcms-mailmanager', 'Replyto'),
            'subject' => Yii::t('fafcms-mailmanager', 'Subject'),
            'message_html' => Yii::t('fafcms-mailmanager', 'Message Html'),
            'message_text' => Yii::t('fafcms-mailmanager', 'Message Text'),
            'attachments' => Yii::t('fafcms-mailmanager', 'Attachments'),
            'track_view' => Yii::t('fafcms-mailmanager', 'Track View'),
            'track_click' => Yii::t('fafcms-mailmanager', 'Track Click'),
            'unsubscribe_contentmeta_id' => Yii::t('fafcms-mailmanager', 'Unsubscribe Contentmeta ID'),
            'config_contentmeta_id' => Yii::t('fafcms-mailmanager', 'Config Contentmeta ID'),
            'created_by' => Yii::t('fafcms-mailmanager', 'Created By'),
            'updated_by' => Yii::t('fafcms-mailmanager', 'Updated By'),
            'activated_by' => Yii::t('fafcms-mailmanager', 'Activated By'),
            'deactivated_by' => Yii::t('fafcms-mailmanager', 'Deactivated By'),
            'deleted_by' => Yii::t('fafcms-mailmanager', 'Deleted By'),
            'created_at' => Yii::t('fafcms-mailmanager', 'Created At'),
            'updated_at' => Yii::t('fafcms-mailmanager', 'Updated At'),
            'activated_at' => Yii::t('fafcms-mailmanager', 'Activated At'),
            'deactivated_at' => Yii::t('fafcms-mailmanager', 'Deactivated At'),
            'deleted_at' => Yii::t('fafcms-mailmanager', 'Deleted At'),
        ]);
    }

    /**
     * Gets query for [[ActivatedBy]].
     *
     * @return ActiveQuery
     */
    public function getActivatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'activated_by',
        ]);
    }

    /**
     * Gets query for [[ConfigContentmeta]].
     *
     * @return ActiveQuery
     */
    public function getConfigContentmeta(): ActiveQuery
    {
        return $this->hasOne(Contentmeta::class, [
            'id' => 'config_contentmeta_id',
        ]);
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return ActiveQuery
     */
    public function getCreatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'created_by',
        ]);
    }

    /**
     * Gets query for [[DeactivatedBy]].
     *
     * @return ActiveQuery
     */
    public function getDeactivatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'deactivated_by',
        ]);
    }

    /**
     * Gets query for [[DeletedBy]].
     *
     * @return ActiveQuery
     */
    public function getDeletedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'deleted_by',
        ]);
    }

    /**
     * Gets query for [[TemplateMailings]].
     *
     * @return ActiveQuery
     */
    public function getTemplateMailings(): ActiveQuery
    {
        return $this->hasMany(Mailing::class, [
            'template_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[TemplateMails]].
     *
     * @return ActiveQuery
     */
    public function getTemplateMails(): ActiveQuery
    {
        return $this->hasMany(Mail::class, [
            'template_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[UnsubscribeContentmeta]].
     *
     * @return ActiveQuery
     */
    public function getUnsubscribeContentmeta(): ActiveQuery
    {
        return $this->hasOne(Contentmeta::class, [
            'id' => 'unsubscribe_contentmeta_id',
        ]);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return ActiveQuery
     */
    public function getUpdatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'updated_by',
        ]);
    }
}
