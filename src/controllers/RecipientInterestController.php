<?php

declare(strict_types=1);

/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 - 2021 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-mailmanager/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-mailmanager
 * @see https://www.finally-a-fast.com/packages/fafcms-module-mailmanager/docs Documentation of the module mail manager
 * @since File available since Release 1.0.0
 */

namespace fafcms\mailmanager\controllers;

use fafcms\helpers\DefaultController;
use fafcms\mailmanager\models\RecipientInterest;

/**
 * Class RecipientInterestController
 *
 * @package fafcms\mailmanager\models
 */
class RecipientInterestController extends DefaultController
{
    public static $modelClass = RecipientInterest::class;
}
