<?php

namespace fafcms\mailmanager\abstracts\models;

use fafcms\fafcms\{
    inputs\DateTimePicker,
    inputs\ExtendedDropDownList,
    inputs\NumberInput,
    inputs\SwitchCheckbox,
    inputs\TextInput,
    items\ActionColumn,
    items\Card,
    items\Column,
    items\DataColumn,
    items\FormField,
    items\Row,
    items\Tab,
    models\User,
};
use fafcms\helpers\{
    ActiveRecord,
    classes\OptionProvider,
    interfaces\EditViewInterface,
    interfaces\FieldConfigInterface,
    interfaces\IndexViewInterface,
    traits\AttributeOptionTrait,
    traits\BeautifulModelTrait,
    traits\OptionProviderTrait,
};
use fafcms\mailmanager\{
    Bootstrap,
    models\Mail,
    models\Mailing,
    models\Recipient,
};
use Yii;
use yii\db\ActiveQuery;
use yii\validators\DateValidator;

/**
 * This is the abstract model class for table "{{%track}}".
 *
 * @package fafcms\mailmanager\abstracts\models
 *
 * @property-read array $fieldConfig
 *
 * @property int $id
 * @property int|null $mailing_id
 * @property int|null $mail_id
 * @property int|null $recipient_id
 * @property int $type
 * @property string|null $target
 * @property int $used
 * @property int $use_count
 * @property string|null $first_use_at
 * @property string|null $last_use_at
 * @property string|null $valid_until
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property User $createdBy
 * @property Mail $mail
 * @property Mailing $mailing
 * @property Recipient $recipient
 * @property User $updatedBy
 */
abstract class BaseTrack extends ActiveRecord implements FieldConfigInterface, IndexViewInterface, EditViewInterface
{
    use BeautifulModelTrait;
    use OptionProviderTrait;
    use AttributeOptionTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return Bootstrap::$id . '/track';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return  'track';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-mailmanager', 'Tracks');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-mailmanager', 'Track');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['id'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionProviderTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptionProvider(array $properties = []): OptionProvider
    {
        return (new OptionProvider(static::class))
            ->setSelect([
                static::tableName() . '.id',
                static::tableName() . '.id'
            ])
            ->setSort([static::tableName() . '.id' => SORT_ASC])
            ->setItemLabel(static function ($item) {
                return static::extendedLabel($item);
            })
            ->setProperties($properties);
    }
    //endregion OptionProviderTrait implementation

    //region AttributeOptionTrait implementation
    /**
     * @inheritDoc
     */
    public function attributeOptions(): array
    {
        return [
            'mailing_id' => static function($properties = []) {
                return Mailing::getOptionProvider($properties)->getOptions();
            },
            'mail_id' => static function($properties = []) {
                return Mail::getOptionProvider($properties)->getOptions();
            },
            'recipient_id' => static function($properties = []) {
                return Recipient::getOptionProvider($properties)->getOptions();
            },
            'created_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'updated_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
        ];
    }
    //endregion AttributeOptionTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'id' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'mailing_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('mailing_id', false),
                'relationClassName' => Mailing::class,
            ],
            'mail_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('mail_id', false),
                'relationClassName' => Mail::class,
            ],
            'recipient_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('recipient_id', false),
                'relationClassName' => Recipient::class,
            ],
            'type' => [
                'type' => NumberInput::class,
            ],
            'target' => [
                'type' => TextInput::class,
            ],
            'used' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'use_count' => [
                'type' => NumberInput::class,
            ],
            'first_use_at' => [
                'type' => DateTimePicker::class,
            ],
            'last_use_at' => [
                'type' => DateTimePicker::class,
            ],
            'valid_until' => [
                'type' => DateTimePicker::class,
            ],
            'created_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('created_by', false),
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('updated_by', false),
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'created_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
        ];
    }
    //endregion FieldConfigInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        return [
            'default' => [
                'id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'id',
                        'sort' => 1,
                        'link' => true,
                    ],
                ],
                'mailing_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'mailing_id',
                        'sort' => 2,
                        'link' => true,
                    ],
                ],
                'mail_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'mail_id',
                        'sort' => 3,
                        'link' => true,
                    ],
                ],
                'recipient_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'recipient_id',
                        'sort' => 4,
                        'link' => true,
                    ],
                ],
                'type' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'type',
                        'sort' => 5,
                    ],
                ],
                'target' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'target',
                        'sort' => 6,
                    ],
                ],
                'used' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'used',
                        'sort' => 7,
                    ],
                ],
                'use_count' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'use_count',
                        'sort' => 8,
                    ],
                ],
                'first_use_at' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'first_use_at',
                        'sort' => 9,
                    ],
                ],
                'last_use_at' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'last_use_at',
                        'sort' => 10,
                    ],
                ],
                'valid_until' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'valid_until',
                        'sort' => 11,
                    ],
                ],
                'action-column' => [
                    'class' => ActionColumn::class,
                ],
            ]
        ];
    }
    //endregion IndexViewInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-mailing_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'mailing_id',
                                                    ],
                                                ],
                                                'field-mail_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'mail_id',
                                                    ],
                                                ],
                                                'field-recipient_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'recipient_id',
                                                    ],
                                                ],
                                                'field-type' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'type',
                                                    ],
                                                ],
                                                'field-target' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'target',
                                                    ],
                                                ],
                                                'field-used' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'used',
                                                    ],
                                                ],
                                                'field-use_count' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'use_count',
                                                    ],
                                                ],
                                                'field-first_use_at' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'first_use_at',
                                                    ],
                                                ],
                                                'field-last_use_at' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'last_use_at',
                                                    ],
                                                ],
                                                'field-valid_until' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'valid_until',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%track}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'integer-mailing_id' => ['mailing_id', 'integer'],
            'integer-mail_id' => ['mail_id', 'integer'],
            'integer-recipient_id' => ['recipient_id', 'integer'],
            'integer-type' => ['type', 'integer'],
            'integer-use_count' => ['use_count', 'integer'],
            'integer-created_by' => ['created_by', 'integer'],
            'integer-updated_by' => ['updated_by', 'integer'],
            'required-type' => ['type', 'required'],
            'boolean-used' => ['used', 'boolean'],
            'date-first_use_at' => ['first_use_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'first_use_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-last_use_at' => ['last_use_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'last_use_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-valid_until' => ['valid_until', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'valid_until', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-created_at' => ['created_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'created_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-updated_at' => ['updated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'updated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'string-target' => ['target', 'string', 'max' => 5000],
            'exist-created_by' => [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            'exist-mail_id' => [['mail_id'], 'exist', 'skipOnError' => true, 'targetClass' => Mail::class, 'targetAttribute' => ['mail_id' => 'id']],
            'exist-mailing_id' => [['mailing_id'], 'exist', 'skipOnError' => true, 'targetClass' => Mailing::class, 'targetAttribute' => ['mailing_id' => 'id']],
            'exist-recipient_id' => [['recipient_id'], 'exist', 'skipOnError' => true, 'targetClass' => Recipient::class, 'targetAttribute' => ['recipient_id' => 'id']],
            'exist-updated_by' => [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'id' => Yii::t('fafcms-mailmanager', 'ID'),
            'mailing_id' => Yii::t('fafcms-mailmanager', 'Mailing ID'),
            'mail_id' => Yii::t('fafcms-mailmanager', 'Mail ID'),
            'recipient_id' => Yii::t('fafcms-mailmanager', 'Recipient ID'),
            'type' => Yii::t('fafcms-mailmanager', 'Type'),
            'target' => Yii::t('fafcms-mailmanager', 'Target'),
            'used' => Yii::t('fafcms-mailmanager', 'Used'),
            'use_count' => Yii::t('fafcms-mailmanager', 'Use Count'),
            'first_use_at' => Yii::t('fafcms-mailmanager', 'First Use At'),
            'last_use_at' => Yii::t('fafcms-mailmanager', 'Last Use At'),
            'valid_until' => Yii::t('fafcms-mailmanager', 'Valid Until'),
            'created_by' => Yii::t('fafcms-mailmanager', 'Created By'),
            'updated_by' => Yii::t('fafcms-mailmanager', 'Updated By'),
            'created_at' => Yii::t('fafcms-mailmanager', 'Created At'),
            'updated_at' => Yii::t('fafcms-mailmanager', 'Updated At'),
        ]);
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return ActiveQuery
     */
    public function getCreatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'created_by',
        ]);
    }

    /**
     * Gets query for [[Mail]].
     *
     * @return ActiveQuery
     */
    public function getMail(): ActiveQuery
    {
        return $this->hasOne(Mail::class, [
            'id' => 'mail_id',
        ]);
    }

    /**
     * Gets query for [[Mailing]].
     *
     * @return ActiveQuery
     */
    public function getMailing(): ActiveQuery
    {
        return $this->hasOne(Mailing::class, [
            'id' => 'mailing_id',
        ]);
    }

    /**
     * Gets query for [[Recipient]].
     *
     * @return ActiveQuery
     */
    public function getRecipient(): ActiveQuery
    {
        return $this->hasOne(Recipient::class, [
            'id' => 'recipient_id',
        ]);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return ActiveQuery
     */
    public function getUpdatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'updated_by',
        ]);
    }
}
