<?php
/**
 * @var $this \yii\web\View
 * @var $headline string
 * @var $required string
 * @var $formId string
 * @var $model \fafcms\mailmanager\models\SubscribeForm
 * @var $activeFormClass \yii\widgets\ActiveForm
 * @var $submitButtonLabel string
 * @var $submitButtonCssClass string|null
 */

use yii\helpers\Html;
use fafcms\fafcms\widgets\MaskedInput;

$form = $activeFormClass::begin([
    'id' => $formId
]);

echo '<h1>' . $headline . '</h1>';

echo $form->field($model, 'sex')->radioList($model->getAttributeOptions('sex'));

echo $form->field($model, 'firstname')->textInput();

echo $form->field($model, 'lastname')->textInput();

echo $form->field($model, 'email')->widget(MaskedInput::class, [
    'clientOptions' => ['alias' => 'email'],
]);

$interestsOptions = [
    'multiple' => true,
];

foreach ($model->getAttributeOptions('interests') as $interest => $interestLabel) {
    $interestsOptions['options'][$interest] = ['selected' => true];
}

echo $form->field($model, 'interests')->dropDownList($model->getAttributeOptions('interests'), $interestsOptions);

echo $form->field($model, 'privacyPolicy')->checkbox([
    'label' => $model->getAttributeLabel('privacyPolicy')
]);

echo Html::submitButton($submitButtonLabel, ['form' => $form->id, 'class' => $submitButtonCssClass]);

echo '<dl><dt><sup>*</sup> ' . $required . '</dt></dl>';

$activeFormClass::end();
