<?php

declare(strict_types=1);

/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 - 2021 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-mailmanager/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-mailmanager
 * @see https://www.finally-a-fast.com/packages/fafcms-module-mailmanager/docs Documentation of the module mail manager
 * @since File available since Release 1.0.0
 */

namespace fafcms\mailmanager\controllers;

use fafcms\helpers\DefaultController;
use fafcms\mailmanager\models\Mailing;
use fafcms\mailmanager\models\Template;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use Yii;

/**
 * Class TemplateController
 *
 * @package fafcms\mailmanager\models
 */
class TemplateController extends DefaultController
{
    public static $modelClass = Template::class;

    /**
     * @param $id
     *
     * @return Response
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCreateMailing($id): Response
    {
        $model = self::$modelClass::findOne($id);

        if ($model === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $mailing = Mailing::createByTemplate($model->id);

        $singular = Mailing::instance()->getEditDataSingular();

        if ($mailing->save()) {
            Yii::$app->session->setFlash('success', Yii::t('fafcms-core', '{modelClass} has been saved.', [
                'modelClass' => $singular,
            ]));

            return $this->redirect(['/' . $mailing->getEditDataUrl() . '/update', 'id' => $mailing->id]);
        }

        Yii::$app->session->setFlash('error', Yii::t('fafcms-core', 'Error while saving {modelClass}!', [
            'modelClass' => $singular,
        ]) . '<br><br>' . implode('<br><br>', $mailing->getErrorSummary(true)));

        return $this->redirect(Yii::$app->getRequest()->getReferrer());
    }
}
