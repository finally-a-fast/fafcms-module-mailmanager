<?php

declare(strict_types=1);

/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 - 2021 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-mailmanager/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-mailmanager
 * @see https://www.finally-a-fast.com/packages/fafcms-module-mailmanager/docs Documentation of the module mail manager
 * @since File available since Release 1.0.0
 */

namespace fafcms\mailmanager;

use fafcms\fafcms\components\FafcmsComponent;
use fafcms\fafcms\inputs\ExtendedDropDownList;
use fafcms\helpers\abstractions\PluginModule;
use fafcms\helpers\classes\PluginSetting;
use fafcms\sitemanager\models\Contentmeta;
use Yii;

/**
 * Class Module
 *
 * @package fafcms\mailmanager
 */
class Module extends PluginModule
{
    public function getPluginSettingRules(): array
    {
        return [
            [['default_unsubscribe_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contentmeta::class, 'targetAttribute' => ['default_unsubscribe_id' => 'id']],
        ];
    }

    protected function pluginSettingDefinitions(): array
    {
        return [
            new PluginSetting($this, [
                'name' => 'default_unsubscribe_id',
                'label' => Yii::t('fafcms-core', 'Default unsubscribe site'),
                'inputType' => ExtendedDropDownList::class,
                'valueType' => FafcmsComponent::VALUE_TYPE_INT,
                'items' => static function($properties = []) {
                    return Contentmeta::getOptionProvider($properties)->getOptions();
                }
            ]),
        ];
    }
}
