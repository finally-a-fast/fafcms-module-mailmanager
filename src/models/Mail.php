<?php

namespace fafcms\mailmanager\models;

use fafcms\fafcms\items\ActionColumn;
use fafcms\fafcms\items\DataColumn;
use fafcms\fafcms\models\QueueHelper;
use fafcms\helpers\traits\MultilingualTrait;
use fafcms\mailmanager\abstracts\models\BaseMail;
use fafcms\mailmanager\jobs\SendMailJob;
use fafcms\parser\component\Parser;
use yii\data\ActiveDataProvider;
use yii\data\BaseDataProvider;
use yii\helpers\Json;
use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "{{%mail}}".
 *
 * @package fafcms\mailmanager\models
 */
class Mail extends BaseMail
{
    use MultilingualTrait;

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        $indexView = parent::indexView();

        //unset($indexView['default']['message_html'], $indexView['default']['message_text'], $indexView['default']['attachments'], $indexView['default']['data']);

        $indexView['quick'] = [
            'recipient_id' => [
                'class' => DataColumn::class,
                'settings' => [
                    'field' => 'recipient_id',
                    'sort' => 4,
                    'link' => true,
                ],
            ],
            'to' => [
                'class' => DataColumn::class,
                'settings' => [
                    'field' => 'to',
                    'sort' => 5,
                ],
            ],
            'sent_at' => [
                'class' => DataColumn::class,
                'settings' => [
                    'field' => 'sent_at',
                    'sort' => 15,
                ],
            ],
            'viewed' => [
                'class' => DataColumn::class,
                'settings' => [
                    'field' => 'viewed',
                    'sort' => 16,
                ],
            ],
            'clicked' => [
                'class' => DataColumn::class,
                'settings' => [
                    'field' => 'clicked',
                    'sort' => 20,
                ],
            ],
            'unsubscribed' => [
                'class' => DataColumn::class,
                'settings' => [
                    'field' => 'unsubscribed',
                    'sort' => 24,
                ],
            ],
            'action-column' => [
                'class' => ActionColumn::class,
            ],
        ];

        return $indexView;
    }
    //endregion IndexViewInterface implementation

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        $rules = parent::rules();

        //needed to send activation mails to an inactive recipient at the newsletter registration
        unset($rules['exist-recipient_id']);

        return $rules;
    }

    /**
     * @param int   $templateId
     * @param array $attributes
     * @param array $data
     *
     * @return static|null
     * @throws \yii\base\InvalidConfigException
     */
    public static function createByTemplate(int $templateId, array $attributes, array $data = []): ?self
    {
        $template = Template::find()->where(['id' => $templateId])->one();

        if ($template === null) {
            return null;
        }

        $to = $template->getParsedAttribute('to', $data);
        $from = $template->getParsedAttribute('from', $data);
        $cc = $template->getParsedAttribute('cc', $data);
        $bcc = $template->getParsedAttribute('bcc', $data);
        $replyto = $template->getParsedAttribute('replyto', $data);

        $mail = new self(array_merge([
            'template_id'  => $templateId,
            'to'           => (empty($to) ? '' : Json::encode($to)),
            'from'         => (empty($from) ? '' : Json::encode($from)),
            'cc'           => (empty($cc) ? '' : Json::encode($cc)),
            'bcc'          => (empty($bcc) ? '' : Json::encode($bcc)),
            'replyto'      => (empty($replyto) ? '' : Json::encode($replyto)),
            'subject'      => Yii::$app->fafcmsParser->parse(Parser::TYPE_TEXT, $template->subject, Parser::ROOT, $data),
            'message_html' => Yii::$app->fafcmsParser->parse(Parser::TYPE_HTML_MAIL, $template->message_html, Parser::ROOT, $data),
            'message_text' => Yii::$app->fafcmsParser->parse(Parser::TYPE_TEXT_MAIL, $template->message_text, Parser::ROOT, $data),
            'attachments'  => Json::encode(array_merge(Json::decode($template->attachments, true) ?? [], Yii::$app->fafcmsParser->data['attached-files'] ?? []), ),
            'embedments'   => Json::encode(Yii::$app->fafcmsParser->data['embeded-files'] ?? []),
        ], $attributes));

        return $mail;
    }

    /**
     * @param Mailing $mailing
     * @param array   $attributes
     * @param array $data
     *
     * @return static|null
     */
    public static function createByMailing(Mailing $mailing, array $attributes, array $data = []): ?self
    {
        if ($mailing === null) {
            return null;
        }

        $data = array_merge(Json::decode($mailing->data) ?? [], $data);

        $to = $mailing->getParsedAttribute('to', $data);

        if (empty($to)) {
            $to = $data['recipient']['email'];
        }

        $from = $mailing->getParsedAttribute('from', $data);
        $cc = $mailing->getParsedAttribute('cc', $data);
        $bcc = $mailing->getParsedAttribute('bcc', $data);
        $replyto = $mailing->getParsedAttribute('replyto', $data);

        $mail = new self(array_merge([
            'mailing_id'   => $mailing->id,
            'template_id'  => $mailing->template_id,
            'to'           => (empty($to) ? '' : Json::encode($to)),
            'from'         => (empty($from) ? '' : Json::encode($from)),
            'cc'           => (empty($cc) ? '' : Json::encode($cc)),
            'bcc'          => (empty($bcc) ? '' : Json::encode($bcc)),
            'replyto'      => (empty($replyto) ? '' : Json::encode($replyto)),
            'subject'      => Yii::$app->fafcmsParser->parse(Parser::TYPE_TEXT, $mailing->subject, Parser::ROOT, $data),
            'message_html' => Yii::$app->fafcmsParser->parse(Parser::TYPE_HTML_MAIL, $mailing->message_html, Parser::ROOT, $data),
            'message_text' => Yii::$app->fafcmsParser->parse(Parser::TYPE_TEXT_MAIL, $mailing->message_text, Parser::ROOT, $data),
            'attachments'  => Json::encode(array_merge(Json::decode($mailing->attachments, true) ?? [], Yii::$app->fafcmsParser->data['attached-files'] ?? []), ),
            'embedments'   => Json::encode(Yii::$app->fafcmsParser->data['embeded-files'] ?? []),
        ], $attributes));

        return $mail;
    }

    /**
     * @return bool
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\StaleObjectException
     */
    public function trackLinks(): bool
    {
        $this->message_html = preg_replace_callback('/track-href="([^"]*)"/mi', function($match) {
            $track = Track::findOrCreate([
                'mail_id' => $this->id,
                'mailing_id' => $this->mailing_id,
                'recipient_id' => $this->recipient_id,
                'type' => 1,
                'target' => $match[1],
            ]);

            if ($track !== null) {
                return 'href="' . Url::to(['/mailing/' . $track->hashId], true) . '"';
            }

            return 'href="' . $match[1] . '"';
        }, $this->message_html);

        $this->message_text = preg_replace_callback('/<track-href>([^<]*)<\/track-href>/mi', function($match) {
            $track = Track::findOrCreate([
                'mail_id' => $this->id,
                'mailing_id' => $this->mailing_id,
                'recipient_id' => $this->recipient_id,
                'type' => 1,
                'target' => $match[1],
            ]);

            if ($track !== null) {
                return Url::to(['/mailing/' . $track->hashId], true);
            }

            return $match[1];
        }, $this->message_text);

        return $this->save();
    }

    /**
     * @return bool
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\StaleObjectException
     */
    public function trackViews(): bool
    {
        $trackHtml = '';

        $track = Track::findOrCreate([
            'mail_id' => $this->id,
            'mailing_id' => $this->mailing_id,
            'recipient_id' => $this->recipient_id,
            'type' => 2
        ]);

        if ($track !== null) {
            $trackHtml .= '<img src="' . Url::to(['/mailing/' . $track->hashId], true) . '" height="1" width="1" />';
        }

        $this->message_html = str_ireplace('</body>', $trackHtml . '</body>', $this->message_html);

        return $this->save();
    }

    /**
     * @param bool $runValidation
     * @param null $attributeNames
     * @param bool $send
     *
     * @return bool
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\StaleObjectException
     */
    public function save($runValidation = true, $attributeNames = null, bool $send = true): bool
    {
        $result = parent::save($runValidation, $attributeNames);

        if ($result && $send && !$this->send()) {
            $this->delete();
            return false;
        }

        return $result;
    }

    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function send(): bool
    {
        return QueueHelper::runJob(SendMailJob::class, [
            'mailId' => $this->id
        ], 512);
    }

    //region search
    /**
     * {@inheritdoc}
     */
    public function searchRules(): array
    {
        return [
            [['id', 'mailing_id', 'template_id', 'recipient_id', 'viewed', 'view_count', 'clicked', 'click_count', 'unsubscribed', 'bounced', 'created_by', 'updated_by'], 'integer', 'on' => static::SCENARIO_SEARCH],
            [['to', 'cc', 'bcc', 'from', 'replyto', 'subject', 'message_html', 'message_text', 'attachments', 'embedments', 'sent_at', 'first_view_at', 'last_view_at', 'first_click_at', 'last_click_at', 'unsubscribed_at', 'bounced_at', 'bounce_reason', 'created_at', 'updated_at'], 'safe', 'on' => static::SCENARIO_SEARCH],
        ];
    }

    /**
     * @param array $params
     *
     * @return BaseDataProvider
     * @throws \yii\base\InvalidConfigException
     */
    public function search(array $params): BaseDataProvider
    {
        if (!$this->searchInternal($dataProvider, $params)) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $dataProvider->query->andFilterWhere([
            'id' => $this->id,
            'mailing_id' => $this->mailing_id,
            'template_id' => $this->template_id,
            'recipient_id' => $this->recipient_id,
            'sent_at' => $this->sent_at,
            'viewed' => $this->viewed,
            'view_count' => $this->view_count,
            'first_view_at' => $this->first_view_at,
            'last_view_at' => $this->last_view_at,
            'clicked' => $this->clicked,
            'click_count' => $this->click_count,
            'first_click_at' => $this->first_click_at,
            'last_click_at' => $this->last_click_at,
            'unsubscribed' => $this->unsubscribed,
            'unsubscribed_at' => $this->unsubscribed_at,
            'bounced' => $this->bounced,
            'bounced_at' => $this->bounced_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $dataProvider->query->andFilterWhere(['like', 'to', $this->to])
            ->andFilterWhere(['like', 'cc', $this->cc])
            ->andFilterWhere(['like', 'bcc', $this->bcc])
            ->andFilterWhere(['like', 'from', $this->from])
            ->andFilterWhere(['like', 'replyto', $this->replyto])
            ->andFilterWhere(['like', 'subject', $this->subject])
            ->andFilterWhere(['like', 'message_html', $this->message_html])
            ->andFilterWhere(['like', 'message_text', $this->message_text])
            ->andFilterWhere(['like', 'attachments', $this->attachments])
            ->andFilterWhere(['like', 'embedments', $this->embedments])
            ->andFilterWhere(['like', 'bounce_reason', $this->bounce_reason]);

        /**
         * @var $dataProvider ActiveDataProvider
         */
        $dataProvider->query->orderBy(['id' => SORT_DESC]);

        return $dataProvider;
    }
    //endregion search
}
