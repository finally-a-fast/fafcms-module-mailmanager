<?php

namespace fafcms\mailmanager\abstracts\models;

use fafcms\fafcms\{
    inputs\DateTimePicker,
    inputs\ExtendedDropDownList,
    inputs\NumberInput,
    inputs\SwitchCheckbox,
    items\ActionColumn,
    items\Card,
    items\Column,
    items\DataColumn,
    items\FormField,
    items\Row,
    items\Tab,
    models\User,
};
use fafcms\helpers\{
    ActiveRecord,
    classes\OptionProvider,
    interfaces\EditViewInterface,
    interfaces\FieldConfigInterface,
    interfaces\IndexViewInterface,
    traits\AttributeOptionTrait,
    traits\BeautifulModelTrait,
    traits\OptionProviderTrait,
};
use fafcms\mailmanager\{
    Bootstrap,
    models\Interest,
    models\Mailing,
};
use Yii;
use yii\db\ActiveQuery;
use yii\validators\DateValidator;

/**
 * This is the abstract model class for table "{{%mailing_interest}}".
 *
 * @package fafcms\mailmanager\abstracts\models
 *
 * @property-read array $fieldConfig
 *
 * @property int $id
 * @property int $has_not
 * @property int $mailing_id
 * @property int $interest_id
 * @property int|null $created_by
 * @property string|null $created_at
 *
 * @property User $createdBy
 * @property Interest $interest
 * @property Mailing $mailing
 */
abstract class BaseMailingInterest extends ActiveRecord implements FieldConfigInterface, IndexViewInterface, EditViewInterface
{
    use BeautifulModelTrait;
    use OptionProviderTrait;
    use AttributeOptionTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return Bootstrap::$id . '/mailinginterest';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return  'mailinginterest';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-mailmanager', 'MailingInterests');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-mailmanager', 'MailingInterest');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['id'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionProviderTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptionProvider(array $properties = []): OptionProvider
    {
        return (new OptionProvider(static::class))
            ->setSelect([
                static::tableName() . '.id',
                static::tableName() . '.id'
            ])
            ->setSort([static::tableName() . '.id' => SORT_ASC])
            ->setItemLabel(static function ($item) {
                return static::extendedLabel($item);
            })
            ->setProperties($properties);
    }
    //endregion OptionProviderTrait implementation

    //region AttributeOptionTrait implementation
    /**
     * @inheritDoc
     */
    public function attributeOptions(): array
    {
        return [
            'mailing_id' => static function($properties = []) {
                return Mailing::getOptionProvider($properties)->getOptions();
            },
            'interest_id' => static function($properties = []) {
                return Interest::getOptionProvider($properties)->getOptions();
            },
            'created_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
        ];
    }
    //endregion AttributeOptionTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'id' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'has_not' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'mailing_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('mailing_id', false),
                'relationClassName' => Mailing::class,
            ],
            'interest_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('interest_id', false),
                'relationClassName' => Interest::class,
            ],
            'created_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('created_by', false),
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'created_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
        ];
    }
    //endregion FieldConfigInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        return [
            'default' => [
                'id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'id',
                        'sort' => 1,
                        'link' => true,
                    ],
                ],
                'has_not' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'has_not',
                        'sort' => 2,
                    ],
                ],
                'mailing_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'mailing_id',
                        'sort' => 3,
                        'link' => true,
                    ],
                ],
                'interest_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'interest_id',
                        'sort' => 4,
                        'link' => true,
                    ],
                ],
                'action-column' => [
                    'class' => ActionColumn::class,
                ],
            ]
        ];
    }
    //endregion IndexViewInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-has_not' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'has_not',
                                                    ],
                                                ],
                                                'field-mailing_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'mailing_id',
                                                    ],
                                                ],
                                                'field-interest_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'interest_id',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%mailing_interest}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'boolean-has_not' => ['has_not', 'boolean'],
            'required-mailing_id' => ['mailing_id', 'required'],
            'required-interest_id' => ['interest_id', 'required'],
            'integer-mailing_id' => ['mailing_id', 'integer'],
            'integer-interest_id' => ['interest_id', 'integer'],
            'integer-created_by' => ['created_by', 'integer'],
            'date-created_at' => ['created_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'created_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'exist-created_by' => [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            'exist-interest_id' => [['interest_id'], 'exist', 'skipOnError' => true, 'targetClass' => Interest::class, 'targetAttribute' => ['interest_id' => 'id']],
            'exist-mailing_id' => [['mailing_id'], 'exist', 'skipOnError' => true, 'targetClass' => Mailing::class, 'targetAttribute' => ['mailing_id' => 'id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'id' => Yii::t('fafcms-mailmanager', 'ID'),
            'has_not' => Yii::t('fafcms-mailmanager', 'Has Not'),
            'mailing_id' => Yii::t('fafcms-mailmanager', 'Mailing ID'),
            'interest_id' => Yii::t('fafcms-mailmanager', 'Interest ID'),
            'created_by' => Yii::t('fafcms-mailmanager', 'Created By'),
            'created_at' => Yii::t('fafcms-mailmanager', 'Created At'),
        ]);
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return ActiveQuery
     */
    public function getCreatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'created_by',
        ]);
    }

    /**
     * Gets query for [[Interest]].
     *
     * @return ActiveQuery
     */
    public function getInterest(): ActiveQuery
    {
        return $this->hasOne(Interest::class, [
            'id' => 'interest_id',
        ]);
    }

    /**
     * Gets query for [[Mailing]].
     *
     * @return ActiveQuery
     */
    public function getMailing(): ActiveQuery
    {
        return $this->hasOne(Mailing::class, [
            'id' => 'mailing_id',
        ]);
    }
}
