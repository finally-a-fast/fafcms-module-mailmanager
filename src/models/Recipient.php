<?php

namespace fafcms\mailmanager\models;

use fafcms\fafcms\items\ActionColumn;
use fafcms\fafcms\items\DataColumn;
use fafcms\helpers\traits\MultilingualTrait;
use fafcms\mailmanager\abstracts\models\BaseRecipient;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\BaseDataProvider;

/**
 * This is the model class for table "{{%recipient}}".
 *
 * @package fafcms\mailmanager\models
 */
class Recipient extends BaseRecipient
{
    use MultilingualTrait;

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        $indexView = parent::indexView();

        //unset($indexView['default']['message_html'], $indexView['default']['message_text'], $indexView['default']['attachments'], $indexView['default']['data']);

        $indexView['quick'] = [
            'status' => [
                'class' => DataColumn::class,
                'settings' => [
                    'field' => 'status',
                    'sort' => 2,
                ],
            ],
            'firstname' => [
                'class' => DataColumn::class,
                'settings' => [
                    'field' => 'firstname',
                    'sort' => 3,
                    'link' => true,
                ],
            ],
            'lastname' => [
                'class' => DataColumn::class,
                'settings' => [
                    'field' => 'lastname',
                    'sort' => 4,
                    'link' => true,
                ],
            ],
            'email' => [
                'class' => DataColumn::class,
                'settings' => [
                    'field' => 'email',
                    'sort' => 6,
                ],
            ],
            'sex' => [
                'class' => DataColumn::class,
                'settings' => [
                    'field' => 'sex',
                    'sort' => 7,
                ],
            ],
            'requested' => [
                'class' => DataColumn::class,
                'settings' => [
                    'field' => 'requested',
                    'sort' => 8,
                ],
            ],
            'confirmed' => [
                'class' => DataColumn::class,
                'settings' => [
                    'field' => 'confirmed',
                    'sort' => 12,
                ],
            ],
            'action-column' => [
                'class' => ActionColumn::class,
            ],
        ];

        return $indexView;
    }
    //endregion IndexViewInterface implementation

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return 'account-box-outline';
    }
    //endregion BeautifulModelTrait implementation

    public function attributeOptions(): array
    {
        $attributeOptions = parent::attributeOptions();

        $attributeOptions['sex'] = [
            'female' => Yii::t('fafcms-mailmanager', 'Miss'),
            'male' => Yii::t('fafcms-mailmanager', 'Mister'),
            'misc' => Yii::t('fafcms-mailmanager', 'Miscellaneous'),
        ];

        return $attributeOptions;
    }

    public function getSalutation(): string
    {
        return $this->getAttributeOptions('sex')[$this->sex] ?? '';
    }

    //region search
    /**
     * {@inheritdoc}
     */
    public function searchRules(): array
    {
        return [
            [
                ['id', 'recipientlist_id', 'requested', 'requested_mail_id', 'confirmed', 'confirmed_mail_id', 'created_by', 'updated_by', 'activated_by', 'deactivated_by', 'deleted_by'],
                'integer', 'on' => static::SCENARIO_SEARCH
            ],
            [
                ['status', 'email', 'sex', 'firstname', 'lastname', 'requested_at', 'requested_data', 'confirmed_at', 'confirmed_data', 'created_at', 'updated_at', 'activated_at', 'deactivated_at', 'deleted_at'],
                'safe', 'on' => static::SCENARIO_SEARCH
            ],
        ];
    }

    /**
     * @param array $params
     *
     * @return BaseDataProvider
     * @throws \yii\base\InvalidConfigException
     */
    public function search(array $params): BaseDataProvider
    {
        if (!$this->searchInternal($dataProvider, $params)) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $dataProvider->query->andFilterWhere([
            'id' => $this->id,
            'recipientlist_id' => $this->recipientlist_id,
            'requested' => $this->requested,
            'requested_at' => $this->requested_at,
            'requested_mail_id' => $this->requested_mail_id,
            'confirmed' => $this->confirmed,
            'confirmed_at' => $this->confirmed_at,
            'confirmed_mail_id' => $this->confirmed_mail_id,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'activated_by' => $this->activated_by,
            'deactivated_by' => $this->deactivated_by,
            'deleted_by' => $this->deleted_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'activated_at' => $this->activated_at,
            'deactivated_at' => $this->deactivated_at,
            'deleted_at' => $this->deleted_at,
        ]);

        $dataProvider->query->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'sex', $this->sex])
            ->andFilterWhere(['like', 'firstname', $this->firstname])
            ->andFilterWhere(['like', 'lastname', $this->lastname])
            ->andFilterWhere(['like', 'requested_data', $this->requested_data])
            ->andFilterWhere(['like', 'confirmed_data', $this->confirmed_data]);

        /**
         * @var $dataProvider ActiveDataProvider
         */
        $dataProvider->query->orderBy(['id' => SORT_DESC]);

        return $dataProvider;
    }
    //endregion search
}
