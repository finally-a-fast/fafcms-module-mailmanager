<?php

declare(strict_types=1);

/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 - 2021 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-mailmanager/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-mailmanager
 * @see https://www.finally-a-fast.com/packages/fafcms-module-mailmanager/docs Documentation of the module mail manager
 * @since File available since Release 1.0.0
 */

namespace fafcms\mailmanager\elements;

use DateTime;
use DateTimeZone;
use Faf\TemplateEngine\Helpers\ElementSetting;
use Faf\TemplateEngine\Helpers\ParserElement;
use fafcms\mailmanager\models\Recipient;
use fafcms\mailmanager\models\Recipientlist;
use fafcms\mailmanager\models\SubscribeForm;
use fafcms\sitemanager\models\Contentmeta;
use Yii;
use yii\helpers\Json;
use Yiisoft\Validator\Rule\Number;
use Yiisoft\Validator\Rule\Required;

/**
 * Class NewsletterSubscribeForm
 *
 * @package fafcms\mailmanager\elements
 */
class NewsletterSubscribeForm extends ParserElement
{
    /**
     * {@inheritdoc}
     */
    public function name(): string
    {
        return 'newsletter-subscribe-form';
    }

    /**
     * {@inheritdoc}
     */
    public function description(): string
    {
        return Yii::t('fafcms-mailmanager', 'Shows the newsletter subscribe form.');
    }

    /**
     * {@inheritdoc}
     */
    public function elementSettings(): array
    {
        return [
            new ElementSetting([
                'name' => 'recipientlist-id',
                'label' => Yii::t('fafcms-mailmanager', 'Recipientlist'),
                'rules' => [
                    new Required()
                ]
            ]),
            new ElementSetting([
                'name' => 'active-form-class',
                'label' => Yii::t('fafcms-mailmanager', 'Active form class'),
                'defaultValue' => 'fafcms\\fafcms\\widgets\\ActiveForm',
                'rules' => [
                    new Required()
                ]
            ]),
            new ElementSetting([
                'name' => 'submit-button-label',
                'label' => Yii::t('fafcms-mailmanager', 'Submit button label'),
                'defaultValue' => ['fafcms-mailmanager', 'Subscribe newsletter'],
                'rules' => [
                    new Required()
                ]
            ]),
            new ElementSetting([
                'name' => 'submit-button-css-class',
                'label' => Yii::t('fafcms-mailmanager', 'Submit button css class'),
            ]),
            new ElementSetting([
                'name' => 'privacy-policy-service-name',
                'label' => Yii::t('fafcms-mailmanager', 'Privacy policy service name'),
            ]),
            new ElementSetting([
                'name' => 'privacy-policy-mail-contact',
                'label' => Yii::t('fafcms-mailmanager', 'Privacy policy mail contact'),
            ]),
            new ElementSetting([
                'name' => 'privacy-policy-privacy-page',
                'label' => Yii::t('fafcms-mailmanager', 'Privacy policy privacy page (link or content meta id)'),
            ]),
            new ElementSetting([
                'name' => 'success-message',
                'label' => Yii::t('fafcms-mailmanager', 'Success message'),
                'content' => true,
                'defaultValue' => Yii::t('fafcms-mailmanager', 'To secure your data, you will receive an email from us shortly, which contains a confirmation link. Please click on the confirmation link for final registration.'),
                'rules' => [
                    new Required()
                ]
            ]),
            new ElementSetting([
                'name' => 'already-subscribed-mail-subject',
                'label' => Yii::t('fafcms-mailmanager', 'Already subscribed mail subject'),
                'defaultValue' => Yii::t('fafcms-mailmanager', 'Newsletter subscription') .  ' - ' . Yii::$app->fafcms->getCurrentProjectLanguage()->site_name,
                'rules' => [
                    new Required()
                ]
            ]),
            new ElementSetting([
                'name' => 'already-subscribed-mail-template',
                'label' => Yii::t('fafcms-mailmanager', 'Already subscribed mail template id'),
                'rules' => [
                    new Required(),
                    new Number()
                ]
            ]),
            new ElementSetting([
                'name' => 'already-subscribed-change-page',
                'label' => Yii::t('fafcms-mailmanager', 'Already subscribed change page (link or content meta id)'),
            ]),
            new ElementSetting([
                'name' => 'new-subscription-mail-subject',
                'label' => Yii::t('fafcms-mailmanager', 'New subscription mail subject'),
                'defaultValue' => Yii::t('fafcms-mailmanager', 'Newsletter subscription') .  ' - ' . Yii::$app->fafcms->getCurrentProjectLanguage()->site_name,
                'rules' => [
                    new Required()
                ]
            ]),
            new ElementSetting([
                'name' => 'new-subscription-mail-template',
                'label' => Yii::t('fafcms-mailmanager', 'New subscription mail template id'),
                'rules' => [
                    new Required(),
                    new Number()
                ]
            ]),
            new ElementSetting([
                'name' => 'new-subscription-confirm-page',
                'label' => Yii::t('fafcms-mailmanager', 'Subscription confirmed change page (link or content meta id)'),
            ]),
        ];
    }

    /**
     * @param Recipientlist $recipientlist
     *
     * @return SubscribeForm
     * @throws \yii\base\InvalidConfigException
     */
    private function createModel(Recipientlist $recipientlist): SubscribeForm
    {
        $privacyPage = $this->data['privacy-policy-privacy-page'];

        if (is_numeric($privacyPage)) {
            $privacyPage = '/' . Contentmeta::getUrlById($privacyPage);
        }

        $alreadySubscribedChangePage = $this->data['already-subscribed-change-page'];

        if (is_numeric($alreadySubscribedChangePage)) {
            $alreadySubscribedChangePage = '/' . Contentmeta::getUrlById($alreadySubscribedChangePage);
        }

        $newSubscriptionConfirmPage = $this->data['new-subscription-confirm-page'];

        if (is_numeric($newSubscriptionConfirmPage)) {
            $newSubscriptionConfirmPage = '/' . Contentmeta::getUrlById($newSubscriptionConfirmPage);
        }

        return new SubscribeForm([
            'privacyPolicyServiceName'      => $this->data['privacy-policy-service-name'],
            'privacyPolicyMailContact'      => $this->data['privacy-policy-mail-contact'],
            'privacyPolicyPrivacyPage'      => $privacyPage,
            'recipientlist'                 => $recipientlist,
            'alreadySubscribedMailSubject'  => $this->data['already-subscribed-mail-subject'],
            'alreadySubscribedMailTemplate' => $this->data['already-subscribed-mail-template'],
            'alreadySubscribedChangePage'   => $alreadySubscribedChangePage,
            'newSubscriptionMailSubject'    => $this->data['new-subscription-mail-subject'],
            'newSubscriptionMailTemplate'   => $this->data['new-subscription-mail-template'],
            'newSubscriptionConfirmPage'    => $newSubscriptionConfirmPage,
        ]);
    }

    /**
     * {@inheritdoc}
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        $recipientlist = Recipientlist::find()->where(['id' => $this->data['recipientlist-id']])->one();

        if ($recipientlist === null) {
            return '';
        }

        $model = $this->createModel($recipientlist);

        $content = '';

        if (Yii::$app->request->get('action') === 'confirm') {
            $id = Yii::$app->request->get('id');

            if ($id !== null && ($recipient = Recipient::find()->where(['hashId' => $id])->byStatus('all')->one()) !== null) {
                if ($recipient->confirmed === 1) {
                    Yii::$app->session->setFlash('warning', Yii::t('fafcms-mailmanager', 'Your newsletter subscription has already been confirmed'));
                } else {
                    $recipient->status = 'active';
                    $recipient->confirmed = 1;
                    $recipient->confirmed_at = (new DateTime('NOW', new DateTimeZone(Yii::$app->formatter->defaultTimeZone)))->format('Y-m-d H:i:s');
                    $recipient->confirmed_data = Json::encode(['get' => $_GET, 'server' => $_SERVER]);

                    if ($recipient->save()) {
                        Yii::$app->session->setFlash('success', '<b>' . Yii::t('fafcms-mailmanager', 'Thank you!') . '</b><br>' . Yii::t('fafcms-mailmanager', 'Your confirmation was successful.'));
                    } else {
                        Yii::$app->session->setFlash('error', Yii::t('fafcms-core', 'An error has occurred, please try again.') . '<br><br>' . implode('<br><br>', $recipient->getErrorSummary(true)));
                    }
                }

                Yii::$app->view->renderedSiteMode = 'raw';
                Yii::$app->view->renderedSiteContent = Yii::$app->controller->redirect(Yii::$app->fafcmsParser->data['currentContentmeta']->getRelativeUrl());
                return '';
            }

            Yii::$app->session->setFlash('error', Yii::t('fafcms-core', 'An error has occurred, please try again.'));
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                if ($model->save()) {
                    $content .= $this->data['success-message'];
                } else {
                    Yii::$app->session->setFlash('error', Yii::t('fafcms-core', 'An error has occurred, please try again.'));
                }
            } else {
                Yii::$app->session->setFlash('error', Yii::t('fafcms-core', 'An error has occurred, please try again.') . '<br><br>' . implode('<br><br>', $model->getErrorSummary(true)));
            }
        }

        return $content . Yii::$app->view->render('@fafcms/mailmanager/views/subscribe', [
            'model'                => $model,
            'headline'             => Yii::t('fafcms-mailmanager', 'Subscribe newsletter'),
            'required'             => Yii::t('fafcms-mailmanager', 'Mandatory fields'),
            'formId'               => 'subscribe-form-' . $recipientlist->hashId,
            'activeFormClass'      => $this->data['active-form-class'],
            'submitButtonLabel'    => Yii::$app->fafcms->getTextOrTranslation($this->data['submit-button-label']),
            'submitButtonCssClass' => $this->data['submit-button-css-class'],
        ]);
    }
}
