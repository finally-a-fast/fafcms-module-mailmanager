<?php

namespace fafcms\mailmanager\models;

use fafcms\fafcms\inputs\AceEditor;
use fafcms\fafcms\items\Card;
use fafcms\fafcms\items\Column;
use fafcms\fafcms\items\FormField;
use fafcms\fafcms\items\Row;
use fafcms\fafcms\items\Tab;
use fafcms\helpers\traits\MultilingualTrait;
use fafcms\mailmanager\abstracts\models\BaseTemplate;
use fafcms\parser\component\Parser;
use Yii;

/**
 * This is the model class for table "{{%template}}".
 *
 * @package fafcms\mailmanager\models
 */
class Template extends BaseTemplate
{
    use MultilingualTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return 'image-text';
    }
    //endregion BeautifulModelTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        $fieldConfig = parent::getFieldConfig();

        $fieldConfig['message_html'] = [
            'type' => AceEditor::class,
        ];

        $fieldConfig['message_text'] = [
            'type' => AceEditor::class,
        ];

        return $fieldConfig;
    }
    //endregion FieldConfigInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        $indexView = parent::indexView();

        unset($indexView['default']['message_html'], $indexView['default']['message_text'], $indexView['default']['attachments']);

        return $indexView;
    }
    //endregion IndexViewInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        $editView = parent::editView();

        $editView['default'] = [
            'buttons' => [
                static function($buttons, $model) {
                    $buttons['create-mailing'] = [
                        'icon' => 'email-multiple-outline',
                        'label' => ['fafcms-mailmanager', 'Create Mailing'],
                        'url' => ['create-mailing', 'id' => $model->id],
                    ];

                    return $buttons;
                }
            ],
            'contents' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 6,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-status' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'status',
                                                    ],
                                                ],
                                                'field-name' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'name',
                                                    ],
                                                ],
                                                'field-subject' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'subject',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                                'column-3' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 5,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-mailmanager',
                                                    'Tracking',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-track_view' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'track_view',
                                                    ],
                                                ],
                                                'field-track_click' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'track_click',
                                                    ],
                                                ],
                                                'field-unsubscribe_contentmeta_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'unsubscribe_contentmeta_id',
                                                    ],
                                                ],
                                                'field-config_contentmeta_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'config_contentmeta_id',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        'row-2' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-mailmanager',
                                                    'To',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-to' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'to',
                                                    ],
                                                ],
                                                'field-cc' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'cc',
                                                    ],
                                                ],
                                                'field-bcc' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'bcc',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                                'column-2' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-mailmanager',
                                                    'From',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-from' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'from',
                                                    ],
                                                ],
                                                'field-replyto' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'replyto',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        'row-3' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 16,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-mailmanager',
                                                    'Html',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-message_html' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'message_html',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        'row-4' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 16,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-mailmanager',
                                                    'Text',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-message_text' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'message_text',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        'row-5' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 16,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-mailmanager',
                                                    'Attachments',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-attachments' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'attachments',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];

        return $editView;
    }
    //endregion EditViewInterface implementation

    /**
     * @param string $attribute
     * @param array  $data
     *
     * @return array|null|string
     */
    public function getParsedAttribute(string $attribute, array $data = [])
    {
        $parsedAttribute = [];
        $parsedAttributeDatas = Yii::$app->fafcmsParser->parse(Parser::TYPE_TEXT, $this->$attribute, Parser::ROOT, $data, null, true);

        if ($parsedAttributeDatas !== null && is_countable($parsedAttributeDatas)) {
            foreach ($parsedAttributeDatas as $index => $parsedAttributeData) {
                if ($parsedAttributeData !== null) {
                    if (is_string($parsedAttributeData)) {
                        $parsedAttributeData = [$index => $parsedAttributeData];
                    }

                    $parsedAttribute[] = $parsedAttributeData;
                }
            }

            return array_merge(...$parsedAttribute);
        }

        return $parsedAttributeDatas;
    }
}
