<?php

declare(strict_types=1);

/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 - 2021 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-mailmanager/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-mailmanager
 * @see https://www.finally-a-fast.com/packages/fafcms-module-mailmanager/docs Documentation of the module mail manager
 * @since File available since Release 1.0.0
 */

namespace fafcms\mailmanager;

use fafcms\fafcms\components\FafcmsComponent;
use fafcms\helpers\abstractions\PluginBootstrap;
use fafcms\helpers\abstractions\PluginModule;
use fafcms\mailmanager\models\Blacklist;
use fafcms\mailmanager\models\Mail;
use fafcms\mailmanager\models\Mailing;
use fafcms\mailmanager\models\Recipientlist;
use fafcms\mailmanager\models\Template;
use yii\base\Application;
use yii\base\BootstrapInterface;
use yii\i18n\PhpMessageSource;
use Yii;

/**
 * Class Bootstrap
 *
 * @package fafcms\mailmanager
 */
class Bootstrap extends PluginBootstrap implements BootstrapInterface
{
    public static $id = 'fafcms-mailmanager';
    public static $tablePrefix = 'fafcms-mail_';

    protected function bootstrapTranslations(Application $app, PluginModule $module): bool
    {
        if (!isset($app->i18n->translations['fafcms-mailmanager'])) {
            $app->i18n->translations['fafcms-mailmanager'] = [
                'class' => PhpMessageSource::class,
                'basePath' => __DIR__ . '/messages',
                'forceTranslation' => true,
            ];
        }

        return true;
    }

    protected function bootstrapWebApp(Application $app, PluginModule $module): bool
    {
        Yii::$app->view->addNavigationItems(
            FafcmsComponent::NAVIGATION_SIDEBAR_LEFT,
            [
                'mails' => [
                    'after' => 'files',
                    'type' => 'group',
                    'label' => ['fafcms-mailmanager', 'Mail Management'],
                    'items' => [
                        'blacklists'     => [
                            'icon'  => Blacklist::instance()->getEditData()['icon'],
                            'label' => Blacklist::instance()->getEditData()['plural'],
                            'url'   => ['/' . Blacklist::instance()->getEditData()['url'] . '/index'],
                        ],
                        'recipientlists' => [
                            'icon'  => Recipientlist::instance()->getEditData()['icon'],
                            'label' => Recipientlist::instance()->getEditData()['plural'],
                            'url'   => ['/' . Recipientlist::instance()->getEditData()['url'] . '/index'],
                        ],
                        'templates'      => [
                            'icon'  => Template::instance()->getEditData()['icon'],
                            'label' => Template::instance()->getEditData()['plural'],
                            'url'   => ['/' . Template::instance()->getEditData()['url'] . '/index'],
                        ],
                        'mailingss'          => [
                            'icon'  => Mailing::instance()->getEditData()['icon'],
                            'label' => Mailing::instance()->getEditData()['plural'],
                            'url'   => ['/' . Mailing::instance()->getEditData()['url'] . '/index'],
                        ],
                        'mails'          => [
                            'icon'  => Mail::instance()->getEditData()['icon'],
                            'label' => Mail::instance()->getEditData()['plural'],
                            'url'   => ['/' . Mail::instance()->getEditData()['url'] . '/index'],
                        ],
                    ],
                ],
            ]
        );

        $app->fafcms->addBackendUrlRules(self::$id, [
            '<controller:[a-zA-Z0-9\-]+>/<action:[a-zA-Z0-9\-]+>/<id:[a-zA-Z0-9\-\_]+>' => '<controller>/<action>',
            '<controller:[a-zA-Z0-9\-]+>/<action:[a-zA-Z0-9\-]+>' => '<controller>/<action>',
        ]);

        $app->fafcms->addFrontendUrlRules(self::$id, [
            'mailing/<id:[a-zA-Z0-9\-\_]+>' => 'track/track',
        ], false);

        return true;
    }
}
