<?php

namespace fafcms\mailmanager\models;

use fafcms\helpers\traits\MultilingualTrait;
use fafcms\mailmanager\abstracts\models\BaseInterest;

/**
 * This is the model class for table "{{%interest}}".
 *
 * @package fafcms\mailmanager\models
 */
class Interest extends BaseInterest
{
    use MultilingualTrait;

}
