<?php

declare(strict_types=1);

/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 - 2021 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-mailmanager/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-mailmanager
 * @see https://www.finally-a-fast.com/packages/fafcms-module-mailmanager/docs Documentation of the module mail manager
 * @since File available since Release 1.0.0
 */

namespace fafcms\mailmanager\elements;

use Faf\TemplateEngine\Elements\TagAttribute;
use Faf\TemplateEngine\Helpers\ElementSetting;
use Faf\TemplateEngine\Helpers\ParserElement;
use fafcms\filemanager\controllers\FilemanagerController;
use fafcms\filemanager\models\File;
use fafcms\filemanager\models\Filetype;
use fafcms\parser\component\Parser;
use JsonException;
use Yiisoft\Validator\Rule\Number;
use Yiisoft\Validator\Rule\Required;
use Yii;

/**
 * Class EmbedImage
 *
 * @package fafcms\mailmanager\elements
 */
class EmbedImage extends ParserElement
{
    /**
     * {@inheritdoc}
     */
    public function name(): string
    {
        return 'embed-image';
    }

    /**
     * {@inheritdoc}
     */
    public function description(): string
    {
        return 'Embeds an image.';
    }

    /**
     * {@inheritdoc}
     */
    public function elementSettings(): array
    {
        return [
            new ElementSetting([
                'name' => 'src',
                'label' => 'Src',
                'element' => EmbedImageSrc::class,
                'rules' => [
                    new Required(),
                    new Number()
                ]
            ]),
            new ElementSetting([
                'name' => 'name',
                'label' => 'Name',
                'element' => EmbedImageName::class,
                'defaultValue' => 'Mail',
                'rules' => [
                    new Required()
                ]
            ]),
            new ElementSetting([
                'name' => 'variation',
                'label' => 'Variation',
                'element' => EmbedImageVariation::class
            ]),
            new ElementSetting([
                'name' => 'attributes',
                'label' => 'Attributes',
                'element' => TagAttribute::class,
                'rawData' => true,
                'attributeNameAsKey' => true,
                'multiple' => true,
                'multipleAttributeExpression' => '/^(.*)?$/i',
            ])
        ];
    }

    /**
     * {@inheritdoc}
     * @return string
     * @throws JsonException
     */
    public function run(): string
    {
        $options = $this->data['attributes'];
        unset($options['src'], $options['name'], $options['variation']);

        if (($fileModel = File::find()->where(['id' => $this->data['src']])->one()) !== null) {
            $meta = File::loadMeta($fileModel);

            if (!isset($meta['width'], $meta['height'])) {
                return '';
            }

            $originalWidth = $meta['width'];
            $originalHeight = $meta['height'];
            $aspect = $originalWidth / $originalHeight;

            if (!empty($this->data['variation'] ?? '')) {
                $rawImageFormat = explode(',', $this->data['variation']);
            } else {
                $rawImageFormat = [$originalWidth, $originalHeight];
            }

            $imageFormat = File::getImageFormat($rawImageFormat, $aspect);
            $fileVariation = FilemanagerController::getFileVariation([
                $this->data['name'],
                $imageFormat['width'],
                $imageFormat['height'],
                $imageFormat['options'],
            ], $fileModel, true);

            if ($fileVariation !== null) {
                $articleImagePath = $fileVariation['name'];

                if (!file_exists($articleImagePath)) {
                    $articleImagePath = null;
                }

                $id = $fileVariation['fileformatvariation']['id'];
                $options['src'] = '#fafcms-embed-image#' . $id . '#fafcms-embed-image#';

                $filetypes = Yii::$app->dataCache->index(Filetype::class, 'id');

                Yii::$app->fafcmsParser->data['embeded-files'][$id] = [
                    'file' => $articleImagePath,
                    'name' => $fileModel['filename'] . '.' . $filetypes[$fileModel->filetype_id]['default_extension'],
                    'type' => $filetypes[$fileModel->filetype_id]['mime_type']
                ];

                return $this->parser->htmlTag('img', '', $options);
            }
        }

        return '';
    }

    public function allowedTypes(): ?array
    {
        return [Parser::TYPE_HTML_MAIL];
    }
}
