<?php

namespace fafcms\mailmanager\controllers;

use DateTime;
use DateTimeZone;
use fafcms\fafcms\models\Projectlanguage;
use fafcms\mailmanager\models\Mail;
use fafcms\mailmanager\models\Mailing;
use fafcms\mailmanager\models\Recipient;
use fafcms\mailmanager\models\RecipientInterest;
use fafcms\sitemanager\models\Contentmeta;
use yii\console\Controller;
use Yii;
use yii\base\Exception;
use yii\db\Expression;
use yii\helpers\Url;

class CronController extends Controller
{
    protected function getNow()
    {
        return (new DateTime('NOW', new DateTimeZone(Yii::$app->formatter->defaultTimeZone)))->format('Y-m-d H:i:s');
    }

    /**
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     * @throws Exception
     */
    public function actionSendMailings()
    {
        $mailing = Mailing::find()->where([
            'AND',
            [
                'start_dispatch' => 1,
                'status' => 'active',
                'sent_at' => null
            ],
            [
                'OR',
                ['<=', 'start_dispatch_at', $this->getNow()],
                ['start_dispatch_at' => null,]
            ],
        ])->one();

        if ($mailing === null) {
            return;
        }

        $mailing->sent_at = $this->getNow();

        if ($mailing->validate() && $mailing->save()) {
            $domain = Projectlanguage::find()->where(['id' => $mailing->projectlanguage_id])->one()->domain;
            $baseUrl = 'http' . ($domain->force_https === 1 ? 's' : '') . '://' . $domain->domain;

            Yii::$app->urlManager->baseUrl = $baseUrl;

            $filter = [
                'status' => 'active',
                'recipientlist_id' => $mailing->recipientlist_id
            ];

            $recipients = Recipient::find()->where($filter);

            $hasInterests = array_filter($mailing->mailingMailingInterests, static function($item) {
                return $item['has_not'] === 0;
            });

            foreach ($hasInterests as $hasInterest) {
                $recipients->andWhere(['=', RecipientInterest::find()->select('COUNT(*)')->where(['interest_id' => $hasInterest->interest_id, 'recipient_id' => new Expression(Recipient::tableName() . '.id')]), 1]);
            }

            $hasNotInterests = array_filter($mailing->mailingMailingInterests, static function($item) {
                return $item['has_not'] === 1;
            });

            foreach ($hasNotInterests as $hasNotInterest) {
                $recipients->andWhere(['=', RecipientInterest::find()->select('COUNT(*)')->where(['interest_id' => $hasNotInterest->interest_id, 'recipient_id' => new Expression(Recipient::tableName() . '.id')]), 0]);
            }

            $recipients = $recipients->all();

            $transaction = Yii::$app->db->beginTransaction();

            try {
                $unsubscribeUrl = Contentmeta::find()->where(['id' => $mailing->unsubscribe_contentmeta_id])->one();

                if ($unsubscribeUrl !== null) {
                    $unsubscribeUrl = $unsubscribeUrl->getRelativeUrl();
                }

                $configUrl = Contentmeta::find()->where(['id' => $mailing->config_contentmeta_id])->one();

                if ($configUrl !== null) {
                    $configUrl = $configUrl->getRelativeUrl();
                }

                foreach ($recipients as $recipient) {
                    $mail = Mail::createByMailing($mailing, [
                        'recipient_id'  => $recipient['id'],
                    ], [
                        'recipient'  => $recipient,
                        'unsubscribeUrl' => $unsubscribeUrl !== null ? Url::to([$unsubscribeUrl, 'action' => 'change', 'id' => $recipient['hashId']], true) : null,
                        'configUrl' => $configUrl !== null ? Url::to([$configUrl, 'action' => 'change', 'id' => $recipient['hashId']], true) : null,
                    ]);

                    if ($mail->save()) {
                        if ($mailing->track_click === 1) {
                            $mail->trackLinks();
                        }

                        if ($mailing->track_view === 1) {
                            $mail->trackViews();
                        }

                        $mailing->sent_count++;
                    }
                }

                if ($mailing->validate() && $mailing->save()) {
                    $transaction->commit();
                } else {
                    $transaction->rollBack();
                    throw new Exception('Mailing save error', print_r($mailing->errors, true));
                }
            } catch (Exception $e) {
                $transaction->rollBack();
                throw new Exception('Mail create error', $e->getCode(), $e);
            }
        } else {
            throw new Exception('Mailing save error', print_r($mailing->errors, true));
        }
    }
}
