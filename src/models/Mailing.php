<?php

namespace fafcms\mailmanager\models;

use fafcms\fafcms\inputs\AceEditor;
use fafcms\fafcms\items\ActionColumn;
use fafcms\fafcms\items\Card;
use fafcms\fafcms\items\Column;
use fafcms\fafcms\items\DataColumn;
use fafcms\fafcms\items\FormField;
use fafcms\fafcms\items\IndexView;
use fafcms\fafcms\items\Row;
use fafcms\fafcms\items\Tab;
use fafcms\helpers\traits\MultilingualTrait;
use fafcms\mailmanager\abstracts\models\BaseMailing;
use fafcms\parser\component\Parser;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\BaseDataProvider;

/**
 * This is the model class for table "{{%mailing}}".
 *
 * @package fafcms\mailmanager\models
 */
class Mailing extends BaseMailing
{
    public const SCENARIO_DISPATCH = 'dispatch';

    use MultilingualTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return 'email-multiple-outline';
    }
    //endregion BeautifulModelTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        $fieldConfig = parent::getFieldConfig();

        $fieldConfig['message_html'] = [
            'type' => AceEditor::class,
        ];

        $fieldConfig['message_text'] = [
            'type' => AceEditor::class,
        ];

        if ($this->start_dispatch === 0) {
            $fieldConfig['template_id']['options']['disabled'] = true;
            $fieldConfig['start_dispatch']['options']['disabled'] = true;
            $fieldConfig['sent_count']['options']['disabled'] = true;
            $fieldConfig['sent_at']['options']['disabled'] = true;
            $fieldConfig['view_count']['options']['disabled'] = true;
            $fieldConfig['click_count']['options']['disabled'] = true;
            $fieldConfig['unsubscribe_count']['options']['disabled'] = true;
            $fieldConfig['relation_model_class']['options']['disabled'] = true;
            $fieldConfig['relation_model_id']['options']['disabled'] = true;
        } else {
            foreach ($fieldConfig as $fieldConfigName => &$fieldConfigItem) {
                if ($fieldConfigName !== 'name') {
                    $fieldConfigItem['options']['disabled'] = true;
                }
            }
        }

        return $fieldConfig;
    }
    //endregion FieldConfigInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        $indexView = parent::indexView();

        unset($indexView['default']['message_html'], $indexView['default']['message_text'], $indexView['default']['attachments'], $indexView['default']['data']);

        $indexView['quick'] = [
            'name' => [
                'class' => DataColumn::class,
                'settings' => [
                    'field' => 'name',
                    'sort' => 3,
                    'link' => true,
                ],
            ],
            'template_id' => [
                'class' => DataColumn::class,
                'settings' => [
                    'field' => 'template_id',
                    'sort' => 4,
                    'link' => true,
                ],
            ],
            'subject' => [
                'class' => DataColumn::class,
                'settings' => [
                    'field' => 'subject',
                    'sort' => 10,
                ],
            ],
            'start_dispatch' => [
                'class' => DataColumn::class,
                'settings' => [
                    'field' => 'start_dispatch',
                    'sort' => 15,
                ],
            ],
            'start_dispatch_at' => [
                'class' => DataColumn::class,
                'settings' => [
                    'field' => 'start_dispatch_at',
                    'sort' => 16,
                ],
            ],
            'sent_count' => [
                'class' => DataColumn::class,
                'settings' => [
                    'field' => 'sent_count',
                    'sort' => 17,
                ],
            ],
            'sent_at' => [
                'class' => DataColumn::class,
                'settings' => [
                    'field' => 'sent_at',
                    'sort' => 18,
                ],
            ],
            'recipientlist_id' => [
                'class' => DataColumn::class,
                'settings' => [
                    'field' => 'recipientlist_id',
                    'sort' => 23,
                    'link' => true,
                ],
            ],
            'view_count' => [
                'class' => DataColumn::class,
                'settings' => [
                    'field' => 'view_count',
                    'sort' => 24,
                ],
            ],
            'click_count' => [
                'class' => DataColumn::class,
                'settings' => [
                    'field' => 'click_count',
                    'sort' => 25,
                ],
            ],
            'unsubscribe_count' => [
                'class' => DataColumn::class,
                'settings' => [
                    'field' => 'unsubscribe_count',
                    'sort' => 26,
                ],
            ],
            'action-column' => [
                'class' => ActionColumn::class,
            ],
        ];

        return $indexView;
    }
    //endregion IndexViewInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        $editView = parent::editView();

        $editView['default'] = [
            'buttons' => [
                static function($buttons, $model) {
                    if ($model->sent_at === null) {
                        if ($model->start_dispatch === 0) {
                            $buttons['create-mailing'] = [
                                'icon' => 'email-send-outline',
                                'label' => ['fafcms-mailmanager', 'Start Dispatch'],
                                'url' => ['start-dispatch', 'id' => $model->id],
                            ];
                        } else {
                            $buttons['create-mailing'] = [
                                'icon' => 'email-remove-outline',
                                'label' => ['fafcms-mailmanager', 'Cancel Dispatch'],
                                'url' => ['cancel-dispatch', 'id' => $model->id],
                            ];
                        }
                    }

                    return $buttons;
                }
            ],
            'contents' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 6,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-status' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'status',
                                                    ],
                                                ],
                                                'field-template_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'template_id',
                                                    ],
                                                ],
                                                'field-name' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'name',
                                                    ],
                                                ],
                                                'field-subject' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'subject',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                                'column-3' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 5,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-mailmanager',
                                                    'Tracking',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-track_view' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'track_view',
                                                    ],
                                                ],
                                                'field-track_click' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'track_click',
                                                    ],
                                                ],
                                                'field-unsubscribe_contentmeta_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'unsubscribe_contentmeta_id',
                                                    ],
                                                ],
                                                'field-config_contentmeta_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'config_contentmeta_id',
                                                    ],
                                                ],
                                                'field-sent_at' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'sent_at',
                                                    ],
                                                ],
                                                'field-sent_count' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'sent_count',
                                                    ],
                                                ],
                                                'field-view_count' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'view_count',
                                                    ],
                                                ],
                                                'field-click_count' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'click_count',
                                                    ],
                                                ],
                                                'field-unsubscribe_count' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'unsubscribe_count',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        'row-2' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-mailmanager',
                                                    'Dispatch',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-start_dispatch' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'start_dispatch',
                                                    ],
                                                ],
                                                'field-start_dispatch_at' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'start_dispatch_at',
                                                    ],
                                                ],
                                                'field-recipientlist_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'recipientlist_id',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                                'column-2' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-mailmanager',
                                                    'Relations',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-data' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'data',
                                                    ],
                                                ],
                                                'field-relation_model_class' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'relation_model_class',
                                                    ],
                                                ],
                                                'field-relation_model_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'relation_model_id',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        'row-3' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-mailmanager',
                                                    'To',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-to' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'to',
                                                    ],
                                                ],
                                                'field-cc' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'cc',
                                                    ],
                                                ],
                                                'field-bcc' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'bcc',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                                'column-2' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-mailmanager',
                                                    'From',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-from' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'from',
                                                    ],
                                                ],
                                                'field-replyto' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'replyto',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        'row-4' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 16,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-mailmanager',
                                                    'Html',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-message_html' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'message_html',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        'row-5' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 16,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-mailmanager',
                                                    'Text',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-message_text' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'message_text',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        'row-6' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 16,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-mailmanager',
                                                    'Attachments',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-attachments' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'attachments',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
                'tab-2' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-mailmanager',
                            'Mails',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 16,
                                    ],
                                    'contents' => [
                                        'index-1' => [
                                            'class' => IndexView::class,
                                            'settings' => [
                                                'modelClass' => Mail::class,
                                                'isWidget' => true,
                                                'searchFilter' => [
                                                    'Mail' => [
                                                        'mailing_id' => Yii::$app->request->get('id'),
                                                    ]
                                                ]
                                            ],
                                            'contents' => Mail::indexView()['quick']
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];

        return $editView;
    }
    //endregion EditViewInterface implementation

    /**
     * @param int   $templateId
     * @param array $attributes
     *
     * @return static|null
     * @throws \yii\base\InvalidConfigException
     */
    public static function createByTemplate(int $templateId, array $attributes = []): ?self
    {
        $template = Template::find()->where(['id' => $templateId])->one();

        if ($template === null) {
            return null;
        }

        $mailing = new self(array_merge([
            'project_id'                 => Yii::$app->fafcms->getCurrentProjectId(),
            'projectlanguage_id'         => Yii::$app->fafcms->getCurrentProjectLanguageId(),
            'status'                     => 'active',
            'template_id'                => $templateId,
            'name'                       => $template->name,
            'to'                         => $template->to,
            'from'                       => $template->from,
            'cc'                         => $template->cc,
            'bcc'                        => $template->bcc,
            'replyto'                    => $template->replyto,
            'subject'                    => $template->subject,
            'message_html'               => $template->message_html,
            'message_text'               => $template->message_text,
            'attachments'                => $template->attachments,
            'track_view'                 => $template->track_view,
            'track_click'                => $template->track_click,
            'unsubscribe_contentmeta_id' => $template->unsubscribe_contentmeta_id,
            'config_contentmeta_id'      => $template->config_contentmeta_id,
        ], $attributes));

        return $mailing;
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'required-recipientlist_id' => ['recipientlist_id', 'required', 'on' => static::SCENARIO_DISPATCH],
        ]);
    }

    /**
     * @param string $attribute
     * @param array  $data
     *
     * @return array|null|string
     */
    public function getParsedAttribute(string $attribute, array $data = [])
    {
        $parsedAttribute = [];
        $parsedAttributeDatas = Yii::$app->fafcmsParser->parse(Parser::TYPE_TEXT, $this->$attribute, Parser::ROOT, $data, null, true);

        if ($parsedAttributeDatas !== null && is_countable($parsedAttributeDatas)) {
            foreach ($parsedAttributeDatas as $index => $parsedAttributeData) {
                if ($parsedAttributeData !== null) {
                    if (is_string($parsedAttributeData)) {
                        $parsedAttributeData = [$index => $parsedAttributeData];
                    }

                    $parsedAttribute[] = $parsedAttributeData;
                }
            }

            return array_merge(...$parsedAttribute);
        }

        return $parsedAttributeDatas;
    }

    //region search
    /**
     * {@inheritdoc}
     */
    public function searchRules(): array
    {
        return [
            'integer-id'                         => ['template_id', 'integer', 'on' => static::SCENARIO_SEARCH],
            'integer-start_dispatch'             => ['start_dispatch', 'integer', 'on' => static::SCENARIO_SEARCH],
            'integer-sent_count'                 => ['sent_count', 'integer', 'on' => static::SCENARIO_SEARCH],
            'integer-track_view'                 => ['track_view', 'integer', 'on' => static::SCENARIO_SEARCH],
            'integer-track_click'                => ['track_click', 'integer', 'on' => static::SCENARIO_SEARCH],
            'integer-unsubscribe_contentmeta_id' => ['unsubscribe_contentmeta_id', 'integer', 'on' => static::SCENARIO_SEARCH],
            'integer-config_contentmeta_id'      => ['config_contentmeta_id', 'integer', 'on' => static::SCENARIO_SEARCH],
            'integer-recipientlist_id'           => ['recipientlist_id', 'integer', 'on' => static::SCENARIO_SEARCH],
            'integer-view_count'                 => ['view_count', 'integer', 'on' => static::SCENARIO_SEARCH],
            'integer-click_count'                => ['click_count', 'integer', 'on' => static::SCENARIO_SEARCH],
            'integer-unsubscribe_count'          => ['unsubscribe_count', 'integer', 'on' => static::SCENARIO_SEARCH],
            'integer-relation_model_id'          => ['relation_model_id', 'integer', 'on' => static::SCENARIO_SEARCH],
            'integer-created_by'                 => ['created_by', 'integer', 'on' => static::SCENARIO_SEARCH],
            'integer-updated_by'                 => ['updated_by', 'integer', 'on' => static::SCENARIO_SEARCH],
            'integer-activated_by'               => ['activated_by', 'integer', 'on' => static::SCENARIO_SEARCH],
            'integer-deactivated_by'             => ['deactivated_by', 'integer', 'on' => static::SCENARIO_SEARCH],
            'integer-deleted_by'                 => ['deleted_by', 'integer', 'on' => static::SCENARIO_SEARCH],
            'safe-status'                        => ['status', 'safe', 'on' => static::SCENARIO_SEARCH],
            'safe-name'                          => ['name', 'safe', 'on' => static::SCENARIO_SEARCH],
            'safe-status'                        => ['status', 'safe', 'on' => static::SCENARIO_SEARCH],
            'safe-to'                            => ['to', 'safe', 'on' => static::SCENARIO_SEARCH],
            'safe-cc'                            => ['cc', 'safe', 'on' => static::SCENARIO_SEARCH],
            'safe-bcc'                           => ['bcc', 'safe', 'on' => static::SCENARIO_SEARCH],
            'safe-from'                          => ['from', 'safe', 'on' => static::SCENARIO_SEARCH],
            'safe-replyto'                       => ['replyto', 'safe', 'on' => static::SCENARIO_SEARCH],
            'safe-subject'                       => ['subject', 'safe', 'on' => static::SCENARIO_SEARCH],
            'safe-message_html'                  => ['message_html', 'safe', 'on' => static::SCENARIO_SEARCH],
            'safe-message_text'                  => ['message_text', 'safe', 'on' => static::SCENARIO_SEARCH],
            'safe-attachments'                   => ['attachments', 'safe', 'on' => static::SCENARIO_SEARCH],
            'safe-data'                          => ['data', 'safe', 'on' => static::SCENARIO_SEARCH],
            'safe-start_dispatch_at'             => ['start_dispatch_at', 'safe', 'on' => static::SCENARIO_SEARCH],
            'safe-sent_at'                       => ['sent_at', 'safe', 'on' => static::SCENARIO_SEARCH],
            'safe-relation_model_class'          => ['relation_model_class', 'safe', 'on' => static::SCENARIO_SEARCH],
            'safe-created_at'                    => ['created_at', 'safe', 'on' => static::SCENARIO_SEARCH],
            'safe-updated_at'                    => ['updated_at', 'safe', 'on' => static::SCENARIO_SEARCH],
            'safe-activated_at'                  => ['activated_at', 'safe', 'on' => static::SCENARIO_SEARCH],
            'safe-deactivated_at'                => ['deactivated_at', 'safe', 'on' => static::SCENARIO_SEARCH],
            'safe-deleted_at'                    => ['deleted_at', 'safe', 'on' => static::SCENARIO_SEARCH],
        ];
    }

    /**
     * @param array $params
     *
     * @return BaseDataProvider
     * @throws \yii\base\InvalidConfigException
     */
    public function search(array $params): BaseDataProvider
    {
        if (!$this->searchInternal($dataProvider, $params)) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $dataProvider->query->andFilterWhere([
               'id' => $this->id,
               'template_id' => $this->template_id,
               'start_dispatch' => $this->start_dispatch,
               'start_dispatch_at' => $this->start_dispatch_at,
               'sent_count' => $this->sent_count,
               'sent_at' => $this->sent_at,
               'track_view' => $this->track_view,
               'track_click' => $this->track_click,
               'unsubscribe_contentmeta_id' => $this->unsubscribe_contentmeta_id,
               'config_contentmeta_id' => $this->config_contentmeta_id,
               'recipientlist_id' => $this->recipientlist_id,
               'view_count' => $this->view_count,
               'click_count' => $this->click_count,
               'unsubscribe_count' => $this->unsubscribe_count,
               'relation_model_id' => $this->relation_model_id,
               'created_by' => $this->created_by,
               'updated_by' => $this->updated_by,
               'activated_by' => $this->activated_by,
               'deactivated_by' => $this->deactivated_by,
               'deleted_by' => $this->deleted_by,
               'created_at' => $this->created_at,
               'updated_at' => $this->updated_at,
               'activated_at' => $this->activated_at,
               'deactivated_at' => $this->deactivated_at,
               'deleted_at' => $this->deleted_at,
           ]);

        $dataProvider->query->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'to', $this->to])
            ->andFilterWhere(['like', 'cc', $this->cc])
            ->andFilterWhere(['like', 'bcc', $this->bcc])
            ->andFilterWhere(['like', 'from', $this->from])
            ->andFilterWhere(['like', 'replyto', $this->replyto])
            ->andFilterWhere(['like', 'subject', $this->subject])
            ->andFilterWhere(['like', 'message_html', $this->message_html])
            ->andFilterWhere(['like', 'message_text', $this->message_text])
            ->andFilterWhere(['like', 'attachments', $this->attachments])
            ->andFilterWhere(['like', 'data', $this->data])
            ->andFilterWhere(['like', 'relation_model_class', $this->relation_model_class]);

        /**
         * @var $dataProvider ActiveDataProvider
         */
        $dataProvider->query->orderBy(['id' => SORT_DESC]);

        return $dataProvider;
    }
    //endregion search

}
