<?php

namespace fafcms\mailmanager\models;

use fafcms\helpers\traits\MultilingualTrait;
use fafcms\mailmanager\abstracts\models\BaseRecipientInterest;

/**
 * This is the model class for table "{{%recipient_interest}}".
 *
 * @package fafcms\mailmanager\models
 */
class RecipientInterest extends BaseRecipientInterest
{
    use MultilingualTrait;

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        $rules = parent::rules();

        //needed to add relations like interests to an inactive recipient at the newsletter registration
        unset($rules['exist-recipient_id']);

        return $rules;
    }

}
