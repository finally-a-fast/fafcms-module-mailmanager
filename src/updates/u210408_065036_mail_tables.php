<?php


declare(strict_types=1);

/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 - 2021 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-mailmanager/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-mailmanager
 * @see https://www.finally-a-fast.com/packages/fafcms-module-mailmanager/docs Documentation of the module mail manager
 * @since File available since Release 1.0.0
 */

namespace fafcms\mailmanager\updates;

use fafcms\mailmanager\updates\migrations\m210408_065036_mail_tables;
use fafcms\updater\base\Update;

/**
 * Class u210408_065036_mail_tables
 *
 * @package fafcms\mailmanager\updates
 */
class u210408_065036_mail_tables extends Update
{
    /**
     * {@inheritdoc}
     */
    public function up(): bool
    {
        return $this->migrateUp(m210408_065036_mail_tables::class);
    }

    /**
     * {@inheritdoc}
     */
    public function down(): bool
    {
        return $this->migrateDown(m210408_065036_mail_tables::class);
    }
}
