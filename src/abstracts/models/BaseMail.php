<?php

namespace fafcms\mailmanager\abstracts\models;

use fafcms\fafcms\{
    inputs\DateTimePicker,
    inputs\ExtendedDropDownList,
    inputs\NumberInput,
    inputs\SwitchCheckbox,
    inputs\Textarea,
    items\ActionColumn,
    items\Card,
    items\Column,
    items\DataColumn,
    items\FormField,
    items\Row,
    items\Tab,
    models\User,
};
use fafcms\helpers\{
    ActiveRecord,
    classes\OptionProvider,
    interfaces\EditViewInterface,
    interfaces\FieldConfigInterface,
    interfaces\IndexViewInterface,
    traits\AttributeOptionTrait,
    traits\BeautifulModelTrait,
    traits\OptionProviderTrait,
};
use fafcms\mailmanager\{
    Bootstrap,
    models\Mailing,
    models\Recipient,
    models\Template,
    models\Track,
};
use Yii;
use yii\db\ActiveQuery;
use yii\validators\DateValidator;

/**
 * This is the abstract model class for table "{{%mail}}".
 *
 * @package fafcms\mailmanager\abstracts\models
 *
 * @property-read array $fieldConfig
 *
 * @property int $id
 * @property int|null $mailing_id
 * @property int|null $template_id
 * @property int|null $recipient_id
 * @property string $to
 * @property string|null $cc
 * @property string|null $bcc
 * @property string $from
 * @property string|null $replyto
 * @property string $subject
 * @property string $message_html
 * @property string $message_text
 * @property string|null $attachments
 * @property string|null $embedments
 * @property string|null $sent_at
 * @property int $viewed
 * @property int $view_count
 * @property string|null $first_view_at
 * @property string|null $last_view_at
 * @property int $clicked
 * @property int $click_count
 * @property string|null $first_click_at
 * @property string|null $last_click_at
 * @property int $unsubscribed
 * @property string|null $unsubscribed_at
 * @property int $bounced
 * @property string|null $bounced_at
 * @property string|null $bounce_reason
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property User $createdBy
 * @property Track[] $mailTracks
 * @property Mailing $mailing
 * @property Recipient $recipient
 * @property Template $template
 * @property User $updatedBy
 */
abstract class BaseMail extends ActiveRecord implements FieldConfigInterface, IndexViewInterface, EditViewInterface
{
    use BeautifulModelTrait;
    use OptionProviderTrait;
    use AttributeOptionTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return Bootstrap::$id . '/mail';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return  'mail';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-mailmanager', 'Mails');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-mailmanager', 'Mail');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['id'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionProviderTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptionProvider(array $properties = []): OptionProvider
    {
        return (new OptionProvider(static::class))
            ->setSelect([
                static::tableName() . '.id',
                static::tableName() . '.id'
            ])
            ->setSort([static::tableName() . '.id' => SORT_ASC])
            ->setItemLabel(static function ($item) {
                return static::extendedLabel($item);
            })
            ->setProperties($properties);
    }
    //endregion OptionProviderTrait implementation

    //region AttributeOptionTrait implementation
    /**
     * @inheritDoc
     */
    public function attributeOptions(): array
    {
        return [
            'mailing_id' => static function($properties = []) {
                return Mailing::getOptionProvider($properties)->getOptions();
            },
            'template_id' => static function($properties = []) {
                return Template::getOptionProvider($properties)->getOptions();
            },
            'recipient_id' => static function($properties = []) {
                return Recipient::getOptionProvider($properties)->getOptions();
            },
            'created_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'updated_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
        ];
    }
    //endregion AttributeOptionTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'id' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'mailing_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('mailing_id', false),
                'relationClassName' => Mailing::class,
            ],
            'template_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('template_id', false),
                'relationClassName' => Template::class,
            ],
            'recipient_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('recipient_id', false),
                'relationClassName' => Recipient::class,
            ],
            'to' => [
                'type' => Textarea::class,
            ],
            'cc' => [
                'type' => Textarea::class,
            ],
            'bcc' => [
                'type' => Textarea::class,
            ],
            'from' => [
                'type' => Textarea::class,
            ],
            'replyto' => [
                'type' => Textarea::class,
            ],
            'subject' => [
                'type' => Textarea::class,
            ],
            'message_html' => [
                'type' => Textarea::class,
            ],
            'message_text' => [
                'type' => Textarea::class,
            ],
            'attachments' => [
                'type' => Textarea::class,
            ],
            'embedments' => [
                'type' => Textarea::class,
            ],
            'sent_at' => [
                'type' => DateTimePicker::class,
            ],
            'viewed' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'view_count' => [
                'type' => NumberInput::class,
            ],
            'first_view_at' => [
                'type' => DateTimePicker::class,
            ],
            'last_view_at' => [
                'type' => DateTimePicker::class,
            ],
            'clicked' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'click_count' => [
                'type' => NumberInput::class,
            ],
            'first_click_at' => [
                'type' => DateTimePicker::class,
            ],
            'last_click_at' => [
                'type' => DateTimePicker::class,
            ],
            'unsubscribed' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'unsubscribed_at' => [
                'type' => DateTimePicker::class,
            ],
            'bounced' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'bounced_at' => [
                'type' => DateTimePicker::class,
            ],
            'bounce_reason' => [
                'type' => Textarea::class,
            ],
            'created_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('created_by', false),
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('updated_by', false),
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'created_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
        ];
    }
    //endregion FieldConfigInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        return [
            'default' => [
                'id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'id',
                        'sort' => 1,
                        'link' => true,
                    ],
                ],
                'mailing_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'mailing_id',
                        'sort' => 2,
                        'link' => true,
                    ],
                ],
                'template_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'template_id',
                        'sort' => 3,
                        'link' => true,
                    ],
                ],
                'recipient_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'recipient_id',
                        'sort' => 4,
                        'link' => true,
                    ],
                ],
                'to' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'to',
                        'sort' => 5,
                    ],
                ],
                'cc' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'cc',
                        'sort' => 6,
                    ],
                ],
                'bcc' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'bcc',
                        'sort' => 7,
                    ],
                ],
                'from' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'from',
                        'sort' => 8,
                    ],
                ],
                'replyto' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'replyto',
                        'sort' => 9,
                    ],
                ],
                'subject' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'subject',
                        'sort' => 10,
                    ],
                ],
                'message_html' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'message_html',
                        'sort' => 11,
                    ],
                ],
                'message_text' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'message_text',
                        'sort' => 12,
                    ],
                ],
                'attachments' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'attachments',
                        'sort' => 13,
                    ],
                ],
                'embedments' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'embedments',
                        'sort' => 14,
                    ],
                ],
                'sent_at' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'sent_at',
                        'sort' => 15,
                    ],
                ],
                'viewed' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'viewed',
                        'sort' => 16,
                    ],
                ],
                'view_count' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'view_count',
                        'sort' => 17,
                    ],
                ],
                'first_view_at' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'first_view_at',
                        'sort' => 18,
                    ],
                ],
                'last_view_at' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'last_view_at',
                        'sort' => 19,
                    ],
                ],
                'clicked' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'clicked',
                        'sort' => 20,
                    ],
                ],
                'click_count' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'click_count',
                        'sort' => 21,
                    ],
                ],
                'first_click_at' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'first_click_at',
                        'sort' => 22,
                    ],
                ],
                'last_click_at' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'last_click_at',
                        'sort' => 23,
                    ],
                ],
                'unsubscribed' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'unsubscribed',
                        'sort' => 24,
                    ],
                ],
                'unsubscribed_at' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'unsubscribed_at',
                        'sort' => 25,
                    ],
                ],
                'bounced' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'bounced',
                        'sort' => 26,
                    ],
                ],
                'bounced_at' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'bounced_at',
                        'sort' => 27,
                    ],
                ],
                'bounce_reason' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'bounce_reason',
                        'sort' => 28,
                    ],
                ],
                'action-column' => [
                    'class' => ActionColumn::class,
                ],
            ]
        ];
    }
    //endregion IndexViewInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-mailing_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'mailing_id',
                                                    ],
                                                ],
                                                'field-template_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'template_id',
                                                    ],
                                                ],
                                                'field-recipient_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'recipient_id',
                                                    ],
                                                ],
                                                'field-to' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'to',
                                                    ],
                                                ],
                                                'field-cc' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'cc',
                                                    ],
                                                ],
                                                'field-bcc' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'bcc',
                                                    ],
                                                ],
                                                'field-from' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'from',
                                                    ],
                                                ],
                                                'field-replyto' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'replyto',
                                                    ],
                                                ],
                                                'field-subject' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'subject',
                                                    ],
                                                ],
                                                'field-message_html' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'message_html',
                                                    ],
                                                ],
                                                'field-message_text' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'message_text',
                                                    ],
                                                ],
                                                'field-attachments' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'attachments',
                                                    ],
                                                ],
                                                'field-embedments' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'embedments',
                                                    ],
                                                ],
                                                'field-sent_at' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'sent_at',
                                                    ],
                                                ],
                                                'field-viewed' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'viewed',
                                                    ],
                                                ],
                                                'field-view_count' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'view_count',
                                                    ],
                                                ],
                                                'field-first_view_at' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'first_view_at',
                                                    ],
                                                ],
                                                'field-last_view_at' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'last_view_at',
                                                    ],
                                                ],
                                                'field-clicked' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'clicked',
                                                    ],
                                                ],
                                                'field-click_count' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'click_count',
                                                    ],
                                                ],
                                                'field-first_click_at' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'first_click_at',
                                                    ],
                                                ],
                                                'field-last_click_at' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'last_click_at',
                                                    ],
                                                ],
                                                'field-unsubscribed' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'unsubscribed',
                                                    ],
                                                ],
                                                'field-unsubscribed_at' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'unsubscribed_at',
                                                    ],
                                                ],
                                                'field-bounced' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'bounced',
                                                    ],
                                                ],
                                                'field-bounced_at' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'bounced_at',
                                                    ],
                                                ],
                                                'field-bounce_reason' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'bounce_reason',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%mail}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'integer-mailing_id' => ['mailing_id', 'integer'],
            'integer-template_id' => ['template_id', 'integer'],
            'integer-recipient_id' => ['recipient_id', 'integer'],
            'integer-view_count' => ['view_count', 'integer'],
            'integer-click_count' => ['click_count', 'integer'],
            'integer-created_by' => ['created_by', 'integer'],
            'integer-updated_by' => ['updated_by', 'integer'],
            'required-to' => ['to', 'required'],
            'required-from' => ['from', 'required'],
            'required-subject' => ['subject', 'required'],
            'required-message_html' => ['message_html', 'required'],
            'required-message_text' => ['message_text', 'required'],
            'string-to' => ['to', 'string'],
            'string-cc' => ['cc', 'string'],
            'string-bcc' => ['bcc', 'string'],
            'string-from' => ['from', 'string'],
            'string-replyto' => ['replyto', 'string'],
            'string-subject' => ['subject', 'string'],
            'string-message_html' => ['message_html', 'string'],
            'string-message_text' => ['message_text', 'string'],
            'string-attachments' => ['attachments', 'string'],
            'string-embedments' => ['embedments', 'string'],
            'string-bounce_reason' => ['bounce_reason', 'string'],
            'date-sent_at' => ['sent_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'sent_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-first_view_at' => ['first_view_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'first_view_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-last_view_at' => ['last_view_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'last_view_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-first_click_at' => ['first_click_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'first_click_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-last_click_at' => ['last_click_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'last_click_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-unsubscribed_at' => ['unsubscribed_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'unsubscribed_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-bounced_at' => ['bounced_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'bounced_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-created_at' => ['created_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'created_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-updated_at' => ['updated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'updated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'boolean-viewed' => ['viewed', 'boolean'],
            'boolean-clicked' => ['clicked', 'boolean'],
            'boolean-unsubscribed' => ['unsubscribed', 'boolean'],
            'boolean-bounced' => ['bounced', 'boolean'],
            'exist-created_by' => [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            'exist-mailing_id' => [['mailing_id'], 'exist', 'skipOnError' => true, 'targetClass' => Mailing::class, 'targetAttribute' => ['mailing_id' => 'id']],
            'exist-recipient_id' => [['recipient_id'], 'exist', 'skipOnError' => true, 'targetClass' => Recipient::class, 'targetAttribute' => ['recipient_id' => 'id']],
            'exist-template_id' => [['template_id'], 'exist', 'skipOnError' => true, 'targetClass' => Template::class, 'targetAttribute' => ['template_id' => 'id']],
            'exist-updated_by' => [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'id' => Yii::t('fafcms-mailmanager', 'ID'),
            'mailing_id' => Yii::t('fafcms-mailmanager', 'Mailing ID'),
            'template_id' => Yii::t('fafcms-mailmanager', 'Template ID'),
            'recipient_id' => Yii::t('fafcms-mailmanager', 'Recipient ID'),
            'to' => Yii::t('fafcms-mailmanager', 'To'),
            'cc' => Yii::t('fafcms-mailmanager', 'Cc'),
            'bcc' => Yii::t('fafcms-mailmanager', 'Bcc'),
            'from' => Yii::t('fafcms-mailmanager', 'From'),
            'replyto' => Yii::t('fafcms-mailmanager', 'Replyto'),
            'subject' => Yii::t('fafcms-mailmanager', 'Subject'),
            'message_html' => Yii::t('fafcms-mailmanager', 'Message Html'),
            'message_text' => Yii::t('fafcms-mailmanager', 'Message Text'),
            'attachments' => Yii::t('fafcms-mailmanager', 'Attachments'),
            'embedments' => Yii::t('fafcms-mailmanager', 'Embedments'),
            'sent_at' => Yii::t('fafcms-mailmanager', 'Sent At'),
            'viewed' => Yii::t('fafcms-mailmanager', 'Viewed'),
            'view_count' => Yii::t('fafcms-mailmanager', 'View Count'),
            'first_view_at' => Yii::t('fafcms-mailmanager', 'First View At'),
            'last_view_at' => Yii::t('fafcms-mailmanager', 'Last View At'),
            'clicked' => Yii::t('fafcms-mailmanager', 'Clicked'),
            'click_count' => Yii::t('fafcms-mailmanager', 'Click Count'),
            'first_click_at' => Yii::t('fafcms-mailmanager', 'First Click At'),
            'last_click_at' => Yii::t('fafcms-mailmanager', 'Last Click At'),
            'unsubscribed' => Yii::t('fafcms-mailmanager', 'Unsubscribed'),
            'unsubscribed_at' => Yii::t('fafcms-mailmanager', 'Unsubscribed At'),
            'bounced' => Yii::t('fafcms-mailmanager', 'Bounced'),
            'bounced_at' => Yii::t('fafcms-mailmanager', 'Bounced At'),
            'bounce_reason' => Yii::t('fafcms-mailmanager', 'Bounce Reason'),
            'created_by' => Yii::t('fafcms-mailmanager', 'Created By'),
            'updated_by' => Yii::t('fafcms-mailmanager', 'Updated By'),
            'created_at' => Yii::t('fafcms-mailmanager', 'Created At'),
            'updated_at' => Yii::t('fafcms-mailmanager', 'Updated At'),
        ]);
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return ActiveQuery
     */
    public function getCreatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'created_by',
        ]);
    }

    /**
     * Gets query for [[MailTracks]].
     *
     * @return ActiveQuery
     */
    public function getMailTracks(): ActiveQuery
    {
        return $this->hasMany(Track::class, [
            'mail_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[Mailing]].
     *
     * @return ActiveQuery
     */
    public function getMailing(): ActiveQuery
    {
        return $this->hasOne(Mailing::class, [
            'id' => 'mailing_id',
        ]);
    }

    /**
     * Gets query for [[Recipient]].
     *
     * @return ActiveQuery
     */
    public function getRecipient(): ActiveQuery
    {
        return $this->hasOne(Recipient::class, [
            'id' => 'recipient_id',
        ]);
    }

    /**
     * Gets query for [[Template]].
     *
     * @return ActiveQuery
     */
    public function getTemplate(): ActiveQuery
    {
        return $this->hasOne(Template::class, [
            'id' => 'template_id',
        ]);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return ActiveQuery
     */
    public function getUpdatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'updated_by',
        ]);
    }
}
