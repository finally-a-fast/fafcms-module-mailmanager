<?php

namespace fafcms\mailmanager\models;

use fafcms\helpers\traits\MultilingualTrait;
use fafcms\mailmanager\abstracts\models\BaseMailingInterest;

/**
 * This is the model class for table "{{%mailing_interest}}".
 *
 * @package fafcms\mailmanager\models
 */
class MailingInterest extends BaseMailingInterest
{
    use MultilingualTrait;

}
