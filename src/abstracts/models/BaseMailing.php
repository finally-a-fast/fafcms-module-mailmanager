<?php

namespace fafcms\mailmanager\abstracts\models;

use fafcms\fafcms\{
    inputs\DateTimePicker,
    inputs\DropDownList,
    inputs\ExtendedDropDownList,
    inputs\NumberInput,
    inputs\SwitchCheckbox,
    inputs\Textarea,
    inputs\TextInput,
    items\ActionColumn,
    items\Card,
    items\Column,
    items\DataColumn,
    items\FormField,
    items\Row,
    items\Tab,
    models\User,
};
use fafcms\helpers\{
    ActiveRecord,
    classes\OptionProvider,
    interfaces\EditViewInterface,
    interfaces\FieldConfigInterface,
    interfaces\IndexViewInterface,
    traits\AttributeOptionTrait,
    traits\BeautifulModelTrait,
    traits\OptionProviderTrait,
};
use fafcms\mailmanager\{
    Bootstrap,
    models\Mail,
    models\MailingInterest,
    models\Recipientlist,
    models\Template,
    models\Track,
};
use fafcms\sitemanager\models\Contentmeta;
use Yii;
use yii\db\ActiveQuery;
use yii\validators\DateValidator;

/**
 * This is the abstract model class for table "{{%mailing}}".
 *
 * @package fafcms\mailmanager\abstracts\models
 *
 * @property-read array $fieldConfig
 *
 * @property int $id
 * @property string $status
 * @property int|null $template_id
 * @property string $name
 * @property string|null $to
 * @property string|null $cc
 * @property string|null $bcc
 * @property string|null $from
 * @property string|null $replyto
 * @property string|null $subject
 * @property string $message_html
 * @property string $message_text
 * @property string|null $attachments
 * @property string|null $data
 * @property int $start_dispatch
 * @property string|null $start_dispatch_at
 * @property int $sent_count
 * @property string|null $sent_at
 * @property int $track_view
 * @property int $track_click
 * @property int|null $unsubscribe_contentmeta_id
 * @property int|null $config_contentmeta_id
 * @property int|null $recipientlist_id
 * @property int $view_count
 * @property int $click_count
 * @property int $unsubscribe_count
 * @property string|null $relation_model_class
 * @property int|null $relation_model_id
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $activated_by
 * @property int|null $deactivated_by
 * @property int|null $deleted_by
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $activated_at
 * @property string|null $deactivated_at
 * @property string|null $deleted_at
 *
 * @property User $activatedBy
 * @property Contentmeta $configContentmeta
 * @property User $createdBy
 * @property User $deactivatedBy
 * @property User $deletedBy
 * @property MailingInterest[] $mailingMailingInterests
 * @property Mail[] $mailingMails
 * @property Track[] $mailingTracks
 * @property Recipientlist $recipientlist
 * @property Template $template
 * @property Contentmeta $unsubscribeContentmeta
 * @property User $updatedBy
 */
abstract class BaseMailing extends ActiveRecord implements FieldConfigInterface, IndexViewInterface, EditViewInterface
{
    use BeautifulModelTrait;
    use OptionProviderTrait;
    use AttributeOptionTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return Bootstrap::$id . '/mailing';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return  'mailing';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-mailmanager', 'Mailings');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-mailmanager', 'Mailing');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['name'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionProviderTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptionProvider(array $properties = []): OptionProvider
    {
        return (new OptionProvider(static::class))
            ->setSelect([
                static::tableName() . '.id',
                static::tableName() . '.name'
            ])
            ->setSort([static::tableName() . '.name' => SORT_ASC])
            ->setItemLabel(static function ($item) {
                return static::extendedLabel($item);
            })
            ->setProperties($properties);
    }
    //endregion OptionProviderTrait implementation

    //region AttributeOptionTrait implementation
    /**
     * @inheritDoc
     */
    public function attributeOptions(): array
    {
        return [
            'status' => [
                static::STATUS_ACTIVE => Yii::t('fafcms-core', 'Active'),
                static::STATUS_INACTIVE => Yii::t('fafcms-core', 'Inactive'),
            ],
            'template_id' => static function($properties = []) {
                return Template::getOptionProvider($properties)->getOptions();
            },
            'unsubscribe_contentmeta_id' => static function($properties = []) {
                return Contentmeta::getOptionProvider($properties)->getOptions();
            },
            'config_contentmeta_id' => static function($properties = []) {
                return Contentmeta::getOptionProvider($properties)->getOptions();
            },
            'recipientlist_id' => static function($properties = []) {
                return Recipientlist::getOptionProvider($properties)->getOptions();
            },
            'created_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'updated_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'activated_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'deactivated_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'deleted_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
        ];
    }
    //endregion AttributeOptionTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'id' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'status' => [
                'type' => DropDownList::class,
                'items' => $this->getAttributeOptions('status', false),
            ],
            'template_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('template_id', false),
                'relationClassName' => Template::class,
            ],
            'name' => [
                'type' => TextInput::class,
            ],
            'to' => [
                'type' => Textarea::class,
            ],
            'cc' => [
                'type' => Textarea::class,
            ],
            'bcc' => [
                'type' => Textarea::class,
            ],
            'from' => [
                'type' => Textarea::class,
            ],
            'replyto' => [
                'type' => Textarea::class,
            ],
            'subject' => [
                'type' => Textarea::class,
            ],
            'message_html' => [
                'type' => Textarea::class,
            ],
            'message_text' => [
                'type' => Textarea::class,
            ],
            'attachments' => [
                'type' => Textarea::class,
            ],
            'data' => [
                'type' => Textarea::class,
            ],
            'start_dispatch' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'start_dispatch_at' => [
                'type' => DateTimePicker::class,
            ],
            'sent_count' => [
                'type' => NumberInput::class,
            ],
            'sent_at' => [
                'type' => DateTimePicker::class,
            ],
            'track_view' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'track_click' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'unsubscribe_contentmeta_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('unsubscribe_contentmeta_id', false),
                'relationClassName' => Contentmeta::class,
            ],
            'config_contentmeta_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('config_contentmeta_id', false),
                'relationClassName' => Contentmeta::class,
            ],
            'recipientlist_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('recipientlist_id', false),
                'relationClassName' => Recipientlist::class,
            ],
            'view_count' => [
                'type' => NumberInput::class,
            ],
            'click_count' => [
                'type' => NumberInput::class,
            ],
            'unsubscribe_count' => [
                'type' => NumberInput::class,
            ],
            'relation_model_class' => [
                'type' => TextInput::class,
            ],
            'relation_model_id' => [
                'type' => NumberInput::class,
            ],
            'created_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('created_by', false),
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('updated_by', false),
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'activated_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('activated_by', false),
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deactivated_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('deactivated_by', false),
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deleted_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('deleted_by', false),
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'created_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'activated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deactivated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deleted_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
        ];
    }
    //endregion FieldConfigInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        return [
            'default' => [
                'id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'id',
                        'sort' => 1,
                    ],
                ],
                'status' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'status',
                        'sort' => 2,
                    ],
                ],
                'name' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'name',
                        'sort' => 3,
                        'link' => true,
                    ],
                ],
                'template_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'template_id',
                        'sort' => 4,
                        'link' => true,
                    ],
                ],
                'to' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'to',
                        'sort' => 5,
                    ],
                ],
                'cc' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'cc',
                        'sort' => 6,
                    ],
                ],
                'bcc' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'bcc',
                        'sort' => 7,
                    ],
                ],
                'from' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'from',
                        'sort' => 8,
                    ],
                ],
                'replyto' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'replyto',
                        'sort' => 9,
                    ],
                ],
                'subject' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'subject',
                        'sort' => 10,
                    ],
                ],
                'message_html' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'message_html',
                        'sort' => 11,
                    ],
                ],
                'message_text' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'message_text',
                        'sort' => 12,
                    ],
                ],
                'attachments' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'attachments',
                        'sort' => 13,
                    ],
                ],
                'data' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'data',
                        'sort' => 14,
                    ],
                ],
                'start_dispatch' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'start_dispatch',
                        'sort' => 15,
                    ],
                ],
                'start_dispatch_at' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'start_dispatch_at',
                        'sort' => 16,
                    ],
                ],
                'sent_count' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'sent_count',
                        'sort' => 17,
                    ],
                ],
                'sent_at' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'sent_at',
                        'sort' => 18,
                    ],
                ],
                'track_view' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'track_view',
                        'sort' => 19,
                    ],
                ],
                'track_click' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'track_click',
                        'sort' => 20,
                    ],
                ],
                'unsubscribe_contentmeta_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'unsubscribe_contentmeta_id',
                        'sort' => 21,
                        'link' => true,
                    ],
                ],
                'config_contentmeta_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'config_contentmeta_id',
                        'sort' => 22,
                        'link' => true,
                    ],
                ],
                'recipientlist_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'recipientlist_id',
                        'sort' => 23,
                        'link' => true,
                    ],
                ],
                'view_count' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'view_count',
                        'sort' => 24,
                    ],
                ],
                'click_count' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'click_count',
                        'sort' => 25,
                    ],
                ],
                'unsubscribe_count' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'unsubscribe_count',
                        'sort' => 26,
                    ],
                ],
                'relation_model_class' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'relation_model_class',
                        'sort' => 27,
                    ],
                ],
                'relation_model_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'relation_model_id',
                        'sort' => 28,
                    ],
                ],
                'action-column' => [
                    'class' => ActionColumn::class,
                ],
            ]
        ];
    }
    //endregion IndexViewInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-status' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'status',
                                                    ],
                                                ],
                                                'field-template_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'template_id',
                                                    ],
                                                ],
                                                'field-name' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'name',
                                                    ],
                                                ],
                                                'field-to' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'to',
                                                    ],
                                                ],
                                                'field-cc' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'cc',
                                                    ],
                                                ],
                                                'field-bcc' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'bcc',
                                                    ],
                                                ],
                                                'field-from' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'from',
                                                    ],
                                                ],
                                                'field-replyto' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'replyto',
                                                    ],
                                                ],
                                                'field-subject' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'subject',
                                                    ],
                                                ],
                                                'field-message_html' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'message_html',
                                                    ],
                                                ],
                                                'field-message_text' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'message_text',
                                                    ],
                                                ],
                                                'field-attachments' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'attachments',
                                                    ],
                                                ],
                                                'field-data' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'data',
                                                    ],
                                                ],
                                                'field-start_dispatch' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'start_dispatch',
                                                    ],
                                                ],
                                                'field-start_dispatch_at' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'start_dispatch_at',
                                                    ],
                                                ],
                                                'field-sent_count' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'sent_count',
                                                    ],
                                                ],
                                                'field-sent_at' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'sent_at',
                                                    ],
                                                ],
                                                'field-track_view' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'track_view',
                                                    ],
                                                ],
                                                'field-track_click' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'track_click',
                                                    ],
                                                ],
                                                'field-unsubscribe_contentmeta_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'unsubscribe_contentmeta_id',
                                                    ],
                                                ],
                                                'field-config_contentmeta_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'config_contentmeta_id',
                                                    ],
                                                ],
                                                'field-recipientlist_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'recipientlist_id',
                                                    ],
                                                ],
                                                'field-view_count' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'view_count',
                                                    ],
                                                ],
                                                'field-click_count' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'click_count',
                                                    ],
                                                ],
                                                'field-unsubscribe_count' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'unsubscribe_count',
                                                    ],
                                                ],
                                                'field-relation_model_class' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'relation_model_class',
                                                    ],
                                                ],
                                                'field-relation_model_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'relation_model_id',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%mailing}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'integer-template_id' => ['template_id', 'integer'],
            'integer-sent_count' => ['sent_count', 'integer'],
            'integer-unsubscribe_contentmeta_id' => ['unsubscribe_contentmeta_id', 'integer'],
            'integer-config_contentmeta_id' => ['config_contentmeta_id', 'integer'],
            'integer-recipientlist_id' => ['recipientlist_id', 'integer'],
            'integer-view_count' => ['view_count', 'integer'],
            'integer-click_count' => ['click_count', 'integer'],
            'integer-unsubscribe_count' => ['unsubscribe_count', 'integer'],
            'integer-relation_model_id' => ['relation_model_id', 'integer'],
            'integer-created_by' => ['created_by', 'integer'],
            'integer-updated_by' => ['updated_by', 'integer'],
            'integer-activated_by' => ['activated_by', 'integer'],
            'integer-deactivated_by' => ['deactivated_by', 'integer'],
            'integer-deleted_by' => ['deleted_by', 'integer'],
            'required-name' => ['name', 'required'],
            'required-message_html' => ['message_html', 'required'],
            'required-message_text' => ['message_text', 'required'],
            'string-to' => ['to', 'string'],
            'string-cc' => ['cc', 'string'],
            'string-bcc' => ['bcc', 'string'],
            'string-from' => ['from', 'string'],
            'string-replyto' => ['replyto', 'string'],
            'string-subject' => ['subject', 'string'],
            'string-message_html' => ['message_html', 'string'],
            'string-message_text' => ['message_text', 'string'],
            'string-attachments' => ['attachments', 'string'],
            'string-data' => ['data', 'string'],
            'boolean-start_dispatch' => ['start_dispatch', 'boolean'],
            'boolean-track_view' => ['track_view', 'boolean'],
            'boolean-track_click' => ['track_click', 'boolean'],
            'date-start_dispatch_at' => ['start_dispatch_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'start_dispatch_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-sent_at' => ['sent_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'sent_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-created_at' => ['created_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'created_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-updated_at' => ['updated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'updated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-activated_at' => ['activated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'activated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-deactivated_at' => ['deactivated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'deactivated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-deleted_at' => ['deleted_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'deleted_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'string-status' => ['status', 'string', 'max' => 255],
            'string-name' => ['name', 'string', 'max' => 255],
            'string-relation_model_class' => ['relation_model_class', 'string', 'max' => 255],
            'exist-activated_by' => [['activated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['activated_by' => 'id']],
            'exist-config_contentmeta_id' => [['config_contentmeta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contentmeta::class, 'targetAttribute' => ['config_contentmeta_id' => 'id']],
            'exist-created_by' => [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            'exist-deactivated_by' => [['deactivated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['deactivated_by' => 'id']],
            'exist-deleted_by' => [['deleted_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['deleted_by' => 'id']],
            'exist-recipientlist_id' => [['recipientlist_id'], 'exist', 'skipOnError' => true, 'targetClass' => Recipientlist::class, 'targetAttribute' => ['recipientlist_id' => 'id']],
            'exist-template_id' => [['template_id'], 'exist', 'skipOnError' => true, 'targetClass' => Template::class, 'targetAttribute' => ['template_id' => 'id']],
            'exist-unsubscribe_contentmeta_id' => [['unsubscribe_contentmeta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contentmeta::class, 'targetAttribute' => ['unsubscribe_contentmeta_id' => 'id']],
            'exist-updated_by' => [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'id' => Yii::t('fafcms-mailmanager', 'ID'),
            'status' => Yii::t('fafcms-mailmanager', 'Status'),
            'template_id' => Yii::t('fafcms-mailmanager', 'Template ID'),
            'name' => Yii::t('fafcms-mailmanager', 'Name'),
            'to' => Yii::t('fafcms-mailmanager', 'To'),
            'cc' => Yii::t('fafcms-mailmanager', 'Cc'),
            'bcc' => Yii::t('fafcms-mailmanager', 'Bcc'),
            'from' => Yii::t('fafcms-mailmanager', 'From'),
            'replyto' => Yii::t('fafcms-mailmanager', 'Replyto'),
            'subject' => Yii::t('fafcms-mailmanager', 'Subject'),
            'message_html' => Yii::t('fafcms-mailmanager', 'Message Html'),
            'message_text' => Yii::t('fafcms-mailmanager', 'Message Text'),
            'attachments' => Yii::t('fafcms-mailmanager', 'Attachments'),
            'data' => Yii::t('fafcms-mailmanager', 'Data'),
            'start_dispatch' => Yii::t('fafcms-mailmanager', 'Start Dispatch'),
            'start_dispatch_at' => Yii::t('fafcms-mailmanager', 'Start Dispatch At'),
            'sent_count' => Yii::t('fafcms-mailmanager', 'Sent Count'),
            'sent_at' => Yii::t('fafcms-mailmanager', 'Sent At'),
            'track_view' => Yii::t('fafcms-mailmanager', 'Track View'),
            'track_click' => Yii::t('fafcms-mailmanager', 'Track Click'),
            'unsubscribe_contentmeta_id' => Yii::t('fafcms-mailmanager', 'Unsubscribe Contentmeta ID'),
            'config_contentmeta_id' => Yii::t('fafcms-mailmanager', 'Config Contentmeta ID'),
            'recipientlist_id' => Yii::t('fafcms-mailmanager', 'Recipientlist ID'),
            'view_count' => Yii::t('fafcms-mailmanager', 'View Count'),
            'click_count' => Yii::t('fafcms-mailmanager', 'Click Count'),
            'unsubscribe_count' => Yii::t('fafcms-mailmanager', 'Unsubscribe Count'),
            'relation_model_class' => Yii::t('fafcms-mailmanager', 'Relation Model Class'),
            'relation_model_id' => Yii::t('fafcms-mailmanager', 'Relation Model ID'),
            'created_by' => Yii::t('fafcms-mailmanager', 'Created By'),
            'updated_by' => Yii::t('fafcms-mailmanager', 'Updated By'),
            'activated_by' => Yii::t('fafcms-mailmanager', 'Activated By'),
            'deactivated_by' => Yii::t('fafcms-mailmanager', 'Deactivated By'),
            'deleted_by' => Yii::t('fafcms-mailmanager', 'Deleted By'),
            'created_at' => Yii::t('fafcms-mailmanager', 'Created At'),
            'updated_at' => Yii::t('fafcms-mailmanager', 'Updated At'),
            'activated_at' => Yii::t('fafcms-mailmanager', 'Activated At'),
            'deactivated_at' => Yii::t('fafcms-mailmanager', 'Deactivated At'),
            'deleted_at' => Yii::t('fafcms-mailmanager', 'Deleted At'),
        ]);
    }

    /**
     * Gets query for [[ActivatedBy]].
     *
     * @return ActiveQuery
     */
    public function getActivatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'activated_by',
        ]);
    }

    /**
     * Gets query for [[ConfigContentmeta]].
     *
     * @return ActiveQuery
     */
    public function getConfigContentmeta(): ActiveQuery
    {
        return $this->hasOne(Contentmeta::class, [
            'id' => 'config_contentmeta_id',
        ]);
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return ActiveQuery
     */
    public function getCreatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'created_by',
        ]);
    }

    /**
     * Gets query for [[DeactivatedBy]].
     *
     * @return ActiveQuery
     */
    public function getDeactivatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'deactivated_by',
        ]);
    }

    /**
     * Gets query for [[DeletedBy]].
     *
     * @return ActiveQuery
     */
    public function getDeletedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'deleted_by',
        ]);
    }

    /**
     * Gets query for [[MailingMailingInterests]].
     *
     * @return ActiveQuery
     */
    public function getMailingMailingInterests(): ActiveQuery
    {
        return $this->hasMany(MailingInterest::class, [
            'mailing_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[MailingMails]].
     *
     * @return ActiveQuery
     */
    public function getMailingMails(): ActiveQuery
    {
        return $this->hasMany(Mail::class, [
            'mailing_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[MailingTracks]].
     *
     * @return ActiveQuery
     */
    public function getMailingTracks(): ActiveQuery
    {
        return $this->hasMany(Track::class, [
            'mailing_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[Recipientlist]].
     *
     * @return ActiveQuery
     */
    public function getRecipientlist(): ActiveQuery
    {
        return $this->hasOne(Recipientlist::class, [
            'id' => 'recipientlist_id',
        ]);
    }

    /**
     * Gets query for [[Template]].
     *
     * @return ActiveQuery
     */
    public function getTemplate(): ActiveQuery
    {
        return $this->hasOne(Template::class, [
            'id' => 'template_id',
        ]);
    }

    /**
     * Gets query for [[UnsubscribeContentmeta]].
     *
     * @return ActiveQuery
     */
    public function getUnsubscribeContentmeta(): ActiveQuery
    {
        return $this->hasOne(Contentmeta::class, [
            'id' => 'unsubscribe_contentmeta_id',
        ]);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return ActiveQuery
     */
    public function getUpdatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'updated_by',
        ]);
    }
}
