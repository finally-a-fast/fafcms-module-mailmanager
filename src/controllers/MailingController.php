<?php

declare(strict_types=1);

/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 - 2021 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-mailmanager/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-mailmanager
 * @see https://www.finally-a-fast.com/packages/fafcms-module-mailmanager/docs Documentation of the module mail manager
 * @since File available since Release 1.0.0
 */

namespace fafcms\mailmanager\controllers;

use fafcms\helpers\DefaultController;
use fafcms\mailmanager\models\Mailing;
use yii\web\NotFoundHttpException;
use Yii;
use yii\web\Response;

/**
 * Class MailingController
 *
 * @package fafcms\mailmanager\models
 */
class MailingController extends DefaultController
{
    public static $modelClass = Mailing::class;

    /**
     * @param $id
     *
     * @return Response
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionStartDispatch($id): Response
    {
        $model = self::$modelClass::findOne($id);

        if ($model === null || $model->sent_at !== null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $singular = $model->getEditDataSingular();

        $model->start_dispatch = 1;

        $model->setScenario(self::$modelClass::SCENARIO_DISPATCH);

        if ($model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('fafcms-mailmanager', 'Mail dispatch will start at defined time.', [
                'modelClass' => $singular,
            ]));
        } else {
            Yii::$app->session->setFlash('error', Yii::t('fafcms-mailmanager', 'Error while saving {modelClass}!', [
                'modelClass' => $singular,
            ]) . '<br><br>' . implode('<br><br>', $model->getErrorSummary(true)));
        }

        return $this->redirect(Yii::$app->getRequest()->getReferrer());
    }

    /**
     * @param $id
     *
     * @return Response
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCancelDispatch($id): Response
    {
        $model = self::$modelClass::findOne($id);

        if ($model === null || $model->sent_at !== null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $singular = $model->getEditDataSingular();

        $model->start_dispatch = 0;

        if ($model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('fafcms-core', 'Mail dispatch has been canceled.', [
                'modelClass' => $singular,
            ]));
        } else {
            Yii::$app->session->setFlash('error', Yii::t('fafcms-core', 'Error while saving {modelClass}!', [
                'modelClass' => $singular,
            ]) . '<br><br>' . implode('<br><br>', $model->getErrorSummary(true)));
        }

        return $this->redirect(Yii::$app->getRequest()->getReferrer());
    }
}
