<?php

namespace fafcms\mailmanager\models;

use fafcms\helpers\traits\MultilingualTrait;
use fafcms\mailmanager\abstracts\models\BaseBlacklist;

/**
 * This is the model class for table "{{%blacklist}}".
 *
 * @package fafcms\mailmanager\models
 */
class Blacklist extends BaseBlacklist
{
    use MultilingualTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return 'account-box-multiple';
    }
    //endregion BeautifulModelTrait implementation
}
