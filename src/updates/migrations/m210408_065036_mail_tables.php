<?php

declare(strict_types=1);

/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 - 2021 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-mailmanager/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-mailmanager
 * @see https://www.finally-a-fast.com/packages/fafcms-module-mailmanager/docs Documentation of the module mail manager
 * @since File available since Release 1.0.0
 */

namespace fafcms\mailmanager\updates\migrations;

use fafcms\mailmanager\models\{
    Blacklist,
    Interest,
    Mail,
    Mailing,
    MailingInterest,
    Recipient,
    RecipientInterest,
    Recipientlist,
    Template,
    Track,
};

use fafcms\fafcms\models\Project;
use fafcms\fafcms\models\Projectlanguage;
use fafcms\fafcms\models\User;
use fafcms\sitemanager\models\Contentmeta;
use fafcms\updater\base\Migration;

/**
 * Class m210408_065036_mail_tables
 *
 * @package fafcms\mailmanager\updates\migrations
 */
class m210408_065036_mail_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp(): bool
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(Recipientlist::tableName(), [
            'id' => $this->primaryKey(10)->unsigned(),
            'project_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'translation_base_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'projectlanguage_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'status' => $this->string(255)->notNull()->defaultValue('inactive'),
            'name' => $this->string(255)->notNull(),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'activated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deactivated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deleted_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
            'updated_at' => $this->datetime()->null()->defaultValue(null),
            'activated_at' => $this->datetime()->null()->defaultValue(null),
            'deactivated_at' => $this->datetime()->null()->defaultValue(null),
            'deleted_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndexByColumns(Recipientlist::tableName(), ['project_id'], false);
        $this->createIndexByColumns(Recipientlist::tableName(), ['translation_base_id'], false);
        $this->createIndexByColumns(Recipientlist::tableName(), ['projectlanguage_id'], false);
        $this->createIndexByColumns(Recipientlist::tableName(), ['created_by'], false);
        $this->createIndexByColumns(Recipientlist::tableName(), ['updated_by'], false);
        $this->createIndexByColumns(Recipientlist::tableName(), ['activated_by'], false);
        $this->createIndexByColumns(Recipientlist::tableName(), ['deactivated_by'], false);
        $this->createIndexByColumns(Recipientlist::tableName(), ['deleted_by'], false);

        $this->addForeignKeyByColumns(Recipientlist::tableName(), 'project_id', Project::tableName(), 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKeyByColumns(Recipientlist::tableName(), 'translation_base_id', Recipientlist::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Recipientlist::tableName(), 'projectlanguage_id', Projectlanguage::tableName(), 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKeyByColumns(Recipientlist::tableName(), 'created_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Recipientlist::tableName(), 'updated_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Recipientlist::tableName(), 'activated_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Recipientlist::tableName(), 'deactivated_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Recipientlist::tableName(), 'deleted_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');

        $this->createTable(Interest::tableName(), [
            'id' => $this->primaryKey(10)->unsigned(),
            'project_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'translation_base_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'projectlanguage_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'status' => $this->string(255)->notNull()->defaultValue('inactive'),
            'recipientlist_id' => $this->integer(10)->unsigned()->notNull(),
            'user_selectable' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'name' => $this->string(255)->notNull(),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'activated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deactivated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deleted_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
            'updated_at' => $this->datetime()->null()->defaultValue(null),
            'activated_at' => $this->datetime()->null()->defaultValue(null),
            'deactivated_at' => $this->datetime()->null()->defaultValue(null),
            'deleted_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndexByColumns(Interest::tableName(), ['project_id'], false);
        $this->createIndexByColumns(Interest::tableName(), ['translation_base_id'], false);
        $this->createIndexByColumns(Interest::tableName(), ['projectlanguage_id'], false);
        $this->createIndexByColumns(Interest::tableName(), ['recipientlist_id'], false);
        $this->createIndexByColumns(Interest::tableName(), ['created_by'], false);
        $this->createIndexByColumns(Interest::tableName(), ['updated_by'], false);
        $this->createIndexByColumns(Interest::tableName(), ['activated_by'], false);
        $this->createIndexByColumns(Interest::tableName(), ['deactivated_by'], false);
        $this->createIndexByColumns(Interest::tableName(), ['deleted_by'], false);

        $this->addForeignKeyByColumns(Interest::tableName(), 'project_id', Project::tableName(), 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKeyByColumns(Interest::tableName(), 'translation_base_id', Interest::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Interest::tableName(), 'projectlanguage_id', Projectlanguage::tableName(), 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKeyByColumns(Interest::tableName(), 'recipientlist_id', Recipientlist::tableName(), 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKeyByColumns(Interest::tableName(), 'created_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Interest::tableName(), 'updated_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Interest::tableName(), 'activated_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Interest::tableName(), 'deactivated_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Interest::tableName(), 'deleted_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');

        $this->createTable(Recipient::tableName(), [
            'id' => $this->primaryKey(10)->unsigned(),
            'project_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'translation_base_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'projectlanguage_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'status' => $this->string(255)->notNull()->defaultValue('inactive'),
            'recipientlist_id' => $this->integer(10)->unsigned()->notNull(),
            'email' => $this->string(255)->notNull(),
            'sex' => $this->string(255)->null()->defaultValue(null),
            'firstname' => $this->string(255)->null()->defaultValue(null),
            'lastname' => $this->string(255)->null()->defaultValue(null),
            'requested' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'requested_at' => $this->datetime()->null()->defaultValue(null),
            'requested_data' => $this->text()->null()->defaultValue(null),
            'requested_mail_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'confirmed' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'confirmed_at' => $this->datetime()->null()->defaultValue(null),
            'confirmed_data' => $this->text()->null()->defaultValue(null),
            'confirmed_mail_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'activated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deactivated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deleted_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
            'updated_at' => $this->datetime()->null()->defaultValue(null),
            'activated_at' => $this->datetime()->null()->defaultValue(null),
            'deactivated_at' => $this->datetime()->null()->defaultValue(null),
            'deleted_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndexByColumns(Recipient::tableName(), ['project_id'], false);
        $this->createIndexByColumns(Recipient::tableName(), ['translation_base_id'], false);
        $this->createIndexByColumns(Recipient::tableName(), ['projectlanguage_id'], false);
        $this->createIndexByColumns(Recipient::tableName(), ['recipientlist_id'], false);
        $this->createIndexByColumns(Recipient::tableName(), ['created_by'], false);
        $this->createIndexByColumns(Recipient::tableName(), ['updated_by'], false);
        $this->createIndexByColumns(Recipient::tableName(), ['activated_by'], false);
        $this->createIndexByColumns(Recipient::tableName(), ['deactivated_by'], false);
        $this->createIndexByColumns(Recipient::tableName(), ['deleted_by'], false);

        $this->addForeignKeyByColumns(Recipient::tableName(), 'project_id', Project::tableName(), 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKeyByColumns(Recipient::tableName(), 'translation_base_id', Recipient::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Recipient::tableName(), 'projectlanguage_id', Projectlanguage::tableName(), 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKeyByColumns(Recipient::tableName(), 'recipientlist_id', Recipientlist::tableName(), 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKeyByColumns(Recipient::tableName(), 'created_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Recipient::tableName(), 'updated_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Recipient::tableName(), 'activated_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Recipient::tableName(), 'deactivated_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Recipient::tableName(), 'deleted_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');

        $this->createTable(RecipientInterest::tableName(), [
            'id' => $this->primaryKey(10)->unsigned(),
            'recipient_id' => $this->integer(10)->unsigned()->notNull(),
            'interest_id' => $this->integer(10)->unsigned()->notNull(),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndexByColumns(RecipientInterest::tableName(), ['recipient_id'], false);
        $this->createIndexByColumns(RecipientInterest::tableName(), ['interest_id'], false);
        $this->createIndexByColumns(RecipientInterest::tableName(), ['created_by'], false);

        $this->addForeignKeyByColumns(RecipientInterest::tableName(), 'recipient_id', Recipient::tableName(), 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKeyByColumns(RecipientInterest::tableName(), 'interest_id', Interest::tableName(), 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKeyByColumns(RecipientInterest::tableName(), 'created_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');

        $this->createTable(Blacklist::tableName(), [
            'id' => $this->primaryKey(10)->unsigned(),
            'project_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'translation_base_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'projectlanguage_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'status' => $this->string(255)->notNull()->defaultValue('inactive'),
            'email' => $this->string(255)->notNull(),
            'sex' => $this->string(255)->null()->defaultValue(null),
            'firstname' => $this->string(255)->null()->defaultValue(null),
            'lastname' => $this->string(255)->null()->defaultValue(null),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'activated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deactivated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deleted_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
            'updated_at' => $this->datetime()->null()->defaultValue(null),
            'activated_at' => $this->datetime()->null()->defaultValue(null),
            'deactivated_at' => $this->datetime()->null()->defaultValue(null),
            'deleted_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndexByColumns(Blacklist::tableName(), ['project_id'], false);
        $this->createIndexByColumns(Blacklist::tableName(), ['translation_base_id'], false);
        $this->createIndexByColumns(Blacklist::tableName(), ['projectlanguage_id'], false);
        $this->createIndexByColumns(Blacklist::tableName(), ['created_by'], false);
        $this->createIndexByColumns(Blacklist::tableName(), ['updated_by'], false);
        $this->createIndexByColumns(Blacklist::tableName(), ['activated_by'], false);
        $this->createIndexByColumns(Blacklist::tableName(), ['deactivated_by'], false);
        $this->createIndexByColumns(Blacklist::tableName(), ['deleted_by'], false);

        $this->addForeignKeyByColumns(Blacklist::tableName(), 'project_id', Project::tableName(), 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKeyByColumns(Blacklist::tableName(), 'translation_base_id', Blacklist::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Blacklist::tableName(), 'projectlanguage_id', Projectlanguage::tableName(), 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKeyByColumns(Blacklist::tableName(), 'created_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Blacklist::tableName(), 'updated_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Blacklist::tableName(), 'activated_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Blacklist::tableName(), 'deactivated_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Blacklist::tableName(), 'deleted_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');

        $this->createTable(Template::tableName(), [
            'id' => $this->primaryKey(10)->unsigned(),
            'project_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'translation_base_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'projectlanguage_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'status' => $this->string(255)->notNull()->defaultValue('inactive'),
            'name' => $this->string(255)->notNull(),
            'to' => $this->text()->null()->defaultValue(null),
            'cc' => $this->text()->null()->defaultValue(null),
            'bcc' => $this->text()->null()->defaultValue(null),
            'from' => $this->text()->null()->defaultValue(null),
            'replyto' => $this->text()->null()->defaultValue(null),
            'subject' => $this->text()->null()->defaultValue(null),
            'message_html' => $this->getDb()->getSchema()->createColumnSchemaBuilder('longtext')->null()->defaultValue(null),
            'message_text' => $this->getDb()->getSchema()->createColumnSchemaBuilder('longtext')->null()->defaultValue(null),
            'attachments' => $this->getDb()->getSchema()->createColumnSchemaBuilder('longtext')->null()->defaultValue(null),
            'track_view' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(1),
            'track_click' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(1),
            'unsubscribe_contentmeta_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'config_contentmeta_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'activated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deactivated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deleted_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
            'updated_at' => $this->datetime()->null()->defaultValue(null),
            'activated_at' => $this->datetime()->null()->defaultValue(null),
            'deactivated_at' => $this->datetime()->null()->defaultValue(null),
            'deleted_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndexByColumns(Template::tableName(), ['project_id'], false);
        $this->createIndexByColumns(Template::tableName(), ['translation_base_id'], false);
        $this->createIndexByColumns(Template::tableName(), ['projectlanguage_id'], false);
        $this->createIndexByColumns(Template::tableName(), ['unsubscribe_contentmeta_id'], false);
        $this->createIndexByColumns(Template::tableName(), ['config_contentmeta_id'], false);
        $this->createIndexByColumns(Template::tableName(), ['created_by'], false);
        $this->createIndexByColumns(Template::tableName(), ['updated_by'], false);
        $this->createIndexByColumns(Template::tableName(), ['activated_by'], false);
        $this->createIndexByColumns(Template::tableName(), ['deactivated_by'], false);
        $this->createIndexByColumns(Template::tableName(), ['deleted_by'], false);

        $this->addForeignKeyByColumns(Template::tableName(), 'project_id', Project::tableName(), 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKeyByColumns(Template::tableName(), 'translation_base_id', Template::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Template::tableName(), 'projectlanguage_id', Projectlanguage::tableName(), 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKeyByColumns(Template::tableName(), 'unsubscribe_contentmeta_id', Contentmeta::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Template::tableName(), 'config_contentmeta_id', Contentmeta::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Template::tableName(), 'created_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Template::tableName(), 'updated_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Template::tableName(), 'activated_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Template::tableName(), 'deactivated_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Template::tableName(), 'deleted_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');

        $this->createTable(Mailing::tableName(), [
            'id' => $this->primaryKey(10)->unsigned(),
            'project_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'translation_base_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'projectlanguage_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'status' => $this->string(255)->notNull()->defaultValue('inactive'),
            'template_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'name' => $this->string(255)->notNull(),
            'to' => $this->text()->null()->defaultValue(null),
            'cc' => $this->text()->null()->defaultValue(null),
            'bcc' => $this->text()->null()->defaultValue(null),
            'from' => $this->text()->null()->defaultValue(null),
            'replyto' => $this->text()->null()->defaultValue(null),
            'subject' => $this->text()->null()->defaultValue(null),
            'message_html' => $this->getDb()->getSchema()->createColumnSchemaBuilder('longtext')->null()->notNull(),
            'message_text' => $this->getDb()->getSchema()->createColumnSchemaBuilder('longtext')->null()->notNull(),
            'attachments' => $this->getDb()->getSchema()->createColumnSchemaBuilder('longtext')->null()->defaultValue(null),
            'data' => $this->getDb()->getSchema()->createColumnSchemaBuilder('longtext')->null()->defaultValue(null),
            'start_dispatch' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'start_dispatch_at' => $this->datetime()->null()->defaultValue(null),
            'sent_count' => $this->integer(10)->unsigned()->notNull()->defaultValue(0),
            'sent_at' => $this->datetime()->null()->defaultValue(null),
            'track_view' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(1),
            'track_click' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(1),
            'unsubscribe_contentmeta_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'config_contentmeta_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'recipientlist_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'view_count' => $this->integer(10)->unsigned()->notNull()->defaultValue(0),
            'click_count' => $this->integer(10)->unsigned()->notNull()->defaultValue(0),
            'unsubscribe_count' => $this->integer(10)->unsigned()->notNull()->defaultValue(0),
            'relation_model_class' => $this->string(255)->notNull(),
            'relation_model_id' => $this->integer(10)->unsigned()->notNull()->defaultValue(0),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'activated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deactivated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deleted_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
            'updated_at' => $this->datetime()->null()->defaultValue(null),
            'activated_at' => $this->datetime()->null()->defaultValue(null),
            'deactivated_at' => $this->datetime()->null()->defaultValue(null),
            'deleted_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndexByColumns(Mailing::tableName(), ['project_id'], false);
        $this->createIndexByColumns(Mailing::tableName(), ['translation_base_id'], false);
        $this->createIndexByColumns(Mailing::tableName(), ['projectlanguage_id'], false);
        $this->createIndexByColumns(Mailing::tableName(), ['template_id'], false);
        $this->createIndexByColumns(Mailing::tableName(), ['unsubscribe_contentmeta_id'], false);
        $this->createIndexByColumns(Mailing::tableName(), ['config_contentmeta_id'], false);
        $this->createIndexByColumns(Mailing::tableName(), ['recipientlist_id'], false);
        $this->createIndexByColumns(Mailing::tableName(), ['created_by'], false);
        $this->createIndexByColumns(Mailing::tableName(), ['updated_by'], false);
        $this->createIndexByColumns(Mailing::tableName(), ['activated_by'], false);
        $this->createIndexByColumns(Mailing::tableName(), ['deactivated_by'], false);
        $this->createIndexByColumns(Mailing::tableName(), ['deleted_by'], false);

        $this->addForeignKeyByColumns(Mailing::tableName(), 'project_id', Project::tableName(), 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKeyByColumns(Mailing::tableName(), 'translation_base_id', Mailing::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Mailing::tableName(), 'projectlanguage_id', Projectlanguage::tableName(), 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKeyByColumns(Mailing::tableName(), 'template_id', Template::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Mailing::tableName(), 'unsubscribe_contentmeta_id', Contentmeta::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Mailing::tableName(), 'config_contentmeta_id', Contentmeta::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Mailing::tableName(), 'recipientlist_id', Recipientlist::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Mailing::tableName(), 'created_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Mailing::tableName(), 'updated_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Mailing::tableName(), 'activated_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Mailing::tableName(), 'deactivated_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Mailing::tableName(), 'deleted_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');

        $this->createTable(MailingInterest::tableName(), [
            'id' => $this->primaryKey(10)->unsigned(),
            'mailing_id' => $this->integer(10)->unsigned()->notNull(),
            'interest_id' => $this->integer(10)->unsigned()->notNull(),
            'has_not' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndexByColumns(MailingInterest::tableName(), ['mailing_id'], false);
        $this->createIndexByColumns(MailingInterest::tableName(), ['interest_id'], false);
        $this->createIndexByColumns(MailingInterest::tableName(), ['created_by'], false);

        $this->addForeignKeyByColumns(MailingInterest::tableName(), 'mailing_id', Mailing::tableName(), 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKeyByColumns(MailingInterest::tableName(), 'interest_id', Interest::tableName(), 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKeyByColumns(MailingInterest::tableName(), 'created_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');

        $this->createTable(Mail::tableName(), [
            'id' => $this->primaryKey(10)->unsigned(),
            'project_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'translation_base_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'projectlanguage_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'mailing_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'template_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'recipient_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'to' => $this->text()->notNull(),
            'cc' => $this->text()->null()->defaultValue(null),
            'bcc' => $this->text()->null()->defaultValue(null),
            'from' => $this->text()->notNull(),
            'replyto' => $this->text()->null()->defaultValue(null),
            'subject' => $this->text()->notNull(),
            'message_html' => $this->getDb()->getSchema()->createColumnSchemaBuilder('longtext')->null()->notNull(),
            'message_text' => $this->getDb()->getSchema()->createColumnSchemaBuilder('longtext')->null()->notNull(),
            'attachments' => $this->getDb()->getSchema()->createColumnSchemaBuilder('longtext')->null()->defaultValue(null),
            'embedments' => $this->getDb()->getSchema()->createColumnSchemaBuilder('longtext')->null()->defaultValue(null),
            'sent_at' => $this->datetime()->null()->defaultValue(null),
            'viewed' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'view_count' => $this->integer(10)->unsigned()->notNull()->defaultValue(0),
            'first_view_at' => $this->datetime()->null()->defaultValue(null),
            'last_view_at' => $this->datetime()->null()->defaultValue(null),
            'clicked' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'click_count' => $this->integer(10)->unsigned()->notNull()->defaultValue(0),
            'first_click_at' => $this->datetime()->null()->defaultValue(null),
            'last_click_at' => $this->datetime()->null()->defaultValue(null),
            'unsubscribed' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'unsubscribed_at' => $this->datetime()->null()->defaultValue(null),
            'bounced' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'bounced_at' => $this->datetime()->null()->defaultValue(null),
            'bounce_reason' => $this->text()->null()->defaultValue(null),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
            'updated_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndexByColumns(Mail::tableName(), ['project_id'], false);
        $this->createIndexByColumns(Mail::tableName(), ['translation_base_id'], false);
        $this->createIndexByColumns(Mail::tableName(), ['projectlanguage_id'], false);
        $this->createIndexByColumns(Mail::tableName(), ['mailing_id'], false);
        $this->createIndexByColumns(Mail::tableName(), ['template_id'], false);
        $this->createIndexByColumns(Mail::tableName(), ['recipient_id'], false);
        $this->createIndexByColumns(Mail::tableName(), ['created_by'], false);
        $this->createIndexByColumns(Mail::tableName(), ['updated_by'], false);

        $this->addForeignKeyByColumns(Mail::tableName(), 'project_id', Project::tableName(), 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKeyByColumns(Mail::tableName(), 'translation_base_id', Mail::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Mail::tableName(), 'projectlanguage_id', Projectlanguage::tableName(), 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKeyByColumns(Mail::tableName(), 'mailing_id', Mailing::tableName(), 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKeyByColumns(Mail::tableName(), 'template_id', Template::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Mail::tableName(), 'recipient_id', Recipient::tableName(), 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKeyByColumns(Mail::tableName(), 'created_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Mail::tableName(), 'updated_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');

        $this->createTable(Track::tableName(), [
            'id' => $this->primaryKey(10)->unsigned(),
            'project_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'translation_base_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'projectlanguage_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'mailing_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'mail_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'recipient_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'type' => $this->integer(10)->unsigned()->notNull(),
            'target' => $this->string(5000)->null()->defaultValue(null),
            'used' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'use_count' => $this->integer(10)->unsigned()->notNull()->defaultValue(0),
            'first_use_at' => $this->datetime()->null()->defaultValue(null),
            'last_use_at' => $this->datetime()->null()->defaultValue(null),
            'valid_until' => $this->datetime()->null()->defaultValue(null),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
            'updated_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndexByColumns(Track::tableName(), ['project_id'], false);
        $this->createIndexByColumns(Track::tableName(), ['translation_base_id'], false);
        $this->createIndexByColumns(Track::tableName(), ['projectlanguage_id'], false);
        $this->createIndexByColumns(Track::tableName(), ['mailing_id'], false);
        $this->createIndexByColumns(Track::tableName(), ['mail_id'], false);
        $this->createIndexByColumns(Track::tableName(), ['recipient_id'], false);
        $this->createIndexByColumns(Track::tableName(), ['created_by'], false);
        $this->createIndexByColumns(Track::tableName(), ['updated_by'], false);

        $this->addForeignKeyByColumns(Track::tableName(), 'project_id', Project::tableName(), 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKeyByColumns(Track::tableName(), 'translation_base_id', Track::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Track::tableName(), 'projectlanguage_id', Projectlanguage::tableName(), 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKeyByColumns(Track::tableName(), 'mailing_id', Mailing::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Track::tableName(), 'mail_id', Mail::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Track::tableName(), 'recipient_id', Recipient::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Track::tableName(), 'created_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKeyByColumns(Track::tableName(), 'updated_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown(): bool
    {
        $this->dropForeignKeyByColumns(Recipientlist::tableName(), 'project_id');
        $this->dropForeignKeyByColumns(Recipientlist::tableName(), 'translation_base_id');
        $this->dropForeignKeyByColumns(Recipientlist::tableName(), 'projectlanguage_id');
        $this->dropForeignKeyByColumns(Recipientlist::tableName(), 'created_by');
        $this->dropForeignKeyByColumns(Recipientlist::tableName(), 'updated_by');
        $this->dropForeignKeyByColumns(Recipientlist::tableName(), 'activated_by');
        $this->dropForeignKeyByColumns(Recipientlist::tableName(), 'deactivated_by');
        $this->dropForeignKeyByColumns(Recipientlist::tableName(), 'deleted_by');

        $this->dropForeignKeyByColumns(Interest::tableName(), 'project_id');
        $this->dropForeignKeyByColumns(Interest::tableName(), 'translation_base_id');
        $this->dropForeignKeyByColumns(Interest::tableName(), 'projectlanguage_id');
        $this->dropForeignKeyByColumns(Interest::tableName(), 'recipientlist_id');
        $this->dropForeignKeyByColumns(Interest::tableName(), 'created_by');
        $this->dropForeignKeyByColumns(Interest::tableName(), 'updated_by');
        $this->dropForeignKeyByColumns(Interest::tableName(), 'activated_by');
        $this->dropForeignKeyByColumns(Interest::tableName(), 'deactivated_by');
        $this->dropForeignKeyByColumns(Interest::tableName(), 'deleted_by');

        $this->dropForeignKeyByColumns(Recipient::tableName(), 'project_id');
        $this->dropForeignKeyByColumns(Recipient::tableName(), 'translation_base_id');
        $this->dropForeignKeyByColumns(Recipient::tableName(), 'projectlanguage_id');
        $this->dropForeignKeyByColumns(Recipient::tableName(), 'recipientlist_id');
        $this->dropForeignKeyByColumns(Recipient::tableName(), 'created_by');
        $this->dropForeignKeyByColumns(Recipient::tableName(), 'updated_by');
        $this->dropForeignKeyByColumns(Recipient::tableName(), 'activated_by');
        $this->dropForeignKeyByColumns(Recipient::tableName(), 'deactivated_by');
        $this->dropForeignKeyByColumns(Recipient::tableName(), 'deleted_by');

        $this->dropForeignKeyByColumns(RecipientInterest::tableName(), 'recipient_id');
        $this->dropForeignKeyByColumns(RecipientInterest::tableName(), 'interest_id');
        $this->dropForeignKeyByColumns(RecipientInterest::tableName(), 'created_by');

        $this->dropForeignKeyByColumns(Blacklist::tableName(), 'project_id');
        $this->dropForeignKeyByColumns(Blacklist::tableName(), 'translation_base_id');
        $this->dropForeignKeyByColumns(Blacklist::tableName(), 'projectlanguage_id');
        $this->dropForeignKeyByColumns(Blacklist::tableName(), 'created_by');
        $this->dropForeignKeyByColumns(Blacklist::tableName(), 'updated_by');
        $this->dropForeignKeyByColumns(Blacklist::tableName(), 'activated_by');
        $this->dropForeignKeyByColumns(Blacklist::tableName(), 'deactivated_by');
        $this->dropForeignKeyByColumns(Blacklist::tableName(), 'deleted_by');

        $this->dropForeignKeyByColumns(Template::tableName(), 'project_id');
        $this->dropForeignKeyByColumns(Template::tableName(), 'translation_base_id');
        $this->dropForeignKeyByColumns(Template::tableName(), 'projectlanguage_id');
        $this->dropForeignKeyByColumns(Template::tableName(), 'unsubscribe_contentmeta_id');
        $this->dropForeignKeyByColumns(Template::tableName(), 'config_contentmeta_id');
        $this->dropForeignKeyByColumns(Template::tableName(), 'created_by');
        $this->dropForeignKeyByColumns(Template::tableName(), 'updated_by');
        $this->dropForeignKeyByColumns(Template::tableName(), 'activated_by');
        $this->dropForeignKeyByColumns(Template::tableName(), 'deactivated_by');
        $this->dropForeignKeyByColumns(Template::tableName(), 'deleted_by');

        $this->dropForeignKeyByColumns(Mailing::tableName(), 'project_id');
        $this->dropForeignKeyByColumns(Mailing::tableName(), 'translation_base_id');
        $this->dropForeignKeyByColumns(Mailing::tableName(), 'projectlanguage_id');
        $this->dropForeignKeyByColumns(Mailing::tableName(), 'template_id');
        $this->dropForeignKeyByColumns(Mailing::tableName(), 'unsubscribe_contentmeta_id');
        $this->dropForeignKeyByColumns(Mailing::tableName(), 'config_contentmeta_id');
        $this->dropForeignKeyByColumns(Mailing::tableName(), 'recipientlist_id');
        $this->dropForeignKeyByColumns(Mailing::tableName(), 'created_by');
        $this->dropForeignKeyByColumns(Mailing::tableName(), 'updated_by');
        $this->dropForeignKeyByColumns(Mailing::tableName(), 'activated_by');
        $this->dropForeignKeyByColumns(Mailing::tableName(), 'deactivated_by');
        $this->dropForeignKeyByColumns(Mailing::tableName(), 'deleted_by');

        $this->dropForeignKeyByColumns(MailingInterest::tableName(), 'mailing_id');
        $this->dropForeignKeyByColumns(MailingInterest::tableName(), 'interest_id');
        $this->dropForeignKeyByColumns(MailingInterest::tableName(), 'created_by');

        $this->dropForeignKeyByColumns(Mail::tableName(), 'project_id');
        $this->dropForeignKeyByColumns(Mail::tableName(), 'translation_base_id');
        $this->dropForeignKeyByColumns(Mail::tableName(), 'projectlanguage_id');
        $this->dropForeignKeyByColumns(Mail::tableName(), 'mailing_id');
        $this->dropForeignKeyByColumns(Mail::tableName(), 'recipient_id');
        $this->dropForeignKeyByColumns(Mail::tableName(), 'created_by');
        $this->dropForeignKeyByColumns(Mail::tableName(), 'updated_by');

        $this->dropForeignKeyByColumns(Track::tableName(), 'project_id');
        $this->dropForeignKeyByColumns(Track::tableName(), 'translation_base_id');
        $this->dropForeignKeyByColumns(Track::tableName(), 'projectlanguage_id');
        $this->dropForeignKeyByColumns(Track::tableName(), 'mailing_id');
        $this->dropForeignKeyByColumns(Track::tableName(), 'mail_id');
        $this->dropForeignKeyByColumns(Track::tableName(), 'recipient_id');
        $this->dropForeignKeyByColumns(Track::tableName(), 'created_by');
        $this->dropForeignKeyByColumns(Track::tableName(), 'updated_by');

        $this->dropTable(Recipientlist::tableName());
        $this->dropTable(Interest::tableName());
        $this->dropTable(Recipient::tableName());
        $this->dropTable(RecipientInterest::tableName());
        $this->dropTable(Blacklist::tableName());
        $this->dropTable(Template::tableName());
        $this->dropTable(Mailing::tableName());
        $this->dropTable(MailingInterest::tableName());
        $this->dropTable(Mail::tableName());
        $this->dropTable(Track::tableName());

        return true;
    }
}
