<?php

namespace fafcms\mailmanager\models;

use fafcms\fafcms\items\Card;
use fafcms\fafcms\items\Column;
use fafcms\fafcms\items\FormField;
use fafcms\fafcms\items\IndexView;
use fafcms\fafcms\items\Row;
use fafcms\fafcms\items\Tab;
use fafcms\helpers\traits\MultilingualTrait;
use fafcms\mailmanager\abstracts\models\BaseRecipientlist;
use yii\data\ActiveDataProvider;
use yii\data\BaseDataProvider;
use yii\db\ActiveQuery;
use Yii;

/**
 * This is the model class for table "{{%recipientlist}}".
 *
 * @property Interest[] $userSelectableRecipientlistInterests
 * @package fafcms\mailmanager\models
 */
class Recipientlist extends BaseRecipientlist
{
    use MultilingualTrait;

    //region EditViewInterface implementation
    public static function editView(): array
    {
        $editView = parent::editView();

        $editView['default'] = [
            'contents' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 11,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-status' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'status',
                                                    ],
                                                ],
                                                'field-name' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'name',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        'row-2' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 16,
                                    ],
                                    'contents' => [
                                        'index-1' => [
                                            'class' => IndexView::class,
                                            'settings' => [
                                                'modelClass' => Recipient::class,
                                                'isWidget' => true,
                                                'searchFilter' => [
                                                    'Recipient' => [
                                                        'recipientlist_id' => Yii::$app->request->get('id'),
                                                    ]
                                                ]
                                            ],
                                            'contents' => Recipient::indexView()['quick']
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];

        return $editView;
    }
    //endregion EditViewInterface implementation

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return 'account-box-multiple-outline';
    }
    //endregion BeautifulModelTrait implementation

    /**
     * Gets query for [[RecipientlistInterests]].
     *
     * @return ActiveQuery
     */
    public function getUserSelectableRecipientlistInterests(): ActiveQuery
    {
        return $this->hasMany(Interest::class, [
            'recipientlist_id' => 'id',
        ])->andWhere(['user_selectable' => 1]);
    }

    //region search
    /**
     * {@inheritdoc}
     */
    public function searchRules(): array
    {
        return [
            [['id', 'created_by', 'updated_by', 'activated_by', 'deactivated_by', 'deleted_by'], 'integer', 'on' => static::SCENARIO_SEARCH],
            [['status', 'name', 'created_at', 'updated_at', 'activated_at', 'deactivated_at', 'deleted_at'], 'safe', 'on' => static::SCENARIO_SEARCH],
        ];
    }

    /**
     * @param array $params
     *
     * @return BaseDataProvider
     * @throws \yii\base\InvalidConfigException
     */
    public function search(array $params): BaseDataProvider
    {
        if (!$this->searchInternal($dataProvider, $params)) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $dataProvider->query->andFilterWhere([
            'id' => $this->id,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'activated_by' => $this->activated_by,
            'deactivated_by' => $this->deactivated_by,
            'deleted_by' => $this->deleted_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'activated_at' => $this->activated_at,
            'deactivated_at' => $this->deactivated_at,
            'deleted_at' => $this->deleted_at,
        ]);

        $dataProvider->query->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'name', $this->name]);


        /**
         * @var $dataProvider ActiveDataProvider
         */
        $dataProvider->query->orderBy(['id' => SORT_DESC]);

        return $dataProvider;
    }
    //endregion search
}
