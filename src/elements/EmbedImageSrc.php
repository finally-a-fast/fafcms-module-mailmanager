<?php

declare(strict_types=1);

/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 - 2021 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-mailmanager/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-mailmanager
 * @see https://www.finally-a-fast.com/packages/fafcms-module-mailmanager/docs Documentation of the module mail manager
 * @since File available since Release 1.0.0
 */

namespace fafcms\mailmanager\elements;

use Faf\TemplateEngine\Helpers\ParserElement;

/**
 * Class EmbedImageSrc
 *
 * @package fafcms\mailmanager\elements
 */
class EmbedImageSrc extends ParserElement
{
    /**
     * {@inheritdoc}
     */
    public function name(): string
    {
        return 'embed-image-src';
    }

    /**
     * {@inheritdoc}
     */
    public function description(): string
    {
        return 'The file id.';
    }

    /**
     * {@inheritdoc}
     */
    public function allowedParents(): ?array
    {
        return [EmbedImage::class];
    }

    /**
     * {@inheritdoc}
     * @return string|int
     */
    public function run()
    {
        return $this->content;
    }
}
