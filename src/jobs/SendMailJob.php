<?php

declare(strict_types=1);

/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 - 2021 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-mailmanager/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-mailmanager
 * @see https://www.finally-a-fast.com/packages/fafcms-module-mailmanager/docs Documentation of the module mail manager
 * @since File available since Release 1.0.0
 */

namespace fafcms\mailmanager\jobs;

use DateTime;
use DateTimeZone;
use Exception;
use fafcms\mailmanager\models\Mail;
use Yii;
use yii\helpers\Json;

/**
 * Class SendMailJob
 *
 * @package fafcms\mailmanager\jobs
 */
class SendMailJob extends \yii\base\BaseObject implements \yii\queue\RetryableJobInterface
{
    public int $mailId;

    /**
     * @inheritdoc
     * @throws Exception
     */
    public function execute($queue)
    {
        $mail = Mail::find()->where(['id' => $this->mailId])->one();

        if ($mail === null) {
            return;
        }

        $mailer = Yii::$app->mailer->compose(null)
            ->setTo(Json::decode($mail->to))
            ->setCc(Json::decode($mail->cc))
            ->setBcc(Json::decode($mail->bcc))
            ->setFrom(Json::decode($mail->from))
            ->setReplyTo(Json::decode($mail->replyto))
            ->setSubject($mail->subject);
            //->setPriority(!empty($mail->priority)?(int)$mail->priority:3) TODO

        $attachments = Json::decode($mail->attachments, true);

        foreach ($attachments as $attachment) {
            $mailer->attach($attachment['file'], [
                'fileName' => $attachment['name'],
                'contentType' => $attachment['type'],
            ]);
        }

        $embedments = Json::decode($mail->embedments, true);

        $messageHtml = $mail->message_html;

        foreach ($embedments as $embedmentId => $embedment) {
            $attachmentCid = $mailer->embed($embedment['file'], [
                'fileName' => $embedment['name'],
                'contentType' => $embedment['type']
            ]);

            $messageHtml = str_replace('#fafcms-embed-image#' . $embedmentId . '#fafcms-embed-image#', $attachmentCid, $messageHtml);
        }

        $mailer
            ->setTextBody($mail->message_text)
            ->setHtmlBody($messageHtml);

        $transaction = Yii::$app->db->beginTransaction();
        $mail->sent_at = (new DateTime('NOW', new DateTimeZone(Yii::$app->formatter->defaultTimeZone)))->format('Y-m-d H:i:s');

        if (!$mail->validate() || !$mail->save()) {
            throw new Exception(print_r($mail->errors, true));
        }

        if ($mailer->send()) {
            if ($transaction !== null) {
                $transaction->commit();
            }
            //$mail->mailer_id = $mailer->getSwiftMessage()->getId(); TODO
        } else {
            if ($transaction !== null) {
                $transaction->rollBack();
            }

            throw new Exception('$mailer->send() error. Mail id: ' . $mail->id);
        }
    }

    /**
     * @inheritdoc
     */
    public function getTtr()
    {
        return 2 * 60;
    }

    /**
     * @inheritdoc
     */
    public function canRetry($attempt, $error)
    {
        return $attempt < 3;
    }
}
