<?php
/**
 * @var $this \yii\web\View
 * @var $recipient \fafcms\mailmanager\models\Recipient|null
 * @var $headline string
 * @var $required string
 * @var $formId string
 * @var $model \fafcms\mailmanager\models\SubscribeForm
 * @var $activeFormClass \yii\widgets\ActiveForm
 * @var $isRequest bool
 * @var $submitButtonLabel string
 * @var $submitButtonCssClass string|null
 * @var $unsubscribeButtonLabel string
 * @var $unsubscribeButtonCssClass string|null
 */

use yii\helpers\Html;
use fafcms\fafcms\widgets\MaskedInput;

$form = $activeFormClass::begin([
    'id' => $formId
]);

echo '<h1>' . $headline . '</h1>';

if ($isRequest) {
    echo $form->field($model, 'email')->widget(MaskedInput::class, [
        'clientOptions' => ['alias' => 'email'],
    ]);
} else {
    echo $form->field($model, 'sex')->radioList($model->getAttributeOptions('sex'));

    echo $form->field($model, 'firstname')->textInput();

    echo $form->field($model, 'lastname')->textInput();

    echo $form->field($model, 'email')->widget(MaskedInput::class, [
        'clientOptions' => ['alias' => 'email'],
        'options' => [
            'disabled' => true
        ]
    ]);

    echo $form->field($model, 'interests')->dropDownList($model->getAttributeOptions('interests'), [
        'multiple' => true,
    ]);
}

echo $form->field($model, 'privacyPolicy')->checkbox([
    'label' => $model->getAttributeLabel('privacyPolicy')
]);

echo Html::submitButton($submitButtonLabel, ['form' => $form->id, 'class' => $submitButtonCssClass]);

if (!$isRequest) {
    echo Html::a($unsubscribeButtonLabel, [Yii::$app->fafcmsParser->data['currentContentmeta']->getRelativeUrl(), 'action' => 'unsubscribe', 'id' => $recipient->hashId], ['class' => $unsubscribeButtonCssClass]);
}

echo '<dl><dt><sup>*</sup> ' . $required . '</dt></dl>';

$activeFormClass::end();
