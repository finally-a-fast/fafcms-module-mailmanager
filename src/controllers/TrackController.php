<?php

declare(strict_types=1);

/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 - 2021 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-mailmanager/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-mailmanager
 * @see https://www.finally-a-fast.com/packages/fafcms-module-mailmanager/docs Documentation of the module mail manager
 * @since File available since Release 1.0.0
 */

namespace fafcms\mailmanager\controllers;

use DateTime;
use DateTimeZone;
use fafcms\helpers\DefaultController;
use fafcms\mailmanager\models\Track;
use yii\web\NotFoundHttpException;
use Yii;

/**
 * Class TrackController
 *
 * @package fafcms\mailmanager\models
 */
class TrackController extends DefaultController
{
    public static $modelClass = Track::class;

    /**
     * @inheritdoc
     */
    public function behaviors(): array
    {
        $behaviors = parent::behaviors();

        $behaviors['access']['rules'][] = [
            'actions' => ['track'],
            'allow' => true,
        ];

        return $behaviors;
    }

    /**
     * @param string $id
     *
     * @return \yii\console\Response|\yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionTrack(string $id)
    {
        $now = (new DateTime('NOW', new DateTimeZone(Yii::$app->formatter->defaultTimeZone)))->format('Y-m-d H:i:s');

        $track = Track::find()->where([
            'AND',
            ['hashId' => $id],
            [
                'OR',
                ['valid_until' => null],
                ['>=', 'valid_until', $now]
            ]
        ])->one();

        if ($track === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $firstUse = $track->first_use_at === null;

        if ($firstUse) {
            $track->first_use_at = $now;

            if ($track->mailing !== null) {
                if ($track->type === 1) {
                    $track->mailing->click_count++;

                    if ($track->mailing->track_view === 1) {
                        $viewTrack = Track::find()->where([
                            'AND',
                            [
                                'mail_id' => $track->mail_id,
                                'mailing_id' => $track->mailing_id,
                                'recipient_id' => $track->recipient_id,
                                'type' => 2,
                                'first_use_at' => null
                            ],
                            [
                                'OR',
                                ['valid_until' => null],
                                ['>=', 'valid_until', $now]
                            ]
                        ])->one();

                        if ($viewTrack !== null) {
                            $viewTrack->used = 1;
                            $viewTrack->use_count++;
                            $viewTrack->first_use_at = $now;
                            $viewTrack->last_use_at = $now;

                            if ($viewTrack->save()) {
                                $track->mailing->view_count++;
                            }
                        }

                    }
                } else {
                    $track->mailing->view_count++;
                }
            }
        }

        $track->used = 1;
        $track->use_count++;
        $track->last_use_at = $now;

        if ($track->save()) {
            if ($track->mailing !== null && $firstUse) {
                $track->mailing->save();
            }
        }

        if ($track->type === 1) {
            return $this->redirect($track->target);
        }

        header('Expires: Thu, 01 Jan 1970 00:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0', false);
        header('Pragma: no-cache');

        return Yii::$app->response->sendFile(Yii::getAlias('@fafcms/mailmanager/assets/img/transparent.png'), '42.png', [
            'mimeType' => 'image/png',
            'inline' => true
        ]);
    }
}
