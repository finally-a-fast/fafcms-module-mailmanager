<?php

declare(strict_types=1);

/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 - 2021 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-mailmanager/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-mailmanager
 * @see https://www.finally-a-fast.com/packages/fafcms-module-mailmanager/docs Documentation of the module mail manager
 * @since File available since Release 1.0.0
 */

namespace fafcms\mailmanager\models;

use DateTime;
use DateTimeZone;
use fafcms\helpers\traits\AttributeOptionTrait;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\db\Exception;
use yii\db\StaleObjectException;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;

/**
 * Class SubscribeForm
 *
 * @package fafcms\mailmanager\models
 */
class SubscribeForm extends Model
{
    use AttributeOptionTrait;

    public string $email = '';
    public string $sex = '';
    public string $firstname = '';
    public string $lastname = '';
    public bool $privacyPolicy = false;
    public array $interests = [];

    public bool $sexRequired = true;
    public bool $firstnameRequired = true;
    public bool $lastnameRequired = true;
    public bool $interestsRequired = true;
    public bool $privacyPolicyRequired = true;

    public string $privacyPolicyServiceName = '';
    public string $privacyPolicyMailContact = '';
    public string $privacyPolicyPrivacyPage = '';

    public ?string $alreadySubscribedMailSubject = null;
    public ?int $alreadySubscribedMailTemplate = null;
    public ?string $alreadySubscribedChangePage = null;

    public ?string $newSubscriptionMailSubject = null;
    public ?int $newSubscriptionMailTemplate = null;

    public ?string $newSubscriptionConfirmPage = null;

    public ?Recipientlist $recipientlist = null;

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'email'         => Yii::t('fafcms-mailmanager', 'Email'),
            'sex'           => Yii::t('fafcms-mailmanager', 'Sex'),
            'firstname'     => Yii::t('fafcms-mailmanager', 'Firstname'),
            'lastname'      => Yii::t('fafcms-mailmanager', 'Lastname'),
            'privacyPolicy' => Yii::t('fafcms-mailmanager', 'I consent to the collection and processing of my details from the form for the use of {name} services. Note: You can revoke your consent for the future at any time by sending an e-mail to {mail}. Detailed information on the handling of user data can be found in our <a href="{privacy}" target="_blank">Privacy Policy</a>.', [
                'name'    => $this->privacyPolicyServiceName,
                'mail'    => $this->privacyPolicyMailContact,
                'privacy' => $this->privacyPolicyPrivacyPage,
            ]),
            'interests'     => Yii::t('fafcms-mailmanager', 'Interests')
        ]);
    }

    public function attributeOptions(): array
    {
        return [
            'sex' => [
                'female' => Yii::t('fafcms-mailmanager', 'Miss'),
                'male' => Yii::t('fafcms-mailmanager', 'Mister'),
                'misc' => Yii::t('fafcms-mailmanager', 'Miscellaneous'),
            ],
            'interests' => ArrayHelper::map($this->recipientlist->userSelectableRecipientlistInterests, 'id', 'name')
        ];
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        $rules = array_merge(parent::rules(), [
            'required-email' => ['email', 'required'],
            'string-email' => ['email', 'string', 'max' => 255],
            'string-sex' => ['sex', 'string', 'max' => 255],
            'string-firstname' => ['firstname', 'string', 'max' => 255],
            'string-lastname' => ['lastname', 'string', 'max' => 255],
        ]);

        if ($this->sexRequired) {
            $rules['required-sex'] = ['sex', 'required'];
        }

        if ($this->firstnameRequired) {
            $rules['required-firstname'] = ['firstname', 'required'];
        }

        if ($this->lastnameRequired) {
            $rules['required-lastname'] = ['lastname', 'required'];
        }

        if ($this->interestsRequired) {
            $rules['required-interests'] = ['interests', 'required'];
        }

        if ($this->privacyPolicyRequired) {
            $rules['required-privacyPolicy'] = ['privacyPolicy', 'required', 'message' => Yii::t('fafcms-mailmanager', 'Please confirm that you have read and agree with the legal notice on data use.')];
            $rules['compare-privacyPolicy'] = ['privacyPolicy', 'compare', 'compareValue' => '1', 'message' => Yii::t('fafcms-mailmanager', 'Please confirm that you have read and agree with the legal notice on data use.')];
        }

        return $rules;
    }

    /**
     * @return bool
     * @throws InvalidConfigException
     */
    public function save($runValidation = true, $attributeNames = null): bool
    {
        if ($this->validate()) {
            $hasError = false;

            $recipient = Recipient::find()->where([
                'recipientlist_id' => $this->recipientlist->id,
                'email' => $this->email
            ])->byStatus('all')->one();

            if ($recipient === null) {
                $recipient = new Recipient();
            } elseif ($recipient->status === 'active') {
                $url = Url::to([$this->alreadySubscribedChangePage, 'action' => 'change', 'id' => $recipient->hashId], true);

                $mail = Mail::createByTemplate($this->alreadySubscribedMailTemplate, [
                    'recipient_id' => $recipient->id,
                ], [
                    'to'        => [$recipient->email => $recipient->firstname . ' ' . $recipient->lastname],
                    'subject'   => $this->alreadySubscribedMailSubject,
                    'html'      => Yii::t('fafcms-mailmanager', 'You have already subscribed to our newsletter. Therefore, you do not need to do anything else.') . '<br><br>' . Yii::t('fafcms-mailmanager','If you want to change your data you can do so at:') . PHP_EOL . '<a href="' . $url . '">' . $url . '</a>',
                    'text'      => Yii::t('fafcms-mailmanager', 'You have already subscribed to our newsletter. Therefore, you do not need to do anything else.') . PHP_EOL . PHP_EOL . Yii::t('fafcms-mailmanager','If you want to change your data you can do so at:') . PHP_EOL . $url,
                    'recipient' => $recipient,
                    'url'       => $url
                ]);

                if ($mail === null || !$mail->save()) {
                    if ($mail !== null) {
                        $this->addError('email', implode('<br><br>', $mail->getErrorSummary(true)));
                    }

                    $hasError = true;
                }

                return !$hasError;
            }

            $recipient->setAttributes([
                'recipientlist_id' => $this->recipientlist->id,
                'email'            => $this->email,
                'sex'              => $this->sex,
                'firstname'        => $this->firstname,
                'lastname'         => $this->lastname,
                'requested'        => 1,
                'requested_at'     => (new DateTime('NOW', new DateTimeZone(Yii::$app->formatter->defaultTimeZone)))->format('Y-m-d H:i:s'),
                'requested_data'   => Json::encode(['post' => $_POST, 'server' => $_SERVER]),
            ]);

            $transaction = Yii::$app->db->beginTransaction();

            if (!$recipient->save()) {
                $this->addError('email', implode('<br><br>', $recipient->getErrorSummary(true)));
                $hasError = true;
            }

            $savedInterests = ArrayHelper::getColumn($recipient->recipientRecipientInterests, 'interest_id');

            foreach ($this->interests as $interest) {
                if (!in_array($interest, $savedInterests, false)) {
                    $recipientInterest = new RecipientInterest([
                        'recipient_id' => $recipient->id,
                        'interest_id' => $interest
                    ]);

                    if (!$recipientInterest->save()) {
                        $this->addError('interests', implode('<br><br>', $recipientInterest->getErrorSummary(true)));
                        $hasError = true;
                        break;
                    }
                }
            }

            foreach ($recipient->recipientRecipientInterests as $interest) {
                if (!in_array($interest->interest_id, $this->interests, false)) {
                    try {
                        $interest->delete();
                    } catch (StaleObjectException | \Throwable $e) {}
                }
            }

            if (!$hasError) {
                $url = Url::to([$this->newSubscriptionConfirmPage ?? Yii::$app->fafcmsParser->data['currentContentmeta']->getRelativeUrl(), 'action' => 'confirm', 'id' => $recipient->hashId], true);

                $mail = Mail::createByTemplate($this->newSubscriptionMailTemplate, [
                    'recipient_id' => $recipient->id,
                ], [
                    'to'        => [$recipient->email => $recipient->firstname . ' ' . $recipient->lastname],
                    'subject'   => $this->newSubscriptionMailSubject,
                    'html'      => Yii::t('fafcms-mailmanager', 'We are very pleased about your interest. Please confirm your newsletter subscription now:') . PHP_EOL . '<a href="' . $url . '">' . $url . '</a>',
                    'text'      => Yii::t('fafcms-mailmanager', 'We are very pleased about your interest. Please confirm your newsletter subscription now:') . PHP_EOL . $url,
                    'recipient' => $recipient,
                    'url'       => $url
                ]);

                if ($mail === null || !$mail->save()) {
                    if ($mail !== null) {
                        $this->addError('email', implode('<br><br>', $mail->getErrorSummary(true)));
                    }

                    $hasError = true;
                } else {
                    $recipient->requested_mail_id = $mail->id;

                    if (!$recipient->save()) {
                        $this->addError('email', implode('<br><br>', $mail->getErrorSummary(true)));
                    }
                }
            }

            if ($hasError) {
                if ($transaction !== null) {
                    $transaction->rollBack();
                }
                return false;
            }

            if ($transaction !== null) {
                try {
                    $transaction->commit();
                } catch (Exception $e) {
                    return false;
                }
            }

            return true;
        }

        return false;
    }
}
