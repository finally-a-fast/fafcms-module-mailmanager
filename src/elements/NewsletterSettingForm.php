<?php

declare(strict_types=1);

/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 - 2021 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-mailmanager/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-mailmanager
 * @see https://www.finally-a-fast.com/packages/fafcms-module-mailmanager/docs Documentation of the module mail manager
 * @since File available since Release 1.0.0
 */

namespace fafcms\mailmanager\elements;

use DateTime;
use DateTimeZone;
use Faf\TemplateEngine\Helpers\ElementSetting;
use Faf\TemplateEngine\Helpers\ParserElement;
use fafcms\mailmanager\models\Recipient;
use fafcms\mailmanager\models\Recipientlist;
use fafcms\mailmanager\models\SettingForm;
use fafcms\sitemanager\models\Contentmeta;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use Yiisoft\Validator\Rule\InRange;
use Yiisoft\Validator\Rule\Number;
use Yiisoft\Validator\Rule\Required;

/**
 * Class NewsletterSettingForm
 *
 * @package fafcms\mailmanager\elements
 */
class NewsletterSettingForm extends ParserElement
{
    /**
     * {@inheritdoc}
     */
    public function name(): string
    {
        return 'newsletter-setting-form';
    }

    /**
     * {@inheritdoc}
     */
    public function description(): string
    {
        return Yii::t('fafcms-mailmanager', 'Shows the newsletter setting form.');
    }

    /**
     * {@inheritdoc}
     */
    public function elementSettings(): array
    {
        return [
            new ElementSetting([
                'name' => 'recipientlist-id',
                'label' => Yii::t('fafcms-mailmanager', 'Recipientlist'),
                'rules' => [
                    new Required()
                ]
            ]),
            new ElementSetting([
                'name' => 'active-form-class',
                'label' => Yii::t('fafcms-mailmanager', 'Active form class'),
                'defaultValue' => 'fafcms\\fafcms\\widgets\\ActiveForm',
                'rules' => [
                    new Required()
                ]
            ]),
            new ElementSetting([
                'name' => 'request-button-label',
                'label' => Yii::t('fafcms-mailmanager', 'Request button label'),
                'defaultValue' => ['fafcms-mailmanager', 'Request data change link'],
                'rules' => [
                    new Required()
                ]
            ]),
            new ElementSetting([
                'name' => 'request-button-css-class',
                'label' => Yii::t('fafcms-mailmanager', 'Request button css class'),
            ]),
            new ElementSetting([
                'name' => 'submit-button-label',
                'label' => Yii::t('fafcms-mailmanager', 'Submit button label'),
                'defaultValue' => ['fafcms-mailmanager', 'Save changes'],
                'rules' => [
                    new Required()
                ]
            ]),
            new ElementSetting([
                'name' => 'submit-button-css-class',
                'label' => Yii::t('fafcms-mailmanager', 'Submit button css class'),
            ]),
            new ElementSetting([
                'name' => 'unsubscribe-button-label',
                'label' => Yii::t('fafcms-mailmanager', 'Unsubscribe button label'),
                'defaultValue' => ['fafcms-mailmanager', 'Unsubscribe from newsletter'],
                'rules' => [
                    new Required()
                ]
            ]),
            new ElementSetting([
                'name' => 'unsubscribe-button-css-class',
                'label' => Yii::t('fafcms-mailmanager', 'Unsubscribe button css class'),
            ]),
            new ElementSetting([
                'name' => 'privacy-policy-service-name',
                'label' => Yii::t('fafcms-mailmanager', 'Privacy policy service name'),
            ]),
            new ElementSetting([
                'name' => 'privacy-policy-mail-contact',
                'label' => Yii::t('fafcms-mailmanager', 'Privacy policy mail contact'),
            ]),
            new ElementSetting([
                'name' => 'privacy-policy-privacy-page',
                'label' => Yii::t('fafcms-mailmanager', 'Privacy policy privacy page (link or content meta id)'),
            ]),
            new ElementSetting([
                'name' => 'request-success-position',
                'label' => Yii::t('fafcms-mailmanager', 'Request success position'),
                'defaultValue' => 'alert',
                'rules' => [
                    new Required(),
                    new InRange(['alert', 'prepend', 'append'])
                ]
            ]),
            new ElementSetting([
                'name' => 'request-success-message',
                'label' => Yii::t('fafcms-mailmanager', 'Request success message'),
                'content' => true,
                'defaultValue' => Yii::t('fafcms-mailmanager', 'To secure your data, you will receive an email from us shortly, which contains a edit link. Please click on the link to change your data.'),
                'rules' => [
                    new Required()
                ]
            ]),
            new ElementSetting([
                'name' => 'save-success-position',
                'label' => Yii::t('fafcms-mailmanager', 'Save success position'),
                'defaultValue' => 'alert',
                'rules' => [
                    new Required(),
                    new InRange(['alert', 'prepend', 'append'])
                ]
            ]),
            new ElementSetting([
                'name' => 'save-success-message',
                'label' => Yii::t('fafcms-mailmanager', 'Save success message'),
                'defaultValue' => Yii::t('fafcms-mailmanager', 'Your data has been updated.'),
                'rules' => [
                    new Required()
                ]
            ]),
            new ElementSetting([
                'name' => 'setting-request-mail-subject',
                'label' => Yii::t('fafcms-mailmanager', 'Setting request mail subject'),
                'defaultValue' => Yii::t('fafcms-mailmanager', 'Newsletter settings') .  ' - ' . Yii::$app->fafcms->getCurrentProjectLanguage()->site_name,
                'rules' => [
                    new Required()
                ]
            ]),
            new ElementSetting([
                'name' => 'setting-request-mail-template',
                'label' => Yii::t('fafcms-mailmanager', 'Setting request mail template id'),
                'rules' => [
                    new Required(),
                    new Number()
                ]
            ]),
        ];
    }

    /**
     * @param Recipientlist $recipientlist
     * @param string        $scenario
     *
     * @return SettingForm
     * @throws \yii\base\InvalidConfigException
     */
    private function createModel(Recipientlist $recipientlist, string $scenario): SettingForm
    {
        $privacyPage = $this->data['privacy-policy-privacy-page'];

        if (is_numeric($privacyPage)) {
            $privacyPage = '/' . Contentmeta::getUrlById($privacyPage);
        }

        return new SettingForm([
            'privacyPolicyServiceName'   => $this->data['privacy-policy-service-name'],
            'privacyPolicyMailContact'   => $this->data['privacy-policy-mail-contact'],
            'privacyPolicyPrivacyPage'   => $privacyPage,
            'recipientlist'              => $recipientlist,
            'scenario'                   => $scenario,
            'settingRequestMailSubject'  => $this->data['setting-request-mail-subject'],
            'settingRequestMailTemplate' => $this->data['setting-request-mail-template'],
        ]);
    }

    /**
     * {@inheritdoc}
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        $recipientlist = Recipientlist::find()->where(['id' => $this->data['recipientlist-id']])->one();

        $scenario = SettingForm::SCENARIO_DEFAULT;

        if ($recipientlist === null) {
            return '';
        }

        $prepend = '';
        $append = '';
        $recipient = null;
        $action = Yii::$app->request->get('action');

        if ($action === 'change') {
            $id = Yii::$app->request->get('id');

            if ($id !== null && ($recipient = Recipient::find()->where(['hashId' => $id])->one()) !== null) {
                $scenario = SettingForm::SCENARIO_CHANGE;
            } else {
                Yii::$app->session->setFlash('error', Yii::t('fafcms-core', 'An error has occurred, please try again.'));
            }
        } elseif ($action == 'unsubscribe') {
            $id = Yii::$app->request->get('id');

            if ($id !== null && ($recipient = Recipient::find()->where(['hashId' => $id])->one()) !== null && $recipient->delete()) {
                Yii::$app->session->setFlash('success', Yii::t('fafcms-mailmanager', 'You have successfully unsubscribed from our newsletter.'));
                Yii::$app->view->renderedSiteMode = 'raw';
                Yii::$app->view->renderedSiteContent = Yii::$app->controller->redirect(Yii::$app->fafcmsParser->data['currentContentmeta']->getRelativeUrl());
            } else {
                Yii::$app->session->setFlash('error', Yii::t('fafcms-core', 'An error has occurred, please try again.'));
            }
        }

        $model = $this->createModel($recipientlist, $scenario);

        if ($scenario === SettingForm::SCENARIO_CHANGE) {
            $model->setAttributes([
                'sex'       => $recipient->sex,
                'firstname' => $recipient->firstname,
                'lastname'  => $recipient->lastname,
                'email'     => $recipient->email,
                'interests' => ArrayHelper::getColumn($recipient->recipientRecipientInterests, 'interest_id'),
            ]);
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                if ($model->save()) {

                    if ($scenario === SettingForm::SCENARIO_CHANGE) {
                        $messageType = 'save-success';
                    } else {
                        $messageType = 'request-success';
                    }

                    switch ($this->data[$messageType . '-position']) {
                        case 'alert':
                            Yii::$app->session->setFlash('success', $this->data[$messageType . '-message']);
                            break;
                        case 'prepend':
                            $prepend .= $this->data[$messageType . '-message'];
                            break;
                        case 'append':
                            $append .= $this->data[$messageType . '-message'];
                            break;
                    }
                } else {
                    Yii::$app->session->setFlash('error', Yii::t('fafcms-core', 'An error has occurred, please try again.'));
                }
            } else {
                Yii::$app->session->setFlash('error', Yii::t('fafcms-core', 'An error has occurred, please try again.') . '<br><br>' . implode('<br><br>', $model->getErrorSummary(true)));
            }
        }

        return $prepend . Yii::$app->view->render('@fafcms/mailmanager/views/setting', [
            'model'                     => $model,
            'recipient'                 => $recipient,
            'headline'                  => Yii::t('fafcms-mailmanager', 'Newsletter settings'),
            'required'                  => Yii::t('fafcms-mailmanager', 'Mandatory fields'),
            'formId'                    => 'subscribe-form-' . $recipientlist->hashId,
            'activeFormClass'           => $this->data['active-form-class'],
            'isRequest'                 => $scenario === SettingForm::SCENARIO_DEFAULT,
            'submitButtonLabel'         => Yii::$app->fafcms->getTextOrTranslation($scenario === SettingForm::SCENARIO_DEFAULT ? $this->data['request-button-label'] : $this->data['submit-button-label']),
            'submitButtonCssClass'      => $scenario === SettingForm::SCENARIO_DEFAULT ? $this->data['request-button-css-class'] : $this->data['submit-button-css-class'],
            'unsubscribeButtonLabel'    => Yii::$app->fafcms->getTextOrTranslation($this->data['unsubscribe-button-label']),
            'unsubscribeButtonCssClass' => $this->data['unsubscribe-button-css-class'],
        ]) . $append;
    }
}
