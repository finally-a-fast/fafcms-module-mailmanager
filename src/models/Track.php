<?php

namespace fafcms\mailmanager\models;

use fafcms\helpers\traits\MultilingualTrait;
use fafcms\mailmanager\abstracts\models\BaseTrack;

/**
 * This is the model class for table "{{%track}}".
 *
 * @package fafcms\mailmanager\models
 */
class Track extends BaseTrack
{
    use MultilingualTrait;

    /**
     * @param array $params
     *
     * @return static|null
     * @throws \yii\base\InvalidConfigException
     */
    public static function findOrCreate(array $params): ?self
    {
        $track = self::find()->where($params)->one();

        if ($track === null) {
            $track = new self($params);

            if (!$track->save()) {
                $track = null;
            }
        }

        return $track;
    }
}
