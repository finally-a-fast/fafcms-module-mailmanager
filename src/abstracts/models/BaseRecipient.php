<?php

namespace fafcms\mailmanager\abstracts\models;

use fafcms\fafcms\{
    inputs\DateTimePicker,
    inputs\DropDownList,
    inputs\EmailInput,
    inputs\ExtendedDropDownList,
    inputs\NumberInput,
    inputs\SwitchCheckbox,
    inputs\Textarea,
    inputs\TextInput,
    items\ActionColumn,
    items\Card,
    items\Column,
    items\DataColumn,
    items\FormField,
    items\Row,
    items\Tab,
    models\User,
};
use fafcms\helpers\{
    ActiveRecord,
    classes\OptionProvider,
    interfaces\EditViewInterface,
    interfaces\FieldConfigInterface,
    interfaces\IndexViewInterface,
    traits\AttributeOptionTrait,
    traits\BeautifulModelTrait,
    traits\OptionProviderTrait,
};
use fafcms\mailmanager\{
    Bootstrap,
    models\Mail,
    models\RecipientInterest,
    models\Recipientlist,
    models\Track,
};
use Yii;
use yii\db\ActiveQuery;
use yii\validators\DateValidator;

/**
 * This is the abstract model class for table "{{%recipient}}".
 *
 * @package fafcms\mailmanager\abstracts\models
 *
 * @property-read array $fieldConfig
 *
 * @property int $id
 * @property string $status
 * @property int $recipientlist_id
 * @property string $email
 * @property string|null $sex
 * @property string|null $firstname
 * @property string|null $lastname
 * @property int $requested
 * @property string|null $requested_at
 * @property string|null $requested_data
 * @property int|null $requested_mail_id
 * @property int $confirmed
 * @property string|null $confirmed_at
 * @property string|null $confirmed_data
 * @property int|null $confirmed_mail_id
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $activated_by
 * @property int|null $deactivated_by
 * @property int|null $deleted_by
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $activated_at
 * @property string|null $deactivated_at
 * @property string|null $deleted_at
 *
 * @property User $activatedBy
 * @property User $createdBy
 * @property User $deactivatedBy
 * @property User $deletedBy
 * @property Mail[] $recipientMails
 * @property RecipientInterest[] $recipientRecipientInterests
 * @property Track[] $recipientTracks
 * @property Recipientlist $recipientlist
 * @property User $updatedBy
 */
abstract class BaseRecipient extends ActiveRecord implements FieldConfigInterface, IndexViewInterface, EditViewInterface
{
    use BeautifulModelTrait;
    use OptionProviderTrait;
    use AttributeOptionTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return Bootstrap::$id . '/recipient';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return  'recipient';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-mailmanager', 'Recipients');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-mailmanager', 'Recipient');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['firstname'] ?? '') . ' ' . ($model['lastname'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionProviderTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptionProvider(array $properties = []): OptionProvider
    {
        return (new OptionProvider(static::class))
            ->setSelect([
                static::tableName() . '.id',
                static::tableName() . '.firstname', static::tableName() . '.lastname'
            ])
            ->setSort([static::tableName() . '.firstname' => SORT_ASC, static::tableName() . '.lastname' => SORT_ASC])
            ->setItemLabel(static function ($item) {
                return static::extendedLabel($item);
            })
            ->setProperties($properties);
    }
    //endregion OptionProviderTrait implementation

    //region AttributeOptionTrait implementation
    /**
     * @inheritDoc
     */
    public function attributeOptions(): array
    {
        return [
            'status' => [
                static::STATUS_ACTIVE => Yii::t('fafcms-core', 'Active'),
                static::STATUS_INACTIVE => Yii::t('fafcms-core', 'Inactive'),
            ],
            'recipientlist_id' => static function($properties = []) {
                return Recipientlist::getOptionProvider($properties)->getOptions();
            },
            'created_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'updated_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'activated_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'deactivated_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'deleted_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
        ];
    }
    //endregion AttributeOptionTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'id' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'status' => [
                'type' => DropDownList::class,
                'items' => $this->getAttributeOptions('status', false),
            ],
            'recipientlist_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('recipientlist_id', false),
                'relationClassName' => Recipientlist::class,
            ],
            'email' => [
                'type' => EmailInput::class,
            ],
            'sex' => [
                'type' => TextInput::class,
            ],
            'firstname' => [
                'type' => TextInput::class,
            ],
            'lastname' => [
                'type' => TextInput::class,
            ],
            'requested' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'requested_at' => [
                'type' => DateTimePicker::class,
            ],
            'requested_data' => [
                'type' => Textarea::class,
            ],
            'requested_mail_id' => [
                'type' => NumberInput::class,
            ],
            'confirmed' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'confirmed_at' => [
                'type' => DateTimePicker::class,
            ],
            'confirmed_data' => [
                'type' => Textarea::class,
            ],
            'confirmed_mail_id' => [
                'type' => NumberInput::class,
            ],
            'created_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('created_by', false),
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('updated_by', false),
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'activated_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('activated_by', false),
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deactivated_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('deactivated_by', false),
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deleted_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('deleted_by', false),
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'created_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'activated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deactivated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deleted_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
        ];
    }
    //endregion FieldConfigInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        return [
            'default' => [
                'id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'id',
                        'sort' => 1,
                    ],
                ],
                'status' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'status',
                        'sort' => 2,
                    ],
                ],
                'firstname' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'firstname',
                        'sort' => 3,
                        'link' => true,
                    ],
                ],
                'lastname' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'lastname',
                        'sort' => 4,
                        'link' => true,
                    ],
                ],
                'recipientlist_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'recipientlist_id',
                        'sort' => 5,
                        'link' => true,
                    ],
                ],
                'email' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'email',
                        'sort' => 6,
                    ],
                ],
                'sex' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'sex',
                        'sort' => 7,
                    ],
                ],
                'requested' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'requested',
                        'sort' => 8,
                    ],
                ],
                'requested_at' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'requested_at',
                        'sort' => 9,
                    ],
                ],
                'requested_data' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'requested_data',
                        'sort' => 10,
                    ],
                ],
                'requested_mail_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'requested_mail_id',
                        'sort' => 11,
                    ],
                ],
                'confirmed' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'confirmed',
                        'sort' => 12,
                    ],
                ],
                'confirmed_at' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'confirmed_at',
                        'sort' => 13,
                    ],
                ],
                'confirmed_data' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'confirmed_data',
                        'sort' => 14,
                    ],
                ],
                'confirmed_mail_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'confirmed_mail_id',
                        'sort' => 15,
                    ],
                ],
                'action-column' => [
                    'class' => ActionColumn::class,
                ],
            ]
        ];
    }
    //endregion IndexViewInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-status' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'status',
                                                    ],
                                                ],
                                                'field-recipientlist_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'recipientlist_id',
                                                    ],
                                                ],
                                                'field-email' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'email',
                                                    ],
                                                ],
                                                'field-sex' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'sex',
                                                    ],
                                                ],
                                                'field-firstname' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'firstname',
                                                    ],
                                                ],
                                                'field-lastname' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'lastname',
                                                    ],
                                                ],
                                                'field-requested' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'requested',
                                                    ],
                                                ],
                                                'field-requested_at' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'requested_at',
                                                    ],
                                                ],
                                                'field-requested_data' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'requested_data',
                                                    ],
                                                ],
                                                'field-requested_mail_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'requested_mail_id',
                                                    ],
                                                ],
                                                'field-confirmed' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'confirmed',
                                                    ],
                                                ],
                                                'field-confirmed_at' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'confirmed_at',
                                                    ],
                                                ],
                                                'field-confirmed_data' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'confirmed_data',
                                                    ],
                                                ],
                                                'field-confirmed_mail_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'confirmed_mail_id',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%recipient}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'required-recipientlist_id' => ['recipientlist_id', 'required'],
            'required-email' => ['email', 'required'],
            'integer-recipientlist_id' => ['recipientlist_id', 'integer'],
            'integer-requested_mail_id' => ['requested_mail_id', 'integer'],
            'integer-confirmed_mail_id' => ['confirmed_mail_id', 'integer'],
            'integer-created_by' => ['created_by', 'integer'],
            'integer-updated_by' => ['updated_by', 'integer'],
            'integer-activated_by' => ['activated_by', 'integer'],
            'integer-deactivated_by' => ['deactivated_by', 'integer'],
            'integer-deleted_by' => ['deleted_by', 'integer'],
            'boolean-requested' => ['requested', 'boolean'],
            'boolean-confirmed' => ['confirmed', 'boolean'],
            'date-requested_at' => ['requested_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'requested_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-confirmed_at' => ['confirmed_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'confirmed_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-created_at' => ['created_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'created_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-updated_at' => ['updated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'updated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-activated_at' => ['activated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'activated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-deactivated_at' => ['deactivated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'deactivated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-deleted_at' => ['deleted_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'deleted_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'string-requested_data' => ['requested_data', 'string'],
            'string-confirmed_data' => ['confirmed_data', 'string'],
            'string-status' => ['status', 'string', 'max' => 255],
            'string-email' => ['email', 'string', 'max' => 255],
            'string-sex' => ['sex', 'string', 'max' => 255],
            'string-firstname' => ['firstname', 'string', 'max' => 255],
            'string-lastname' => ['lastname', 'string', 'max' => 255],
            'exist-activated_by' => [['activated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['activated_by' => 'id']],
            'exist-created_by' => [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            'exist-deactivated_by' => [['deactivated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['deactivated_by' => 'id']],
            'exist-deleted_by' => [['deleted_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['deleted_by' => 'id']],
            'exist-recipientlist_id' => [['recipientlist_id'], 'exist', 'skipOnError' => true, 'targetClass' => Recipientlist::class, 'targetAttribute' => ['recipientlist_id' => 'id']],
            'exist-updated_by' => [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'id' => Yii::t('fafcms-mailmanager', 'ID'),
            'status' => Yii::t('fafcms-mailmanager', 'Status'),
            'recipientlist_id' => Yii::t('fafcms-mailmanager', 'Recipientlist ID'),
            'email' => Yii::t('fafcms-mailmanager', 'Email'),
            'sex' => Yii::t('fafcms-mailmanager', 'Sex'),
            'firstname' => Yii::t('fafcms-mailmanager', 'Firstname'),
            'lastname' => Yii::t('fafcms-mailmanager', 'Lastname'),
            'requested' => Yii::t('fafcms-mailmanager', 'Requested'),
            'requested_at' => Yii::t('fafcms-mailmanager', 'Requested At'),
            'requested_data' => Yii::t('fafcms-mailmanager', 'Requested Data'),
            'requested_mail_id' => Yii::t('fafcms-mailmanager', 'Requested Mail ID'),
            'confirmed' => Yii::t('fafcms-mailmanager', 'Confirmed'),
            'confirmed_at' => Yii::t('fafcms-mailmanager', 'Confirmed At'),
            'confirmed_data' => Yii::t('fafcms-mailmanager', 'Confirmed Data'),
            'confirmed_mail_id' => Yii::t('fafcms-mailmanager', 'Confirmed Mail ID'),
            'created_by' => Yii::t('fafcms-mailmanager', 'Created By'),
            'updated_by' => Yii::t('fafcms-mailmanager', 'Updated By'),
            'activated_by' => Yii::t('fafcms-mailmanager', 'Activated By'),
            'deactivated_by' => Yii::t('fafcms-mailmanager', 'Deactivated By'),
            'deleted_by' => Yii::t('fafcms-mailmanager', 'Deleted By'),
            'created_at' => Yii::t('fafcms-mailmanager', 'Created At'),
            'updated_at' => Yii::t('fafcms-mailmanager', 'Updated At'),
            'activated_at' => Yii::t('fafcms-mailmanager', 'Activated At'),
            'deactivated_at' => Yii::t('fafcms-mailmanager', 'Deactivated At'),
            'deleted_at' => Yii::t('fafcms-mailmanager', 'Deleted At'),
        ]);
    }

    /**
     * Gets query for [[ActivatedBy]].
     *
     * @return ActiveQuery
     */
    public function getActivatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'activated_by',
        ]);
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return ActiveQuery
     */
    public function getCreatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'created_by',
        ]);
    }

    /**
     * Gets query for [[DeactivatedBy]].
     *
     * @return ActiveQuery
     */
    public function getDeactivatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'deactivated_by',
        ]);
    }

    /**
     * Gets query for [[DeletedBy]].
     *
     * @return ActiveQuery
     */
    public function getDeletedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'deleted_by',
        ]);
    }

    /**
     * Gets query for [[RecipientMails]].
     *
     * @return ActiveQuery
     */
    public function getRecipientMails(): ActiveQuery
    {
        return $this->hasMany(Mail::class, [
            'recipient_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[RecipientRecipientInterests]].
     *
     * @return ActiveQuery
     */
    public function getRecipientRecipientInterests(): ActiveQuery
    {
        return $this->hasMany(RecipientInterest::class, [
            'recipient_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[RecipientTracks]].
     *
     * @return ActiveQuery
     */
    public function getRecipientTracks(): ActiveQuery
    {
        return $this->hasMany(Track::class, [
            'recipient_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[Recipientlist]].
     *
     * @return ActiveQuery
     */
    public function getRecipientlist(): ActiveQuery
    {
        return $this->hasOne(Recipientlist::class, [
            'id' => 'recipientlist_id',
        ]);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return ActiveQuery
     */
    public function getUpdatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'updated_by',
        ]);
    }
}
